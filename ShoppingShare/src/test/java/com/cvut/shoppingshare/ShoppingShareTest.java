package com.cvut.shoppingshare;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * ShoppingShare specific base for unit tests.
 * 
 * Extends {@link AbstractTransactionalJUnit4SpringContextTests}.
 * 
 * @author Marek Cech
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })
public class ShoppingShareTest extends AbstractTransactionalJUnit4SpringContextTests {

}
