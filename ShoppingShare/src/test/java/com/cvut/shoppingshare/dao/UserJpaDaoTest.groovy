package com.cvut.shoppingshare.dao

import static org.junit.Assert.*

import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.annotation.Rollback
import org.springframework.transaction.annotation.Transactional

import com.cvut.shoppingshare.ShoppingShareGroovyTest
import com.cvut.shoppingshare.domain.MembershipRequest
import com.cvut.shoppingshare.domain.MembershipRequestState
import com.cvut.shoppingshare.domain.MembershipRequestType
import com.cvut.shoppingshare.domain.ShareGroup
import com.cvut.shoppingshare.domain.ShareUser
import com.cvut.shoppingshare.web.form.ShareUserDto
import com.cvut.shoppingshare.web.form.ShareUserWebDto;
import com.cvut.shoppingshare.web.form.UserAutocompleteForm;

@Transactional
class UserJpaDaoTest extends ShoppingShareGroovyTest {

	@Autowired
	UserDao userDao;
	
	@Autowired
	GroupDao groupDao;
	
	private ShareUser user1;
	private ShareUser user2;
	private ShareUser user3;
	
	private ShareGroup group1;
	
	@Before
	public void setUp() {
		user1 = new ShareUser(login:"a@b.cz", password:"1234", firstname:"Jiri", lastname:"Kysela");
		userDao.persist(user1);
		
		user2 = new ShareUser(login:"c@d.cz", password:"4321", firstname:"Jakub", lastname:"Matuska");
		userDao.persist(user2);
		
		user3 = new ShareUser(login:"e@f.cz", password:"2222", firstname:"Petr", lastname:"Kujal");
		userDao.persist(user3);
		
		group1 = new ShareGroup(name:"group blue", created:new Date());
		group1.addUser(user1);
		groupDao.persist(group1);
		
		user1.addGroup(group1);
		userDao.update(user1);
		
		userDao.getEntityManager().flush();
	}
	
	@After
	public void cleanUp() {
		userDao.remove(user1);
		userDao.remove(user2);
		
		groupDao.remove(group1);
	}
	
	@Test
	@Rollback(true)
	public void getShareUsersOfGroupTest() {
		List<ShareUserDto> result = userDao.getShareUsersOfGroup(group1.getEntityId());
		
		assertNotNull result;
		assertEquals 1, result.size();
		
		ShareUserDto dto1 = result.get(0);
		assertEquals user1.getEntityId(), dto1.getUserId();
		assertEquals user1.getLogin(), dto1.getLogin();
		assertEquals user1.getFirstname(), dto1.getFirstname();
		assertEquals user1.getLastname(), dto1.getLastname();
	}
	
	@Test
	@Rollback(true)
	public void getAllOfGroupWithIdTest() {
		List<ShareUserWebDto> result = userDao.getAllOfGroupWithId(group1.getEntityId());
		
		assertNotNull result;
		assertEquals 1, result.size();
		
		ShareUserWebDto dto1 = result.get(0);
		assertEquals user1.getLogin(), dto1.getLogin();
		assertEquals user1.getFirstname(), dto1.getFirstname();
		assertEquals user1.getLastname(), dto1.getLastname();
	}
	
	@Test
	@Rollback(true)
	public void findByLoginFirstnameLastnameStartsWithFirstnameTest() {
		String tagName = "Jir";
		List<UserAutocompleteForm> result = userDao.findByLoginFirstnameLastnameStartsWith(tagName);
		
		assertNotNull result;
		assertEquals 1, result.size();
		assertEquals user1.getEntityId(), result.get(0).getId();
	}
	
	@Test
	@Rollback(true)
	public void findByLoginFirstnameLastnameStartsWithLastnameTest() {
		String tagName = "Mat";
		List<UserAutocompleteForm> result = userDao.findByLoginFirstnameLastnameStartsWith(tagName);
		
		assertNotNull result;
		assertEquals 1, result.size();
		assertEquals user2.getEntityId(), result.get(0).getId();
	}
	
	@Test
	@Rollback(true)
	public void findByLoginFirstnameLastnameStartsWithLoginTest() {
		String tagName = "e@f";
		List<UserAutocompleteForm> result = userDao.findByLoginFirstnameLastnameStartsWith(tagName);
		
		assertNotNull result;
		assertEquals 1, result.size();
		assertEquals user3.getEntityId(), result.get(0).getId();
	}
	
}
