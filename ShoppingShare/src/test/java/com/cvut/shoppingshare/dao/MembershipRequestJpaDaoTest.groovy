package com.cvut.shoppingshare.dao

import static org.junit.Assert.*

import java.util.List;

import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.annotation.Rollback
import org.springframework.transaction.annotation.Transactional

import com.cvut.shoppingshare.ShoppingShareGroovyTest
import com.cvut.shoppingshare.domain.MembershipRequest
import com.cvut.shoppingshare.domain.MembershipRequestState
import com.cvut.shoppingshare.domain.MembershipRequestType
import com.cvut.shoppingshare.domain.ShareGroup
import com.cvut.shoppingshare.domain.ShareUser
import com.cvut.shoppingshare.web.form.DealtMembershipRequest;
import com.cvut.shoppingshare.web.form.MembershipOfferDto;
import com.cvut.shoppingshare.web.form.MembershipRequestDto;
import com.cvut.shoppingshare.web.form.ShareUserDto;
import com.cvut.shoppingshare.web.form.ShareUserWebDto;

@Transactional
class MembershipRequestJpaDaoTest extends ShoppingShareGroovyTest {
	
	@Autowired
	MembershipRequestDao membershipRequestDao;
	
	@Autowired
	UserDao userDao;
	
	@Autowired
	GroupDao groupDao;
	
	private ShareGroup group1;
	
	private ShareUser nonDealtUser1;
	private ShareUser nonDealtUser2;
	private ShareUser dealtUser1;
	private ShareUser dealtUser2;
	
	private MembershipRequest nonDealtRequest1;
	private MembershipRequest nonDealtRequest2;
	private MembershipRequest dealtRequest1;
	private MembershipRequest dealtRequest2;
	
	@Before
	public void setUp() {
		nonDealtUser1 = new ShareUser(login:"a@a.cz", password:"aaaa", firstname:"Jiri", lastname:"Kysela");
		nonDealtUser2 = new ShareUser(login:"b@b.cz", password:"bbbb", firstname:"Jakub", lastname:"Matuska");
		dealtUser1 = new ShareUser(login:"c@c.cz", password:"cccc", firstname:"Petr", lastname:"Kujal");
		dealtUser2 = new ShareUser(login:"d@d.cz", password:"dddd", firstname:"Vasek", lastname:"Barvir");
		userDao.persist(nonDealtUser1);
		userDao.persist(nonDealtUser2);
		userDao.persist(dealtUser1);
		userDao.persist(dealtUser2);
		
		group1 = new ShareGroup(name:"group blue", created:getDateWithShift(-100));
		group1.addUser(nonDealtUser1);
		group1.addUser(nonDealtUser2);
		group1.addUser(dealtUser1);
		group1.addUser(dealtUser2);
		groupDao.persist(group1);
		
		nonDealtUser1.addGroup(group1);
		nonDealtUser2.addGroup(group1);
		dealtUser1.addGroup(group1);
		dealtUser2.addGroup(group1);
		
		nonDealtRequest1 = new MembershipRequest(group1, nonDealtUser1, MembershipRequestType.REQUEST, MembershipRequestState.NEW);
		nonDealtRequest1.setCreated(getDateWithShift(-60));
		nonDealtRequest2 = new MembershipRequest(group1, nonDealtUser2, MembershipRequestType.REQUEST, MembershipRequestState.NEW);
		nonDealtRequest2.setCreated(getDateWithShift(-40));
		dealtRequest1 = new MembershipRequest(group1, dealtUser1, MembershipRequestType.REQUEST, MembershipRequestState.ACCEPTED);
		dealtRequest1.setCreated(getDateWithShift(-80));
		dealtRequest1.setDealt(getDateWithShift(-70));
		dealtRequest2 = new MembershipRequest(group1, dealtUser2, MembershipRequestType.REQUEST, MembershipRequestState.ACCEPTED);
		dealtRequest2.setCreated(getDateWithShift(-80));
		dealtRequest2.setDealt(getDateWithShift(-20));
		
		membershipRequestDao.persist(nonDealtRequest1);
		membershipRequestDao.persist(nonDealtRequest2);
		membershipRequestDao.persist(dealtRequest1);
		membershipRequestDao.persist(dealtRequest2);
		
		nonDealtUser1.addMembershipRequest(nonDealtRequest1);
		nonDealtUser2.addMembershipRequest(nonDealtRequest2);
		dealtUser1.addMembershipRequest(dealtRequest1);
		dealtUser2.addMembershipRequest(dealtRequest2);
		
		userDao.update(nonDealtUser1);
		userDao.update(nonDealtUser2);
		userDao.update(dealtUser1);
		userDao.update(dealtUser2);
	}
	
	@After
	public void cleanUp() {
		membershipRequestDao.remove(nonDealtRequest1);
		membershipRequestDao.remove(nonDealtRequest2);
		membershipRequestDao.remove(dealtRequest1);
		membershipRequestDao.remove(dealtRequest2);
		
		userDao.remove(nonDealtUser1);
		userDao.remove(nonDealtUser2);
		userDao.remove(dealtUser1);
		userDao.remove(dealtUser2);
		
		groupDao.remove(group1);
	}

	@Test
	@Rollback(true)
	public void getNonDealtRequestsOfGroupSinceDateTest() {
		Date lastSync = getDateWithShift(-50);
		List<MembershipRequestDto> result = membershipRequestDao.getNonDealtRequestsOfGroupSinceDate(group1.getEntityId(), lastSync);
		
		assertNotNull result;
		assertEquals 1, result.size();
		
		MembershipRequestDto dto1 = result.get(0);
		assertEquals nonDealtRequest2.getEntityId(), dto1.getRequestId();
		assertEquals nonDealtRequest2.getMembershipRequestType(), dto1.getMembershipRequestType();
		assertEquals nonDealtUser2.getEntityId(), dto1.getUserId();
		assertEquals nonDealtUser2.getLogin(), dto1.getLogin();
		assertEquals nonDealtUser2.getFirstname(), dto1.getFirstname();
		assertEquals nonDealtUser2.getLastname(), dto1.getLastname();
	}
	
	@Test
	@Rollback(true)
	public void getDealtRequestsOfGroupSinceDateTest() {
		Date lastSync = getDateWithShift(-50);
		List<DealtMembershipRequest> result = membershipRequestDao.getDealtRequestsOfGroupSinceDate(group1.getEntityId(), lastSync);
		
		assertNotNull result;
		assertEquals 1, result.size();
		
		DealtMembershipRequest dto1 = result.get(0);
		assertEquals dealtRequest2.getEntityId(), dto1.getRequestId();
		assertEquals dealtRequest2.getMembershipRequestState(), dto1.getMembershipRequestState();
	}
	
	@Test
	@Rollback(true)
	public void getOffersOfUserSinceDateTest() {
		Date lastSync = getDateWithShift(-10);
		MembershipRequest oldOffer = new MembershipRequest(sharegroup:group1, shareuser:nonDealtUser1, created:getDateWithShift(-15), membershipRequestType:MembershipRequestType.OFFER);
		MembershipRequest newOffer = new MembershipRequest(sharegroup:group1, shareuser:nonDealtUser1, created:getDateWithShift(-5), membershipRequestType:MembershipRequestType.OFFER);
		membershipRequestDao.persist(oldOffer);
		membershipRequestDao.persist(newOffer);
		nonDealtUser1.addMembershipRequest(oldOffer);
		nonDealtUser1.addMembershipRequest(newOffer);
		userDao.update(nonDealtUser1);
		
		List<MembershipOfferDto> result = membershipRequestDao.getOffersOfUserSinceDate(nonDealtUser1.getEntityId(), lastSync);
		
		assertNotNull result;
		assertEquals 1, result.size();
		assertEquals group1.getName(), result.get(0).getGroupName();
		assertEquals newOffer.getEntityId(), result.get(0).requestId;
	}
	
	@Test
	@Rollback(true)
	public void getAllRequestsOfGroupWithIdTest() {
		List<ShareUserDto> result = membershipRequestDao.getAllRequestsOfGroupWithId(group1.getEntityId());
		
		assertNotNull result;
		assertEquals 2, result.size();
		assertTrue result.get(0).getUserId() == nonDealtRequest1.getEntityId() || result.get(0).getUserId() == nonDealtRequest2.getEntityId();
		assertTrue result.get(1).getUserId() == nonDealtRequest1.getEntityId() || result.get(1).getUserId() == nonDealtRequest2.getEntityId();
	}
	
	@Test
	@Rollback(true)
	public void getAllOffersOfGroupWithIdTest() {
		MembershipRequest offer = new MembershipRequest(sharegroup:group1, shareuser:nonDealtUser1, created:getDateWithShift(-15), membershipRequestType:MembershipRequestType.OFFER);
		membershipRequestDao.persist(offer);
		
		List<ShareUserDto> result = membershipRequestDao.getAllOffersOfGroupWithId(group1.getEntityId());
		assertNotNull result;
		assertEquals 1, result.size();
	}
	
	private Date getDateWithShift(int daysShift) {
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.set(Calendar.DAY_OF_MONTH, c.get(Calendar.DAY_OF_MONTH) + daysShift);
		return c.getTime();
	}
	
}
