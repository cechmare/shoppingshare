package com.cvut.shoppingshare.service

import static org.junit.Assert.*

import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.annotation.Rollback
import org.springframework.transaction.annotation.Transactional

import com.cvut.shoppingshare.ShoppingShareGroovyTest
import com.cvut.shoppingshare.dao.GroupDao
import com.cvut.shoppingshare.dao.PurchaseTemplateDao
import com.cvut.shoppingshare.dao.UserDao
import com.cvut.shoppingshare.domain.EntityState
import com.cvut.shoppingshare.domain.Item
import com.cvut.shoppingshare.domain.ItemState
import com.cvut.shoppingshare.domain.Purchase
import com.cvut.shoppingshare.domain.PurchaseTemplate
import com.cvut.shoppingshare.domain.ShareGroup
import com.cvut.shoppingshare.domain.ShareUser
import com.cvut.shoppingshare.web.form.EditEntityForm
import com.cvut.shoppingshare.web.form.PurchaseTemplateSourceEnum
import com.cvut.shoppingshare.web.form.PurchasesItemsDto
import com.cvut.shoppingshare.web.form.TemplateForm
import com.cvut.shoppingshare.web.form.TemplateFromExistingForm

@Transactional
class PurchaseTemplateServiceImplTest extends ShoppingShareGroovyTest {
	
	@Autowired
	PurchaseTemplateService purchaseTemplateService;
	
	@Autowired
	PurchaseTemplateDao purchaseTemplateDao;
	
	@Autowired
	UserDao userDao;
	
	@Autowired
	GroupDao groupDao;
	
	private ShareUser user;
	
	private ShareGroup group1;
	
	private Purchase purchase1;
	
	private PurchaseTemplate template1;
	private PurchaseTemplate template2;
	
	private Item item1;
	private Item item2;
	private Item item3;
	private Item item4;
	
	private Item item5;
	private Item item6;
	
	private String item1Name;
	private String item5Name;

	@Before
	public void setUp() {
		user = new ShareUser(login:"a@b.cz", password:"1234");
		
		group1 = new ShareGroup(name:"group blue", created:new Date());
		group1.addUser(user);
		
		purchase1 = new Purchase(group1, "Monday");
		group1.addPurchase(purchase1);
		
		item5Name = "beerkeg";
		item5 = new Item(purchase1, item5Name);
		item5.setState(ItemState.NEW);
		item6 = new Item(purchase1, "eclair");
		item6.setState(ItemState.DELETED);
		purchase1.addItem(item5);
		purchase1.addItem(item6);
		
		
		template1 = new PurchaseTemplate(user, "bakery", getDateWithShift(-20), EntityState.DELETED);
		template2 = new PurchaseTemplate(user, "weekend", getDateWithShift(-5), EntityState.NEW);
		user.addTemplate(template1);
		user.addTemplate(template2);
		
		item1Name = "donut";
		item1 = new Item(template1, item1Name, getDateWithShift(-2));
		item1.setState(ItemState.NEW);
		item2 = new Item(template1, "eclair", getDateWithShift(-15));
		item2.setState(ItemState.DELETED);
		template1.addItem(item1);
		template1.addItem(item2);
		
		item3 = new Item(template2, "beer", getDateWithShift(-13));
		item4 = new Item(template2, "sugar", getDateWithShift(-13));
		template2.addItem(item3);
		template2.addItem(item4);
		
		groupDao.persist(group1);
		userDao.persist(user);
		groupDao.update(group1);
		userDao.getEntityManager().flush();
	}
	
	@After
	public void cleanUp() {
		userDao.remove(user);
		groupDao.remove(group1);
	}
	
	@Test
	@Rollback(true)
	public void getNewsTest() {
		Date lastSync = getDateWithShift(-10);
		PurchasesItemsDto result = purchaseTemplateService.getNews(lastSync, user.getEntityId());
		
		assertNotNull result;
		assertNotNull result.getPurchases();
		assertNotNull result.getItems();
		assertEquals 1, result.getPurchases().size();
		assertEquals 1, result.getItems().size();
		assertEquals template2.getEntityId(), result.getPurchases().get(0).getPurchaseId();
		assertEquals item1.getEntityId(), result.getItems().get(0).getItemId();
	}
	
	@Test
	@Rollback(true)
	public void addPurchaseSourceTest() {
		String templateName = "template blue";
		TemplateFromExistingForm form = new TemplateFromExistingForm(purchase1.getEntityId(), PurchaseTemplateSourceEnum.PURCHASE, templateName);
		boolean success = purchaseTemplateService.add(form, user.getEntityId());
		
		userDao.getEntityManager().flush();
		assertEquals true, success;
		assertNotNull user.getTemplates();
		assertEquals 3, user.getTemplates().size();
		
		PurchaseTemplate addedTemplate = null;
		for (PurchaseTemplate template: user.getTemplates()) {
			if (template.getEntityId() != template1.getEntityId() && template.getEntityId() != template2.getEntityId()) {
				addedTemplate = template;
			}
		}
		assertNotNull addedTemplate;
		assertNotNull addedTemplate.getItems();
		assertEquals 1, addedTemplate.getItems().size();
		assertEquals item5Name, addedTemplate.getItems().get(0).getName();
	}
	
	@Test
	@Rollback(true)
	public void addTemplateSourceTest() {
		String templateName = "template green";
		TemplateFromExistingForm form = new TemplateFromExistingForm(template1.getEntityId(), PurchaseTemplateSourceEnum.TEMPLATE, templateName);
		boolean success = purchaseTemplateService.add(form, user.getEntityId());
		
		userDao.getEntityManager().flush();
		assertEquals true, success;
		assertNotNull user.getTemplates();
		assertEquals 3, user.getTemplates().size();
		
		PurchaseTemplate addedTemplate = null;
		for (PurchaseTemplate template: user.getTemplates()) {
			if (template.getEntityId() != template1.getEntityId() && template.getEntityId() != template2.getEntityId()) {
				addedTemplate = template;
			}
		}
		assertNotNull addedTemplate;
		assertNotNull addedTemplate.getItems();
		assertEquals 1, addedTemplate.getItems().size();
		assertEquals item1Name, addedTemplate.getItems().get(0).getName();
	}
	
	@Test
	@Rollback(true)
	public void addFormSourceTest() {
		String templateName = "template red";
		List<String> items = ["car", "bike"];
		TemplateForm form = new TemplateForm(templateName, items);
		boolean success = purchaseTemplateService.add(form, user.getEntityId());
		
		userDao.getEntityManager().flush();
		assertEquals true, success;
		assertNotNull user.getTemplates();
		assertEquals 3, user.getTemplates().size();
		
		PurchaseTemplate addedTemplate = null;
		for (PurchaseTemplate template: user.getTemplates()) {
			if (template.getEntityId() != template1.getEntityId() && template.getEntityId() != template2.getEntityId()) {
				addedTemplate = template;
			}
		}
		assertNotNull addedTemplate;
		assertNotNull addedTemplate.getItems();
		assertEquals 2, addedTemplate.getItems().size();
		assertEquals templateName, addedTemplate.getName();
	}
	
	@Test
	@Rollback(true)
	public void markAsRemovedTest() {
		Date created = template1.getUpdated();
		purchaseTemplateService.markAsRemoved(template1.getEntityId())
		
		assertNotNull template1;
		assertNotNull template1.getItems();
		assertEquals 0, template1.getItems().size();
		assertNotNull template1.getUpdated();
		assertTrue created.getTime() < template1.getUpdated().getTime();
		assertNotNull template1.getState();
		assertEquals EntityState.DELETED, template1.getState();
	}
	
	@Test
	@Rollback(true)
	public void editTest() {
		Date created = template2.getUpdated();
		
		String modifiedName = "Vikend";
		EditEntityForm editForm = new EditEntityForm(getDateWithShift(-2), modifiedName, template2.getEntityId());
		boolean success = purchaseTemplateService.edit(editForm);
		
		assertTrue success;
		assertNotNull template2;
		assertTrue created.getTime() < template2.getUpdated().getTime()
		assertEquals modifiedName, template2.getName();
	}
	
	private Date getDateWithShift(int daysShift) {
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.set(Calendar.DAY_OF_MONTH, c.get(Calendar.DAY_OF_MONTH) + daysShift);
		return c.getTime();
	}
	
}
