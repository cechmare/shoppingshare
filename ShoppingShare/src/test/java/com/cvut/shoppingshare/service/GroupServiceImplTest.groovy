package com.cvut.shoppingshare.service

import static org.junit.Assert.*

import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.annotation.Rollback
import org.springframework.transaction.annotation.Transactional

import com.cvut.shoppingshare.ShoppingShareGroovyTest
import com.cvut.shoppingshare.dao.GroupDao
import com.cvut.shoppingshare.dao.MembershipRequestDao;
import com.cvut.shoppingshare.dao.UserDao
import com.cvut.shoppingshare.domain.MembershipRequest
import com.cvut.shoppingshare.domain.MembershipRequestState
import com.cvut.shoppingshare.domain.MembershipRequestType
import com.cvut.shoppingshare.domain.ShareGroup
import com.cvut.shoppingshare.domain.ShareUser
import com.cvut.shoppingshare.web.form.GroupForm
import com.cvut.shoppingshare.web.form.GroupListMessage

@Transactional
class GroupServiceImplTest extends ShoppingShareGroovyTest {
	
	@Autowired
	GroupService groupService;
	
	@Autowired
	UserDao userDao;
	
	@Autowired
	GroupDao groupDao;
	
	@Autowired
	MembershipRequestDao membershipRequestDao;
	
	private ShareUser user1;
	private ShareUser user2;
	
	private ShareGroup group1;
	private ShareGroup group2;
	
	@Before
	public void setUp() {
		user1 = new ShareUser(login:"a@b.cz", password:"1234");
		userDao.persist(user1);
		
		user2 = new ShareUser(login:"c@d.cz", password:"4321");
		userDao.persist(user2);
		
		group1 = new ShareGroup(name:"group blue", created:new Date());
		group1.addUser(user1);
		groupDao.persist(group1);
		
		group2 = new ShareGroup(name:"group red", created:new Date());
		group2.addUser(user2);
		groupDao.persist(group2);
		
		user1.addGroup(group1);
		userDao.update(user1);
		
		user2.addGroup(group2);
		userDao.update(user2);
	}
	
	@After
	public void cleanUp() {
		userDao.remove(user1);
		userDao.remove(user2);
		
		groupDao.remove(group1);
		groupDao.remove(group2);
	}

	@Test
	@Rollback(true)
	public void getAllGroupsWhereParticipateTest() {
		GroupListMessage[] result1 = groupService.getAllGroupsWhereParticipate(user1.getEntityId());
		GroupListMessage[] result2 = groupService.getAllGroupsWhereParticipate(user2.getEntityId());
		
		assertEquals 1, result1.size();
		assertEquals 1, result2.size();
		assertEquals group1.getName(), result1[0].getName();
		assertEquals group2.getName(), result2[0].getName();
	}
	
	@Test
	@Rollback(true)
	public void getAllGroupsWhereParticipateSinceDateNewGroupTest() {
		ShareGroup group3 = new ShareGroup(name:"group white", created:getDateWithShift(-2));
		ShareGroup group4 = new ShareGroup(name:"group yellow", created:getDateWithShift(-10));
		groupDao.persist(group3);
		groupDao.persist(group4);
		user1.addGroup(group3);
		user1.addGroup(group4);
		userDao.update(user1);
		
		Date lastSync = getDateWithShift(-1);
		GroupListMessage[] result = groupService.getAllGroupsWhereParticipateSinceDate(user1.getEntityId(), lastSync);
		
		assertEquals 1, result.size();
		assertNotNull result[0];
		assertEquals group1.getName(), result[0].getName();
	}
	
	@Test
	@Rollback(true)
	public void getAllGroupsWhereParticipateSinceDateAcceptedRequestTest() {
		Date lastSync = getDateWithShift(-10);
		
		ShareGroup group3 = new ShareGroup(name:"group purple", created:getDateWithShift(-20));
		groupDao.persist(group3);
		
		MembershipRequest dealtRequest1 = new MembershipRequest(group3, user1, MembershipRequestType.REQUEST, MembershipRequestState.ACCEPTED);
		dealtRequest1.setCreated(getDateWithShift(-19));
		dealtRequest1.setDealt(getDateWithShift(-5));
		membershipRequestDao.persist(dealtRequest1);
		
		user1.addMembershipRequest(dealtRequest1);
		userDao.update(user1);
		
		GroupListMessage[] result = groupService.getAllGroupsWhereParticipateSinceDate(user1.getEntityId(), lastSync);
		assertEquals 2, result.size();
	}
	
	@Test
	@Rollback(true)
	public void addTest() {
		GroupForm form = new GroupForm(name:"group black", userIdsToInvite:[user2.getEntityId()]);
		Long groupId = groupService.add(user1.getEntityId(), form);
		userDao.getEntityManager().flush();
		
		assertNotNull groupId;
		assertEquals 2, user1.getGroups().size();
		assertEquals 1, user2.getMembershipRequests().size();
	}
	
	@Test
	@Rollback(true)
	public void addTestUserIdsNull() {
		GroupForm form = new GroupForm(name:"group black", userIdsToInvite:null);
		Long groupId = groupService.add(user1.getEntityId(), form);
		userDao.getEntityManager().flush();
		
		assertNotNull groupId;
		assertEquals 2, user1.getGroups().size();
		assertEquals 0, user2.getMembershipRequests().size();
	}
	
	@Test
	@Rollback(true)
	public void addTestIdToInviteUnknown() {
		GroupForm form = new GroupForm(name:"group black", userIdsToInvite:[new Long(987654321)]);
		Long groupId = groupService.add(user1.getEntityId(), form);
		userDao.getEntityManager().flush();
		
		assertNotNull groupId;
		assertEquals 2, user1.getGroups().size();
		assertEquals 0, user2.getMembershipRequests().size();
	}
	
	private Date getDateWithShift(int daysShift) {
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.set(Calendar.DAY_OF_MONTH, c.get(Calendar.DAY_OF_MONTH) + daysShift);
		return c.getTime();
	}
}
