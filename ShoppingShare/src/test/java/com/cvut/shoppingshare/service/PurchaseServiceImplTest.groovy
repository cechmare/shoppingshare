package com.cvut.shoppingshare.service

import static org.junit.Assert.*

import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.annotation.Rollback
import org.springframework.transaction.annotation.Transactional

import com.cvut.shoppingshare.ShoppingShareGroovyTest
import com.cvut.shoppingshare.dao.GroupDao
import com.cvut.shoppingshare.dao.PurchaseDao
import com.cvut.shoppingshare.dao.UserDao
import com.cvut.shoppingshare.domain.EntityState
import com.cvut.shoppingshare.domain.Item
import com.cvut.shoppingshare.domain.Purchase
import com.cvut.shoppingshare.domain.ShareGroup
import com.cvut.shoppingshare.domain.ShareUser
import com.cvut.shoppingshare.web.form.EditEntityForm
import com.cvut.shoppingshare.web.form.EntityForm
import com.cvut.shoppingshare.web.form.NewsRequestMessage
import com.cvut.shoppingshare.web.form.PurchaseWebDto;
import com.cvut.shoppingshare.web.form.PurchasesItemsDto

@Transactional
class PurchaseServiceImplTest extends ShoppingShareGroovyTest {
	
	@Autowired
	PurchaseService purchaseService;
	
	@Autowired
	PurchaseDao purchaseDao;
	
	@Autowired
	UserDao userDao;
	
	@Autowired
	GroupDao groupDao;
	
	private ShareUser user;
	
	private ShareGroup group1;
	private ShareGroup group2;
	
	private Purchase purchase1;
	private Purchase purchase2;
	private Purchase purchase3;
	
	private Item item1;
	private Item item2;
	private Item item3;
	
	@Before
	public void setUp() {
		// 2 groups of 1 user
		user = new ShareUser(login:"a@b.cz", password:"1234");
		userDao.persist(user);
		
		// group1
		group1 = new ShareGroup(name:"group blue", created:new Date());
		group1.addUser(user);
		
		// 1 purchase of group1
		purchase1 = new Purchase(group1, "Monday");
		group1.addPurchase(purchase1);
		
		// 1 item of purchase1
		item1 = new Item(purchase1, "donuts");
		purchase1.addItem(item1);
		
		groupDao.persist(group1);
		
		// group2
		group2 = new ShareGroup(name:"group red", created:new Date());
		group2.addUser(user);
		
		// 2 purchases of group2
		purchase2 = new Purchase(group2, "Tuesday");
		purchase2.setUpdated(getDateWithShift(-5));
		purchase3 = new Purchase(group2, "Wednesday");
		purchase3.setUpdated(getDateWithShift(-15));
		group2.addPurchase(purchase2);
		group2.addPurchase(purchase3);
		
		// 2 items of purchase2, 0 items of purchase3
		item2 = new Item(purchase2, "beer");
		item2.setUpdated(getDateWithShift(-20));
		item3 = new Item(purchase2, "eclair");
		item3.setUpdated(getDateWithShift(-3));
		purchase2.addItem(item2);
		purchase2.addItem(item3);
		
		groupDao.persist(group2);
		
		//user update
		user.addGroup(group1);
		user.addGroup(group2);
		userDao.update(user);
		userDao.getEntityManager().flush();
	}
	
	@After
	public void cleanUp() {
		userDao.remove(user);
		
		groupDao.remove(group1);
		groupDao.remove(group2);
	}
	
	@Test
	@Rollback(true)
	public void getAllPurchasesOfGroupTest() {
		PurchaseWebDto[] group1Purchases = purchaseService.getAllPurchasesOfGroup(group1.getEntityId());
		PurchaseWebDto[] group2Purchases = purchaseService.getAllPurchasesOfGroup(group2.getEntityId());
		
		assertNotNull group1Purchases;
		assertNotNull group2Purchases;
		assertEquals 1, group1Purchases.length;
		assertEquals 2, group2Purchases.length;
	}
	
	@Test
	@Rollback(true)
	public void getPurchasesItemsNewsTest() {
		Date lastSync = getDateWithShift(-10);
		NewsRequestMessage request = new NewsRequestMessage(lastSync, group2.getEntityId());
		PurchasesItemsDto news = purchaseService.getPurchasesItemsNews(request);
		
		assertNotNull news;
		assertNotNull news.getPurchases();
		assertNotNull news.getItems();
		assertEquals 1, news.getPurchases().size();
		assertEquals 1, news.getItems().size();
		assertEquals purchase2.getEntityId(), news.getPurchases().get(0).getPurchaseId();
		assertEquals item3.getEntityId(), news.getItems().get(0).getItemId();
	}
	
	@Test
	@Rollback(true)
	public void addTest() {
		String name = "butchman";
		Long groupId = group1.getEntityId();
		EntityForm form = new EntityForm(name, groupId);
		Long purchaseId = purchaseService.add(form);
		
		assertNotNull purchaseId;
		
		Purchase purchase = purchaseService.findById(purchaseId);
		assertEquals groupId, purchase.getSharegroup().getEntityId();
		assertEquals name, purchase.getName();
	}
	
	@Test
	@Rollback(true)
	public void markAsRemovedTest() {
		Date created = purchase2.getUpdated();
		purchaseService.markAsRemoved(purchase2.getEntityId());
		
		assertNotNull purchase2;
		assertNotNull purchase2.getItems();
		assertEquals 0, purchase2.getItems().size();
		assertNotNull purchase2.getUpdated();
		assertTrue created.getTime() < purchase2.getUpdated().getTime();
		assertNotNull purchase2.getState();
		assertEquals EntityState.DELETED, purchase2.getState();
	}
	
	@Test
	@Rollback(true)
	public void editTest() {
		Date created = purchase2.getUpdated();
		
		String modifiedName = "Utery"; 
		EditEntityForm editForm = new EditEntityForm(getDateWithShift(-2), modifiedName, purchase2.getEntityId());
		boolean success = purchaseService.edit(editForm);
		
		assertTrue success;
		assertNotNull purchase2;
		assertTrue created.getTime() < purchase2.getUpdated().getTime()
		assertEquals modifiedName, purchase2.getName();
	}
	
	private Date getDateWithShift(int daysShift) {
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.set(Calendar.DAY_OF_MONTH, c.get(Calendar.DAY_OF_MONTH) + daysShift);
		return c.getTime();
	}

}
