package com.cvut.shoppingshare.service

import static org.junit.Assert.*

import java.util.Date;

import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.annotation.Rollback
import org.springframework.transaction.annotation.Transactional

import com.cvut.shoppingshare.ShoppingShareGroovyTest
import com.cvut.shoppingshare.dao.ItemDao;
import com.cvut.shoppingshare.dao.PurchaseDao
import com.cvut.shoppingshare.dao.UserDao;
import com.cvut.shoppingshare.domain.Item
import com.cvut.shoppingshare.domain.Purchase
import com.cvut.shoppingshare.domain.PurchaseTemplate;
import com.cvut.shoppingshare.domain.ShareUser;
import com.cvut.shoppingshare.web.form.EditEntityForm;
import com.cvut.shoppingshare.web.form.EntityForm

@Transactional
class ItemServiceImplTest extends ShoppingShareGroovyTest {
	
	@Autowired
	PurchaseDao purchaseDao
	
	@Autowired
	ItemDao itemDao
	
	@Autowired
	ItemService itemService
	
	@Autowired
	UserDao userDao
	
	private ShareUser user
	
	private PurchaseTemplate template
	
	private Purchase purchase
	
	@Before
	public void setUp() {
		user = new ShareUser(login:"a@b.cz", password:"1234")
		template = new PurchaseTemplate(user, "weekend")
		user.addTemplate(template)
		userDao.persist(user)
		userDao.getEntityManager().flush()
		
		
		purchase = new Purchase(name:"weekend")
		purchaseDao.persist(purchase)
	}
	
	@After
	public void cleanUp() {
		userDao.remove(user)
		purchaseDao.remove(purchase)
	}

	@Test
	@Rollback(true)
	public void addItemTest() {
		EntityForm itemForm = new EntityForm("donut", purchase.entityId)
		Long itemId = itemService.addItem(itemForm)
		
		assertNotNull purchase.getItems()
		assertEquals 1, purchase.getItems().size()
		assertEquals itemId, purchase.getItems().get(0).getEntityId()
	}
	
	@Test
	@Rollback(true)
	public void markAsRemovedTest() {
		String name = "pie"
		EntityForm itemForm = new EntityForm(name, purchase.entityId)
		Long itemId = itemService.addItem(itemForm)
		Item item = itemService.findById(itemId)
		Date persistedDate = item.getUpdated()
		
		boolean successfullyRemoved = itemService.markAsRemoved(itemId)
		itemDao.entityManager.flush()
		itemDao.entityManager.refresh(item)
		
		assertEquals true, successfullyRemoved
		assertNotNull item
		assertEquals itemId, item.getEntityId()
		assertEquals name, item.getName()
		assertTrue persistedDate.getTime() < item.getUpdated().getTime()
	}
	
	@Test
	@Rollback(true)
	public void addItemTemplateTest() {
		String itemName = "cucumber"
		EntityForm form = new EntityForm(itemName, purchase.getEntityId())
		Long itemId = itemService.addItemTemplate(form)
		
		assertNotNull itemId
		assertNotNull template
		assertNotNull template.getItems()
		assertEquals 1, template.getItems().size()
		assertEquals itemName, template.getItems().get(0).getName()
	}
	
	@Test
	@Rollback(true)
	public void editItem() {
		String itemName = "cucumber"
		EntityForm form = new EntityForm(itemName, purchase.getEntityId())
		Long itemId = itemService.addItemTemplate(form)
		Item item = itemService.findById(itemId)
		Date created = item.getUpdated()
		
		String modifiedName = "big cucumber"
		EditEntityForm editForm = new EditEntityForm(getDateWithShift(+1), modifiedName, itemId)
		boolean success = itemService.editItem(editForm)
		
		assertTrue success
		assertNotNull item
		assertTrue created.getTime() < item.getUpdated().getTime()
		assertEquals modifiedName, item.getName()
	}
	
	private Date getDateWithShift(int daysShift) {
		Calendar c = Calendar.getInstance()
		c.setTime(new Date())
		c.set(Calendar.DAY_OF_MONTH, c.get(Calendar.DAY_OF_MONTH) + daysShift)
		return c.getTime()
	}
	
}
