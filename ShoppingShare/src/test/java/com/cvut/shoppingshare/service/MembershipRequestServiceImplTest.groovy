package com.cvut.shoppingshare.service

import static org.junit.Assert.*

import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.annotation.Rollback
import org.springframework.transaction.annotation.Transactional

import com.cvut.shoppingshare.ShoppingShareGroovyTest
import com.cvut.shoppingshare.dao.GroupDao
import com.cvut.shoppingshare.dao.MembershipRequestDao
import com.cvut.shoppingshare.dao.UserDao
import com.cvut.shoppingshare.domain.MembershipRequest
import com.cvut.shoppingshare.domain.MembershipRequestState
import com.cvut.shoppingshare.domain.MembershipRequestType
import com.cvut.shoppingshare.domain.ShareGroup
import com.cvut.shoppingshare.domain.ShareUser

@Transactional
class MembershipRequestServiceImplTest extends ShoppingShareGroovyTest {

	@Autowired
	MembershipRequestService membershipRequestService;
	
	@Autowired
	MembershipRequestDao membershipRequestDao;
	
	@Autowired
	UserDao userDao;
	
	@Autowired
	GroupDao groupDao;
	
	private ShareGroup group1;
	
	private ShareUser user1;
	
	private MembershipRequest request1;
	
	@Before
	public void setUp() {
		user1 = new ShareUser(login:"a@a.cz", password:"aaaa", firstname:"Jiri", lastname:"Kysela");
		userDao.persist(user1);
		
		group1 = new ShareGroup(name:"group brown", created:new Date());
		groupDao.persist(group1);
		
		request1 = new MembershipRequest(group1, user1, MembershipRequestType.REQUEST, MembershipRequestState.NEW);
		membershipRequestDao.persist(request1);
		
		user1.addMembershipRequest(request1);
		userDao.update(user1);
	}
	
	@After
	public void cleanUp() {
		membershipRequestDao.remove(request1);
		userDao.remove(user1);
		groupDao.remove(group1);
	}
	
	@Test
	@Rollback(true)
	public void acceptTest() {
		boolean result = membershipRequestService.accept(request1.getEntityId());
		
		assertTrue result;
		assertEquals 1, group1.getUsers().size();
		
		ShareUser addedUser = group1.getUsers().get(0);
		assertNotNull addedUser;
		assertEquals user1.getEntityId(), addedUser.getEntityId();
	}
	
	@Test
	@Rollback(true)
	public void rejectTest() {
		boolean result = membershipRequestService.reject(request1.getEntityId());
		
		assertTrue result;
		assertEquals 0, group1.getUsers().size();
		assertEquals 0, user1.getGroups().size();
	}
	
}
