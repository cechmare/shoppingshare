insert into membershiprequest (entityid, created, dealt, membershiprequeststate, membershiprequesttype, sharegroup_id, shareuser_id) values (1, '2014-01-02 10:10:10', NULL, 'NEW', 'OFFER', 1, 2);
insert into membershiprequest (entityid, created, dealt, membershiprequeststate, membershiprequesttype, sharegroup_id, shareuser_id) values (2, '2014-01-02 10:10:10', NULL, 'NEW', 'OFFER', 1, 3);
insert into membershiprequest (entityid, created, dealt, membershiprequeststate, membershiprequesttype, sharegroup_id, shareuser_id) values (3, '2014-01-02 10:10:10', NULL, 'NEW', 'REQUEST', 1, 4);
insert into membershiprequest (entityid, created, dealt, membershiprequeststate, membershiprequesttype, sharegroup_id, shareuser_id) values (4, '2014-01-02 10:10:10', NULL, 'NEW', 'REQUEST', 1, 5);

INSERT INTO hibernate_sequences (sequence_name,sequence_next_hi_value) VALUES ('membershiprequest', 4);