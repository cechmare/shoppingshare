insert into shareuser (entityid, firstname, lastname, login, password) values (1, 'Jiří', 'Kysela', 'a@a.com', 'aaaa');
insert into shareuser (entityid, firstname, lastname, login, password) values (2, 'Petr', 'Kujal', 'b@b.com', 'bbbb');
insert into shareuser (entityid, firstname, lastname, login, password) values (3, 'Jakub', 'Matuška', 'c@c.com', 'cccc');
insert into shareuser (entityid, firstname, lastname, login, password) values (4, 'Vašek', 'Barvíř', 'd@d.com', 'dddd');
insert into shareuser (entityid, firstname, lastname, login, password) values (5, 'Filip', 'Řehák', 'e@e.com', 'eeee');

INSERT INTO hibernate_sequences (sequence_name,sequence_next_hi_value) VALUES ('shareuser', 5);
