<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
  	<head>
  		<meta charset="utf-8">  		
  		<title>ShoppingShare</title>
  		<link type="text/css" href="${pageContext.request.contextPath}/gui/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  		<link type="text/css" href="${pageContext.request.contextPath}/gui/css/screen.css" rel="stylesheet">
	</head>
	<header>
	</header>
	<body>		
		<div id="login-page">
			<div class="page-content">
				<h1>ShoppingShare</h1>
				<h2>Shared shopping easy like never before.</h2>
				
				<div class="panel panel-default" id="login-frame">
	  				<div class="panel-body">
						<ul class="nav nav-tabs">
							<li class="active">
								<a href="#login" data-toggle="tab">Log in</a>
							</li>
							<li>
								<a href="#signup" data-toggle="tab">Sign up</a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane fade in active" id="login">
								<form class="form" action="<c:url value='j_spring_security_check' />" method='POST'>
									<input id="login-email" type="email" class="form-control" placeholder="Email" name='j_username'>
									<input id="login-password" type="password" class="form-control" placeholder="Password" name='j_password'>
									<button type="submit" name="submit" value="submit" class="btn btn-success">Send</button>
								</form>
							</div>
							<div class="tab-pane fade" id="signup">
								<form class="form" action="<c:url value='signup' />" method='POST' id="signup">
									<input id="signup-email" type="email" class="form-control" placeholder="Email" name='email'>
									<input id="signup-password" type="password" class="form-control" placeholder="Password" name='password'>
									<input id="signup-password-again" type="password" class="form-control" placeholder="Password again" name='passwordAgain'>
									<button type="submit" name="submit" value="submit" class="btn btn-success">Send</button>
								</form>
							</div>
						</div>
					</div>
				</div>
					
			</div>
		</div>
	</body>
  	<footer>
  	</footer>
  	<script src="${pageContext.request.contextPath}/gui/bootstrap/js/jquery-1.10.2.min.js"></script>
	<script src="${pageContext.request.contextPath}/gui/bootstrap/js/bootstrap.min.js"></script>
  	<script src="${pageContext.request.contextPath}/gui/gui/js/scripts.js"></script>
</html>
