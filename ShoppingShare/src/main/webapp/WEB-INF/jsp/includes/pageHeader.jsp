<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<nav class="navbar navbar-default navbar-static-top" role="navigation">

	<div class="navbar-left page-header-left" id="header">
		<a href="<c:url value="/" />">
			<h3>ShoppingShare</h3>
		</a>
	</div>
	
	<div class="navbar-right page-header-right">
		<a href="<c:url value="/" />" class="btn btn-default navbar-btn">
			<span class="glyphicon glyphicon-shopping-cart"></span> Purchases
		</a>
		<a href="<c:url value="/members" />" class="btn btn-default navbar-btn">
			<span class="glyphicon glyphicon-user"></span> Users
		</a>
		<div class="btn-group">
		    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
		      Group <span class="caret"></span>
		    </button>
		    <ul class="dropdown-menu" role="menu">
		    	<li><a href="#" data-toggle="modal" data-target="#createGroupModal"><span class="glyphicon glyphicon-plus"></span> Create</a></li>
		    	<li><a href="#" data-toggle="modal" data-target="#searchGroupModal"><span class="glyphicon glyphicon-search"></span> Search</a></li>
		    	<li><a href="<c:url value="/offers" />"><span class="glyphicon glyphicon-link"></span> Offers</a></li>
		    </ul>
		</div>
		<div class="btn-group">
		    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
		      Other <span class="caret"></span>
		    </button>
		    <ul class="dropdown-menu" role="menu">
		    	<li><a href="<c:url value="/templates" />"><span class="glyphicon glyphicon-floppy-disk"></span> Templates</a></li>
		    	<li><a href="#"><span class="glyphicon glyphicon-heart-empty"></span> Personal</a></li>
		    </ul>
		</div>
		<a href="<c:url value="/j_spring_security_logout" />" class="btn btn-default navbar-btn">
			<span class="glyphicon glyphicon-log-out"></span> Logout
		</a>
	</div>

</nav>

<jsp:include page="/WEB-INF/jsp/includes/modal/pageHeaderModals.jsp" />
