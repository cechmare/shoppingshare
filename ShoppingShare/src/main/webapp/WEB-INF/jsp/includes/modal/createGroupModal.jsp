<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="modal fade" id="createGroupModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Create group</h4>
			</div>
			<div class="modal-body">
				<div id="createGroupAlertPlace">
				
				</div>
			    <div class="input-group create-group-modal-element">
				    <input type="text" class="form-control" placeholder="Name" id="createGroupName">
				</div>
				<div class="input-group create-group-modal-element">
					<input type="text" class="form-control" placeholder="User to invite" id="createGroupModalAddUserSearch" value="">
				</div>
				<div class="create-group-modal-element">
					<ul class="list-group" id="createGroupModalAddedUsers">
						
					</ul>
					<button type="button" class="btn btn-default hidden" id="createGroupModalRemoveUser">
						<span class="glyphicon glyphicon-trash"></span> Remove selected
					</button>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary" onclick="processCreateGroupForm()">Send</button>
			</div>
		</div>
	</div>
</div>