<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="modal fade" id="searchGroupModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Search group</h4>
			</div>
			<div class="modal-body">
				<div id="searchGroupAlertPlace">
				
				</div>
				<input type="text" class="form-control" placeholder="Group's name" id="searchGroupModalSearch" value="">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary" id="searchGroupModalSendRequest">Send request</button>
			</div>
		</div>
	</div>
</div>