<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="modal fade" id="createPurchaseFromTemplateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Create purchase from template</h4>
			</div>
			<div class="modal-body">
				<div id="createPurchaseFromTemplateAlertPlace">
				
				</div>
				<div class="input-group create-group-modal-element">
				    <input type="text" class="form-control" placeholder="Purchase name" id="purchaseFromTemplateName">
				</div>
				<div class="create-group-modal-element">
					<div class="row">
						<div class="col-md-5">
							<ul class="nav nav-pills nav-stacked" id="createPurchaseFromTemplateTemplates">
							
							</ul>
						</div>
						<div class="col-md-5" id="createPurchaseFromTemplateItems">
						
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary" onclick="createPurchaseFromTemplateSendButtonPressed()">Send</button>
			</div>
		</div>
	</div>
</div>