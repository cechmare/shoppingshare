<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:content>
	<jsp:body>
		<ul class="nav nav-tabs" id="membersRequestsOffersTabs" hidden="true">
		  <li id="membersTab"><a href="#">Members</a></li>
		  <li id="requestsTab"><a href="#">Requests</a></li>
		  <li id="offersTab"><a href="#">Offers</a></li>
		</ul>
		<div class="panel-body">
			<div class="row"> 			
      			<div class="col-md-9" id="mainPanelContent">
      		
      			</div>
      			<div class="col-md-3">
      				<div class="input-group" id="inviteUserInputGroup" hidden="true">
						<input type="text" class="form-control" placeholder="invite..." id="searchUserToInviteInput" value="">
      				</div>
      				<div id="membersActionButtons">
      				
      				</div>
      			</div>
      		</div>
      	</div>
	</jsp:body>
</t:content>