<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:content>
	<jsp:body>
		<div class="panel-body">
			<div class="row"> 
				<div class="col-md-9" id="mainPanelContent">
      		
      			</div>
      			<div class="col-md-3" id="offersActionButtons">
      			
      			</div>
			</div>
		</div>
	</jsp:body>
</t:content>