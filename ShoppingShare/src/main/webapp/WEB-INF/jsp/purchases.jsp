<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
    
<t:content>
	<jsp:body>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-9">
					<div class="row" id="mainPanelLabel"></div>
      				<div class="row" id="mainPanelContent"></div>
      			</div>
      			<div class="col-md-3" id="purchasesActionButtons">
      				<div class="row" id="purchasesDropdownDiv"></div>
      				<div class="row margintop10" id="addPurchaseDiv"></div>
      				<div class="row margintop10" id="editPurchaseDiv"></div>
      				<div class="row margintop10" id="deletePurchaseDiv"></div>
      				<hr>
      				<div class="row margintop10" id="boughtItemDiv"></div>
      				<div class="row margintop10" id="addItemDiv"></div>
      				<div class="row margintop10" id="editItemDiv"></div>
      				<div class="row margintop10" id="deleteItemDiv"></div>
      			</div>
			</div>
		</div>
      	<jsp:include page="/WEB-INF/jsp/includes/modal/deletePurchaseModal.jsp" />
      	<jsp:include page="/WEB-INF/jsp/includes/modal/createPurchaseFromTemplateModal.jsp" />
	</jsp:body>
</t:content>