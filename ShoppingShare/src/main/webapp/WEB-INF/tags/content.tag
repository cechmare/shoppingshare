<%@tag description="Overall Page template" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
  	<head>
  		<meta charset="utf-8">  		
  		<title>ShoppingShare</title>
  		<link type="text/css" href="${pageContext.request.contextPath}/gui/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  		<link type="text/css" href="${pageContext.request.contextPath}/gui/bootstrap/css/jquery-ui-1.10.4.custom.css" rel="stylesheet">
  		<link type="text/css" href="${pageContext.request.contextPath}/gui/css/screen.css" rel="stylesheet">
	</head>
	<header>
		<jsp:include page="/WEB-INF/jsp/includes/pageHeader.jsp" />
	</header>
	<body>		
   		<div class="container bs-docs-container">
			<div class="row">
				<div class="col-md-2" id="menuPanel">
				
				</div>
				<div class="col-md-9" id="mainContent">
					<div id="alertPlace">
					
					</div>
					<jsp:doBody/>
				</div>
		  	</div>
	  	</div>	  
  	</body>
  	<footer>
<%--     	<jsp:include page="/WEB-INF/jsp/includes/footer.jsp" /> --%>
  	</footer>
  	<script src="${pageContext.request.contextPath}/gui/bootstrap/js/jquery-1.10.2.min.js"></script>
  	<script src="${pageContext.request.contextPath}/gui/bootstrap/js/jquery-ui-1.10.4.custom.js"></script>
	<script src="${pageContext.request.contextPath}/gui/bootstrap/js/bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/gui/js/pageHeaderScripts.js"></script>
	
	<c:choose>
		<c:when test="${requestScope['javax.servlet.forward.servlet_path'].equals('/')}">
  			<script src="${pageContext.request.contextPath}/gui/js/purchasesScripts.js"></script>
		</c:when>
		<c:when test="${requestScope['javax.servlet.forward.servlet_path'].equals('/members')}">
			<script src="${pageContext.request.contextPath}/gui/js/membersScripts.js"></script>
		</c:when>
		<c:when test="${requestScope['javax.servlet.forward.servlet_path'].equals('/offers')}">
			<script src="${pageContext.request.contextPath}/gui/js/offersScripts.js"></script>
		</c:when>
		<c:when test="${requestScope['javax.servlet.forward.servlet_path'].equals('/templates')}">
			<script src="${pageContext.request.contextPath}/gui/js/templatesScripts.js"></script>
		</c:when>
	</c:choose>
	
</html>
