var baseUrl = "https://localhost:8443/ShoppingShare/";

var groups = [ ];

var createGroupModalAddUserSearchSource  = [ ];
var createGroupModalSelectedUsers = [ ];

var searchGroupModalSelectedGroup = null;

$(document).ready(function() {
	
	$('#createGroupModalAddUserSearch').autocomplete({
	     minLength: 3,
	     delay: 500,
	     appendTo: '#createGroupModal',
	     autoFocus: true,
	     select: function(event, ui) {
	    	 createGroupModalUserAdded(ui.item.value);
	     },
	     source: function(request, response, url){
	    	var form = {tag: request.term};
		    $.ajax({
	            url: baseUrl + "user/startsWith",
	            contentType: "application/json; charset=utf-8",
	            dataType: "json",
	            type: "POST",
	            data : JSON.stringify(form),
	            success: function (data) {
	            	createGroupModalAddUserSearchSource = data;
                   response($.map(data, function(item) {
	                    return { 
	                        label: item.name,
	                        value: item.name
	                    };
                   }));
	            }
		     });
	      }
	 });
	
	$('#searchGroupModalSearch').autocomplete({
	     minLength: 3,
	     delay: 500,
	     appendTo: '#searchGroupModal',
	     autoFocus: true,
	     focus: function(event, ui) {
	    	 event.preventDefault();
	    	 $("#searchGroupModalSearch").val(ui.item.label);
	     },
	     select: function(event, ui) {
	    	 event.preventDefault();
	    	 $("#searchGroupModalSearch").val(ui.item.label);
	    	 searchGroupModalSelectedGroup = ui.item.value;
	     },
	     source: function(request, response, url){
		    var form = {tag: request.term};
		    $.ajax({
	            url: baseUrl + "group/startsWith",
	            contentType: "application/json; charset=utf-8",
	            dataType: "json",
	            type: "POST",
	            data : JSON.stringify(form),
	            success: function (data) {
                  response($.map(data, function(item) {
	                    return { 
	                        label: item.name,
	                        value: item.id
	                    };
                  }));
	            }
		     });
	      }
	 });
	
	$('#createGroupModalAddedUsers').delegate('li', 'click', function() {
		 $(this).siblings().removeClass("active");
		 $(this).addClass("active");
		 createGroupModalUserClicked();
	 });
	 
	 $("#createGroupModalRemoveUser").click(function() {
		 createGroupModalUserRemoved();
	 });
	 
	 $("#searchGroupModalSendRequest").click(function() {
		 searchGroupModalSendRequest();
	 });
	
});

function searchGroupModalSendRequest() {
	if (searchGroupModalSelectedGroup == null) {
		addSearchGroupNoGroupSelectedAlert();
		return;
	}
	var sendRequestUrl = baseUrl + "membershiprequest";
	$.ajax({     
        type: "PUT",  
        url: sendRequestUrl,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(searchGroupModalSelectedGroup),
        success: function (data) {
        	$("#searchGroupModal").modal('hide');
        },
        error: function(e) {
            console.log(e);
            addSendRequestServerErrorAlert();
        }
    }); 
}

function createGroupModalUserRemoved() {
	var username = $("#createGroupModalAddedUsers li.active").text();
	$("#createGroupModalAddedUsers li.active").remove();
	$("#createGroupModalRemoveUser").addClass("hidden");
	
	for (i in createGroupModalSelectedUsers) {
		if (createGroupModalSelectedUsers[i].name == username) {
			createGroupModalSelectedUsers.splice(i, 1);
		}
	}
}

function createGroupModalUserClicked() {
	$("#createGroupModalRemoveUser").removeClass("hidden");
}

function createGroupModalUserAdded(username) {
	var user = createGroupModalFindAddedUser(username);
	if (createGroupModalFindSelectedUser(username) == null) {
		createGroupModalSelectedUsers.push(user);
		$("#createGroupModalAddedUsers").append("<li class=\"list-group-item\">" + username + "</li>");
	}
}

function createGroupModalFindSelectedUser(username) {
	for (i in createGroupModalSelectedUsers) {
		if (username == createGroupModalSelectedUsers[i].name) {
			return createGroupModalSelectedUsers[i];
		}
	}
	return null;
}

function createGroupModalFindAddedUser(username) {
	for (i in createGroupModalAddUserSearchSource) {
		if (username == createGroupModalAddUserSearchSource[i].name) {
			return createGroupModalAddUserSearchSource[i];
		}
	}
}

function processCreateGroupForm() {
	var groupName = $("#createGroupName").val();
	if (groupName.length < 3) {
		addCreateGroupNameTooShortAlert();
		return;
	}
	
	var userIdsToInvite = new Array();
	for (i in createGroupModalSelectedUsers) {
		var userId = createGroupModalSelectedUsers[i].id;
		userIdsToInvite.push(userId);
	}
	
	clearAlertPlace();
	
	var groupForm = {"name": groupName, "userIdsToInvite": userIdsToInvite};
	
	var addGroupUrl = baseUrl + "group";
	$.ajax({     
        type: "PUT",  
        url: addGroupUrl,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(groupForm),
        success: function (groupId) {
        	if (groupId == null) {
        		addCreateGroupServerErrorAlert();
        		return;
        	}
        	addGroupToGroupPanel(groupId, groupName);
        	$("#createGroupModal").modal('hide');
        },
        error: function(e) {
            console.log(e);
            addCreateGroupServerErrorAlert();
        }
    }); 
}

function addGroupToGroupPanel(groupId, groupName) {
	groups.push({"id": groupId, "name": groupName});
	var position = groups.length - 1;
	var contentToAdd = "<li onclick=\"groupClicked(" + position + ")\" id=\"group" + position + "\"><a href=\"#\">" + groupName + "</a></li>";
	$("#groupPanel").append(contentToAdd);
	$("#groupPanel li:eq(" + position + ")").click();
}

function addMenuPanel() {
	var content = "<ul class=\"nav nav-pills nav-stacked\" id=\"groupPanel\"></ul>";
	content += "<div class=\"margintop10\" id=\"belowGroupPanel\"></div>";
	$("#menuPanel").html(content);
}

function clearAlertPlace() {
	$("#alertPlace").html("");
}

function isEmpty(data) {
	for (i in data) {
		return false;
	}
	return true;
}

function removeActiveClassOfSiblings(target) {
	$(target).siblings().removeClass("active");
}

function addActiveClass(target) {
	$(target).addClass("active");
}

function addCreateGroupNameTooShortAlert() {
	var content = "<div class=\"alert alert-warning alert-dismissable\"> " +
		  "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
		  "<strong>Name too short!</strong> Minimal length is 3.</div>";
	$("#createGroupAlertPlace").html(content);
}

function addCreateGroupServerErrorAlert() {
	var content = "<div class=\"alert alert-danger alert-dismissable\"> " +
	  "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
	  "<strong>Error occured!</strong> Please try again later.</div>";
	$("#createGroupAlertPlace").html(content);
}

function addSearchGroupNoGroupSelectedAlert() {
	var content = "<div class=\"alert alert-warning alert-dismissable\"> " +
	  "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
	  "<strong>Could not send request!</strong> No group was selected.</div>";
	$("#searchGroupAlertPlace").html(content);
}

function addSendRequestServerErrorAlert() {
	var content = "<div class=\"alert alert-danger alert-dismissable\"> " +
	  "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
	  "<strong>Error occured during creating membership request!</strong> Please try again later.</div>";
	$("#searchGroupAlertPlace").html(content);
}