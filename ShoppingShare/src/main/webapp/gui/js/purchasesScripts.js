var baseUrl = "https://localhost:8443/ShoppingShare/";

var purchases = [];
var items = [];
var templates = [];
var templateItems = [];

var selectedGroupPosition = null;
var selectedPurchasePosition = null;
var selectedItemsPositions = [ ];
var selectedTemplatePosition = null;

$(window).load(function() {
	$("#mainPanelContent").css("padding-right", "15px");
	loadGroups();
	$('#createPurchaseFromTemplateModal').on('shown.bs.modal', function (e) {
		  loadTemplatesForModal();
	});
});

$(document).ready(function() {
	addMenuPanel();
});

function loadTemplatesForModal() {
	var getTemplatesUrl = baseUrl + "purchasetemplate";
	$.getJSON(getTemplatesUrl, function (data) {
		templates = data;
		processTemplatesData();
	});
}

function processTemplatesData() {
	var empty = isEmpty(templates);
	var content = "";
	if (!empty) {
		for (i in templates) {
			var template = templates[i];
			content += "<li onclick=\"templateClicked(" + i + ")\" id=\"template" + i + "\"><a href=\"#\">" + template.name + "</a></li>"; 
		}
		$("#createPurchaseFromTemplateTemplates").html(content);
		templateClicked(0);
	} else {
		showNoTemplatesAlert();
		return;
	}
}

function templateClicked(position) {
	selectedTemplatePosition = position;
	$("#mainPanelContent").html("");
	clearAlertPlace();
	
	var templateButton = $("#template" + position);
	removeActiveClassOfSiblings(templateButton);
	addActiveClass(templateButton);
	
	loadTemplateItems();
}

function loadTemplateItems() {
	var templateId = templates[selectedTemplatePosition].purchaseId;
	var loadItemsUrl = baseUrl + "item/template/all";
	$.ajax({     
        type: "POST",  
        url: loadItemsUrl,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(templateId),
        success: function (data) {
        	templateItems = data;
        	processTemplateItemsData();
        },
        error: function(e) {
            console.log(e);
        }
    }); 
}

function processTemplateItemsData() {
	var empty = isEmpty(templateItems);
	var content = "";
	if (!empty) {
		content += "<ul class=\"list-group itemsList\" id=\"templateItemsList\">";
		for (i in templateItems) {
			var item = templateItems[i];
			content += "<li class=\"list-group-item\">" + item.name + "</li>";
		}
		content += "</ul>";
	}
	$("#createPurchaseFromTemplateItems").html(content);
}

function createPurchaseFromTemplateSendButtonPressed() {
	var name = $("#purchaseFromTemplateName").val();
	if (name.length < 3) {
		showPurchaseFromTemplateNameTooShortAlert();
		return;
	} else if (selectedTemplatePosition == null) {
		showNoTemplateSelectedAlert();
		return;
	}
	var templateId = templates[selectedTemplatePosition].purchaseId;
	var groupId = groups[selectedGroupPosition].id;
	var form = {"name": name, "entityId": templateId, "entityId2": groupId};
	var addPurchaseUrl = baseUrl + "purchase/fromtemplate";
	$.ajax({     
        type: "PUT",  
        url: addPurchaseUrl,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(form),
        success: function (purchaseId) {
        	if (purchaseId == null) {
        		showAddPurchaseFromTemplateServerErrorAlert();
        		return;
        	} else {
        		addPurchaseToDropdown(purchaseId, name);
        		selectedTemplatePosition = null;
        		$("#createPurchaseFromTemplateModal").modal('hide');
        	}
        },
        error: function(e) {
            console.log(e);
            showAddPurchaseFromTemplateServerErrorAlert();
        }
    }); 
}

function loadGroups() {
	var getGroupsUrl = baseUrl + "group/web/all";
	$.getJSON(getGroupsUrl, function (data) {
		groups = data;
		addGroupsToGroupPanel();
	});
}

function addGroupsToGroupPanel() {
	var empty = isEmpty(groups);
	var content = "";
	if (!empty) {
		for (i in groups) {
			var group = groups[i];
			content += "<li onclick=\"groupClicked(" + i + ")\" id=\"group" + i + "\"><a href=\"#\">" + group.name + "</a></li>"; 
		}
		$("#groupPanel").html(content);
		$("#group0").click();
		addAddPurchaseButton();
	} else {
		showNoGroupsParticipatedAlert();
	}
}

function groupClicked(position) {
	selectedGroupPosition = position;
	$("#mainPanelContent").html("");
	$("#mainPanelLabel").html("");
	clearAlertPlace();
	
	var groupButton = $("#group" + position);
	$(groupButton).siblings().removeClass("active");
	$(groupButton).addClass("active");
	
	var groupId = groups[position].id;
	var getGroupPurchasesUrl = baseUrl + "purchase/all";
	$.ajax({     
        type: "POST",  
        url: getGroupPurchasesUrl,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(groupId),
        success: function (data) {
        	if (data == null) {
        		showPurchasesLoadingServerErrorAlert();
        	} else {
        		processPurchasesData(data);
        	}
        },
        error: function(e) {
        	showPurchasesLoadingServerErrorAlert();
            console.log(e);
        }
    }); 
}

function processPurchasesData(data) {
	purchases = data;
	var empty = isEmpty(data);
	var content = "";
	if (!empty) {
		addPurchasesDropdown();
		for (i in data) {
			var purchase = data[i];
			content += "<li onclick=\"purchaseClicked(" + i + ")\" id=\"purchase" + i +"\"><a href=\"#\">" + purchase.name + "</a></li>";
		}
		$("#purchasesDropdown").html(content);
		$("#purchase0").click();
		addEditPurchaseButton();
		addDeletePurchaseButton();
		addBoughtItemButton();
		addAddItemButton();
		addEditItemButton();
		addDeleteItemButton();
	} else {
		content += "No purchases.";
		$("#purchasesDropdown").html(content);
		$("#editPurchaseDiv").html("");
		$("#deletePurchaseDiv").html("");
		$("#boughtItemDiv").html("");
		$("#addItemDiv").html("");
		$("#editItemDiv").html("");
		$("#deleteItemDiv").html("");
		showNoPurchasesCreatedAlert();
	}
}

function purchaseClicked(position) {
	clearAlertPlace();
	$("#mainPanelLabel").html(purchases[position].name);
	selectedPurchasePosition = position;
	var purchaseId = purchases[position].purchaseId;
	var getItemsUrl = baseUrl + "item/all";
	$.ajax({     
        type: "POST",  
        url: getItemsUrl,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(purchaseId),
        success: function (data) {
        	if (data == null) {
        		showItemsLoadingServerErrorAlert();
        	} else {
        		processItemsData(data);
        	}
        },
        error: function(e) {
        	showItemsLoadingServerErrorAlert();
            console.log(e);
        }
    }); 
}

function processItemsData(data) {
	items = data;
	var empty = isEmpty(items);
	var content = "";
	if (!empty) {
		content += "<ul class=\"list-group itemsList\" id=\"itemsList\">";
		for (i in items) {
			var item = items[i];
			if (item.state == "NEW") {
				content += "<li class=\"list-group-item\" onclick=\"selectItem(" + i + ")\">" + item.name + "</li>";
			} else {
				content += "<li class=\"list-group-item bought\" onclick=\"selectItem(" + i + ")\">" + item.name + "</li>";
			}
		}
		content += "</ul>";
	}
	$("#editItemButton").addClass("disabled");
	$("#deleteItemButton").addClass("disabled");
	$("#mainPanelContent").html(content);
}

function selectItem(position) {
	var item = $("#itemsList li:eq(" + position + ")");
	if ($(item).hasClass("active")) {
		removeSelectionOfItemInPosition(position);
		$(item).removeClass("active");
	} else {
		$(item).addClass("active");
		selectedItemsPositions.push(position);
	}
	if (selectedItemsPositions.length == 1) {
		$("#editItemButton").removeClass("disabled");
	} else {
		$("#editItemButton").addClass("disabled");
	}
	if (selectedItemsPositions.length > 0) {
		$("#deleteItemButton").removeClass("disabled");
		$("#boughtItemButton").removeClass("disabled");
	} else {
		$("#deleteItemButton").addClass("disabled");
		$("#boughtItemButton").addClass("disabled");
	}
}

function removeSelectionOfItemInPosition(position) {
	for (i in selectedItemsPositions) {
		if (position == selectedItemsPositions[i]) {
			selectedItemsPositions.splice(i, 1);
		}
	}
}

function addPurchasesDropdown() {
	var content = "<button type=\"button\" class=\"btn btn-default dropdown-toggle width100percent\" data-toggle=\"dropdown\">";
	content += "Select purchase <span class=\"caret\"></span></button>";
	content += "<ul class=\"dropdown-menu width100percent dropdownWithoutTop\" role=\"menu\" id=\"purchasesDropdown\"></ul>";
	$("#purchasesDropdownDiv").html(content);
	$("#purchasesDropdown").css({'top': ''});
}

function addAddPurchaseButton() {
	var content = "<div class=\"btn-group width100percent\">";
	content += "<button type=\"button\" class=\"btn btn-success width80percent\" id=\"addPurchaseButton\" title=\"Add purchase\">" +
		"<span class=\"glyphicon glyphicon-plus\"></span></button>";
	content += "<button type=\"button\" class=\"btn btn-success width20percent\" id=\"addPurchaseFromTemplateButton\" " +
			"title=\"Add purchase from template\" data-toggle=\"modal\" data-target=\"#createPurchaseFromTemplateModal\">" +
		"<span class=\"glyphicon glyphicon-floppy-open\"></span></button>";
	content += "</div>";
	$("#addPurchaseDiv").html(content);
	$("#addPurchaseButton").click(function() {
		addPurchaseButtonPressed();
	});
}

function addAddItemButton() {
	var content = "<button type=\"button\" class=\"btn btn-success width100percent\" id=\"addItemButton\" title=\"Add item\">" +
	"<span class=\"glyphicon glyphicon-plus\"></span></button>";
	$("#addItemDiv").html(content);
	$("#addItemButton").click(function() {
		addItemButtonPressed();
	});
}

function addEditPurchaseButton() {
	var content = "<button type=\"button\" class=\"btn btn-warning width100percent\" id=\"editPurchaseButton\" title=\"Edit purchase\">" +
	"<span class=\"glyphicon glyphicon-edit\"></span></button>";
	$("#editPurchaseDiv").html(content);
	$("#editPurchaseButton").click(function() {
		editPurchaseButtonPressed();
	});
}

function addEditItemButton() {
	var content = "<button type=\"button\" class=\"btn btn-warning width100percent disabled\" id=\"editItemButton\" title=\"Edit item\">" +
	"<span class=\"glyphicon glyphicon-edit\"></span></button>";
	$("#editItemDiv").html(content);
	$("#editItemButton").click(function() {
		editItemButtonPressed();
	});
}

function addDeletePurchaseButton() {
	var content = "<button type=\"button\" class=\"btn btn-danger width100percent\" id=\"deletePurchaseButton\" title=\"Delete purchase\">" +
	"<span class=\"glyphicon glyphicon-trash\"></span></button>";
	$("#deletePurchaseDiv").html(content);
	$("#deletePurchaseButton").click(function() {
		$('#deletePurchaseModal').modal();
	});
}

function addDeleteItemButton() {
	var content = "<button type=\"button\" class=\"btn btn-danger width100percent disabled\" id=\"deleteItemButton\" title=\"Delete item\">" +
	"<span class=\"glyphicon glyphicon-trash\"></span></button>";
	$("#deleteItemDiv").html(content);
	$("#deleteItemButton").click(function() {
		deleteItemButtonPressed();
	});
}

function addBoughtItemButton() {
	var content = "<button type=\"button\" class=\"btn btn-default width100percent disabled\" id=\"boughtItemButton\" title=\"Mark item as bought\">" +
	"<span class=\"glyphicon glyphicon-ok\"></span></button>";
	$("#boughtItemDiv").html(content);
	$("#boughtItemButton").click(function() {
		boughtItemButtonPressed();
	});
}

function addPurchaseButtonPressed() {
	var content = "<input type=\"text\" class=\"form-control\" id=\"addPurchaseInput\" value=\"\">";
	$("#addPurchaseDiv").html(content);
	$("#addPurchaseInput").focus();
	$('#addPurchaseInput').keypress(function(event){
	    var keycode = (event.keyCode ? event.keyCode : event.which);
	    if(keycode == '13'){
	    	processNewPurchase(); 
	    }
	});
	$('#addPurchaseInput').blur(function() {
		addAddPurchaseButton();
	});
}

function addItemButtonPressed() {
	var content = "<input type=\"text\" class=\"form-control\" id=\"addItemInput\" value=\"\">";
	$("#addItemDiv").html(content);
	$("#addItemInput").focus();
	$('#addItemInput').keypress(function(event){
	    var keycode = (event.keyCode ? event.keyCode : event.which);
	    if(keycode == '13'){
	    	processNewItem(); 
	    }
	});
	$('#addItemInput').blur(function() {
		addAddItemButton();
	});
}

function editPurchaseButtonPressed() {
	var purchase = purchases[selectedPurchasePosition];
	var content = "<input type=\"text\" class=\"form-control\" id=\"editPurchaseInput\" value=\"" + purchase.name + "\">";
	$("#editPurchaseDiv").html(content);
	$("#editPurchaseInput").focus();
	$('#editPurchaseInput').keypress(function(event){
	    var keycode = (event.keyCode ? event.keyCode : event.which);
	    if(keycode == '13'){
	    	processEditedPurchase(selectedPurchasePosition);
	    }
	});
	$('#editPurchaseInput').blur(function() {
		addEditPurchaseButton();
	});
}

function editItemButtonPressed() {
	if (selectedItemsPositions.length != 1) {
		return;
	}
	var selectedItem = items[selectedItemsPositions[0]];
	var content = "<input type=\"text\" class=\"form-control margintop10\" id=\"editItemInput\" value=\"" + selectedItem.name + "\">";
	$("#editItemDiv").html(content);
	$("#editItemInput").focus();
	$('#editItemInput').keypress(function(event){
	    var keycode = (event.keyCode ? event.keyCode : event.which);
	    if(keycode == '13'){
	    	processEditedItem(selectedItem);
	    }
	});
	$('#editItemInput').blur(function() {
		addEditItemButton();
	});
}

function deletePurchaseButtonPressed() {
	purchaseId = purchases[selectedPurchasePosition].purchaseId;
	if (purchaseId == null) {
		$("#deletePurchaseModal").modal('hide');
		addDeletePurchaseServerErrorAlert();
		return;
	}
	var deletePurchaseUrl = baseUrl + "purchase";
	$.ajax({     
        type: "DELETE",  
        url: deletePurchaseUrl,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(purchaseId),
        success: function (confirmMessage) {
        	$("#deletePurchaseModal").modal('hide');
        	if (confirmMessage.ok == true) {
        		groupClicked(selectedGroupPosition);
        	} else {
        		addDeletePurchaseServerErrorAlert();
        	}
        },
        error: function(e) {
            console.log(e);
            $("#deletePurchaseModal").modal('hide');
            addDeletePurchaseServerErrorAlert();
        }
    });
}

function deleteItemButtonPressed() {
	if (selectedItemsPositions.length < 1) {
		return;
	}
	var itemIdsToDelete = [];
	for (i in selectedItemsPositions) {
		itemIdsToDelete.push(items[selectedItemsPositions[i]].itemId);
	}
	var deleteItemsUrl = baseUrl + "item/all";
	var deleteItemsForm = {ids: itemIdsToDelete};
	$.ajax({     
        type: "DELETE",  
        url: deleteItemsUrl,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data : JSON.stringify(deleteItemsForm),
        success: function (success) {
        	if (success.ok == true) {
        		selectedItemsPositions = [];
        		purchaseClicked(selectedPurchasePosition);
        	} else {
        		showDeleteItemErrorAlert();
        	}
        },
        error: function(e) {
            console.log(e);
            showDeleteItemErrorAlert();
        }
    }); 
}

function boughtItemButtonPressed() {
	if (selectedItemsPositions.length < 1) {
		return;
	}
	var boughtItems = [];
	var item;
	for (i in selectedItemsPositions) {
		item = items[selectedItemsPositions[i]];
		if (item.state == "NEW") {
			boughtItems.push(item.itemId);
		}
	}
	var boughtItemsUrl = baseUrl + "item/all/bought";
	var boughtItemsForm = {ids: boughtItems};
	$.ajax({     
        type: "POST",  
        url: boughtItemsUrl,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data : JSON.stringify(boughtItemsForm),
        success: function (success) {
        	if (success.ok == true) {
        		selectedItemsPositions = [];
        		purchaseClicked(selectedPurchasePosition);
        	} else {
        		showBoughtItemErrorAlert();
        	}
        },
        error: function(e) {
            console.log(e);
            showBoughtItemErrorAlert();
        }
    }); 
}

function processEditedPurchase(selectedPurchasePosition) {
	var purchase = purchases[selectedPurchasePosition];
	var editedPurchaseName = $("#editPurchaseInput").val();
	if (editedPurchaseName.length < 3) {
		$("#editPurchaseInput").popover({content:'Minimal length is 3.'}).popover('show');
		return;
	} else if (editedPurchaseName == purchase.name) {
		addEditPurchaseButton();
		return;
	} else {
		$("#editPurchaseInput").popover('destroy');
	}
	var editPurchaseUrl = baseUrl + "purchase/web";
	var form = {name: editedPurchaseName, entityId: purchase.purchaseId};
	$.ajax({     
        type: "POST",  
        url: editPurchaseUrl,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(form),
        success: function (success) {
        	if (success.ok == true) {
        		clearAlertPlace();
        		purchase.name = editedPurchaseName;
        		$("#purchase" + selectedPurchasePosition + " a").html(editedPurchaseName);
        		$("#mainPanelLabel").html(editedPurchaseName);
        		addEditPurchaseButton();
        	} else {
        		showEditPurchaseErrorAlert();
        	}
        },
        error: function(e) {
            console.log(e);
            showEditPurchaseErrorAlert();
        }
    });
}

function processEditedItem(selectedItem) {
	var editedItemName = $("#editItemInput").val();
	if (editedItemName.length < 1) {
		$("#editItemInput").popover({content:'Minimal length is 1.'}).popover('show');
		return;
	} else if (selectedItem.name == editedItemName) {
		addEditItemButton();
		$("#editItemButton").removeClass("disabled");
		return;
	} else {
		$("#editItemInput").popover('destroy');
	}
	var editItemUrl = baseUrl + "item/web";
	var form = {name: editedItemName, entityId: selectedItem.itemId};
	$.ajax({     
        type: "POST",  
        url: editItemUrl,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(form),
        success: function (success) {
        	if (success.ok == true) {
        		selectedItem.name = editedItemName;
        		processItemsData(items);
        		selectedItemsPositions = [];
        	} else {
        		showEditItemErrorAlert();
        	}
        },
        error: function(e) {
            console.log(e);
            showEditItemErrorAlert();
        }
    });
}

function processNewPurchase() {
	var purchaseName = $("#addPurchaseInput").val();
	if (purchaseName.length < 3) {
		$("#addPurchaseInput").popover({content:'Minimal length is 3.'}).popover('show');
		return;
	} else {
		$("#addPurchaseInput").popover('destroy');
	}
	var groupId = groups[selectedGroupPosition].id;
	var purchaseForm = {"name": purchaseName, "entityId": groupId};
	var addPurchaseUrl = baseUrl + "purchase";
	$.ajax({     
        type: "PUT",  
        url: addPurchaseUrl,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(purchaseForm),
        success: function (purchaseId) {
        	if (purchaseId == null) {
        		showAddPurchaseServerErrorAlert();
        		return;
        	} else {
        		addPurchaseToDropdown(purchaseId, purchaseName);
        		$("#addPurchaseInput").val("");
        	}
        },
        error: function(e) {
            console.log(e);
            showAddPurchaseServerErrorAlert();
        }
    }); 
}

function processNewItem() {
	var itemName = $("#addItemInput").val();
	if (itemName.length < 1) {
		$("#addItemInput").popover({content:'Minimal length is 1.'}).popover('show');
		return;
	} else {
		$("#addItemInput").popover('destroy');
	}
	var purchaseId = purchases[selectedPurchasePosition].purchaseId;
	var itemForm = {"name": itemName, "entityId": purchaseId};
	var addItemUrl = baseUrl + "item";
	$.ajax({     
        type: "PUT",  
        url: addItemUrl,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(itemForm),
        success: function (itemId) {
        	if (itemId == null) {
        		showAddItemServerErrorAlert();
        		return;
        	}
        	items.unshift({"itemId": itemId, "name": itemName, "state": "NEW"});
        	processItemsData(items);
        	$("#addItemInput").val("");
        },
        error: function(e) {
            console.log(e);
            showAddItemServerErrorAlert();
        }
    }); 
}

function addPurchaseToDropdown(purchaseId, purchaseName) {
	purchases.push({"purchaseId": purchaseId, "name": purchaseName});
	var position = purchases.length - 1;
	var contentToAdd = "<li onclick=\"purchaseClicked(" + position + ")\" id=\"purchase" + position +"\"><a href=\"#\">" + purchaseName + "</a></li>";
	clearAlertPlace();
	if (purchases.length == 1) {
		$("#purchasesDropdown").html(contentToAdd);
		addEditPurchaseButton();
		addDeletePurchaseButton();
		addAddItemButton();
		addEditItemButton();
		addDeleteItemButton();
	} else {
		$("#purchasesDropdown").append(contentToAdd);
	}
	$("#purchase" + position).click();
}

function addDeletePurchaseServerErrorAlert() {
	var content = "<div class=\"alert alert-danger alert-dismissable\"> " +
	  "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
	  "<strong>Error occured during deleting purchase!</strong> Please try again later.</div>";
	$("#alertPlace").html(content);
}

function showBoughtItemErrorAlert() {
	var content = "<div class=\"alert alert-danger alert-dismissable\"> " +
	  "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
	  "<strong>Error occured during marking items bought!</strong> Please try again later.</div>";
	$("#alertPlace").html(content);
}

function showAddPurchaseServerErrorAlert() {
	var content = "<div class=\"alert alert-danger alert-dismissable\"> " +
	  "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
	  "<strong>Error occured during creating purchase!</strong> Please try again later.</div>";
	$("#alertPlace").html(content);
}

function showAddItemServerErrorAlert() {
	var content = "<div class=\"alert alert-danger alert-dismissable\"> " +
	  "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
	  "<strong>Error occured during adding item!</strong> Please try again later.</div>";
	$("#alertPlace").html(content);
}

function showDeleteItemErrorAlert() {
	var content = "<div class=\"alert alert-danger alert-dismissable\"> " +
	  "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
	  "<strong>Deleting item error.</strong> Try again.</div>";
	$("#alertPlace").html(content);
}

function showEditPurchaseErrorAlert() {
	var content = "<div class=\"alert alert-danger alert-dismissable\"> " +
	  "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
	  "<strong>Error occured during editing purchase!</strong> Please try again later.</div>";
	$("#alertPlace").html(content);
}

function showEditItemErrorAlert() {
	var content = "<div class=\"alert alert-danger alert-dismissable\"> " +
	  "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
	  "<strong>Error occured during editing item!</strong> Please try again later.</div>";
	$("#alertPlace").html(content);
}

function showItemsLoadingServerErrorAlert() {
	var content = "<div class=\"alert alert-danger alert-dismissable\"> " +
	  "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
	  "<strong>Error occured during loading items!</strong> Please try again later.</div>";
	$("#alertPlace").html(content);
}

function showPurchasesLoadingServerErrorAlert() {
	var content = "<div class=\"alert alert-danger alert-dismissable\"> " +
	  "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
	  "<strong>Error occured during loading purchases!</strong> Please try again later.</div>";
	$("#alertPlace").html(content);
}

function showNoPurchasesCreatedAlert() {
	var content = "<div class=\"alert alert-info alert-dismissable\"> " +
	  "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
	  "<strong>There are no purchases in this group.</strong> Try to create one.</div>";
	$("#alertPlace").html(content);
}

function showNoGroupsParticipatedAlert() {
	var content = "<div class=\"alert alert-info alert-dismissable\"> " +
	  "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
	  "<strong>No groups participated.</strong> Search or create group to participate.</div>";
	$("#alertPlace").html(content);
}

function showNoTemplatesAlert() {
	var content = "<div class=\"alert alert-info alert-dismissable\"> " +
	  "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
	  "<strong>No templates created.</strong> Create one before using it.</div>";
	$("#createPurchaseFromTemplateAlertPlace").html(content);
}

function showNoTemplateSelectedAlert() {
	var content = "<div class=\"alert alert-danger alert-dismissable\"> " +
	  "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
	  "<strong>No template selected!</strong></div>";
	$("#createPurchaseFromTemplateAlertPlace").html(content);
}

function showPurchaseFromTemplateNameTooShortAlert() {
	var content = "<div class=\"alert alert-danger alert-dismissable\"> " +
	  "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
	  "<strong>Purchase name too short.</strong> Minimal length is 3.</div>";
	$("#createPurchaseFromTemplateAlertPlace").html(content);
}

function showAddPurchaseFromTemplateServerErrorAlert() {
	var content = "<div class=\"alert alert-danger alert-dismissable\"> " +
	  "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
	  "<strong>Error creating purchase!</strong> Please try again later.</div>";
	$("#createPurchaseFromTemplateAlertPlace").html(content);
}