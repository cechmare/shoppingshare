var offers = null;
var selectedOfferPosition = null;

$(document).ready(function() {
	loadOffers();
});

function loadOffers() {
	var getOffersUrl = baseUrl + "membershipoffer";
	$.getJSON(getOffersUrl, function (data) {
		offers = data;
		processOffersData();
	});
}

function processOffersData() {
	var empty = isEmpty(offers);
	var content = "";
	if (!empty) {
		content += "<ul class=\"list-group itemsList\" id=\"offersList\">";
		for (i in offers) {
			var offer = offers[i];
			content += "<li class=\"list-group-item\" onclick=\"selectOffer(" + i + ")\">" + offer.groupName + "</li>";
		}
		content += "</ul>";
		addOffersActionButtons();
	} else {
		showNoOffersAlert();
		removeOffersActionButtons();
	}
	$("#mainPanelContent").html(content);
	$('#offersList').delegate('li', 'click', function() {
		 removeActiveClassOfSiblings($(this));
		 addActiveClass($(this));
	 });
}

function selectOffer(position) {
	selectedOfferPosition = position;
	removeDisabledClassOfOffersActionButtons();
}

function addOffersActionButtons() {
	var content = "<div class=\"row\">";
	content += "<button type=\"button\" class=\"btn btn-success btn-lg width100 disabled\" id=\"acceptOffer\">Accept</button>";
	content += "</div>";
    content += "<div class=\"row\">";
    content += "<button type=\"button\" class=\"btn btn-danger btn-lg width100 margintop10 disabled\" id=\"rejectOffer\">Reject</button>";
    content += "</div>";
    $("#offersActionButtons").html(content);
    $("#acceptOffer").click(function() {
		acceptOffer();
	});
    $("#rejectOffer").click(function() {
		rejectOffer();
	});
}

function removeOffersActionButtons() {
	$("#offersActionButtons").html("");
}

function acceptOffer() {
	var acceptOfferUrl = baseUrl + "membershiprequest/accept";
	var offerId = offers[selectedOfferPosition].requestId;
	$.ajax({     
        type: "POST",  
        url: acceptOfferUrl,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(offerId),
        success: function (data) {
        	if (data.ok == true) {
        		showOfferAcceptedAlert();
        		removeOfferWithIdAndRefresh(offerId);
        	} else {
        		showOfferAcceptionErrorAlert();
        	}
        },
        error: function(e) {
            console.log(e);
            showOfferAcceptionErrorAlert();
        }
    }); 
}

function rejectOffer() {
	var rejectOfferUrl = baseUrl + "membershiprequest/reject";
	var offerId = offers[selectedOfferPosition].requestId;
	$.ajax({     
        type: "POST",  
        url: rejectOfferUrl,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(offerId),
        success: function (data) {
        	if (data.ok == true) {
        		showOfferRejectedAlert();
        		removeOfferWithIdAndRefresh(offerId);
        	} else {
        		showOfferRejectionErrorAlert();
        	}
        },
        error: function(e) {
            console.log(e);
            showOfferRejectionErrorAlert();
        }
    }); 
}

function removeOfferWithIdAndRefresh(offerId) {
	for (i in offers) {
		if (offerId == offers[i].requestId) {
			offers.splice(i, 1);
		}
	}
	processOffersData();
}

function removeDisabledClassOfOffersActionButtons() {
	$("#offersActionButtons button").removeClass("disabled");
}

function showOfferRejectedAlert() {
	var content = "<div class=\"alert alert-success alert-dismissable\"> " +
	  "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
	  "<strong>Success!</strong> Membership offer rejected.</div>";
	$("#alertPlace").html(content);
}

function showOfferAcceptedAlert() {
	var content = "<div class=\"alert alert-success alert-dismissable\"> " +
	  "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
	  "<strong>Success!</strong> Membership request accepted.</div>";
	$("#alertPlace").html(content);
}

function showOfferRejectionErrorAlert() {
	var content = "<div class=\"alert alert-danger alert-dismissable\"> " +
	  "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
	  "<strong>Error rejecting offer!</strong> Try again.</div>";
	$("#alertPlace").html(content);
}

function showOfferAcceptionErrorAlert() {
	var content = "<div class=\"alert alert-danger alert-dismissable\"> " +
	  "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
	  "<strong>Error accepting offer!</strong> Try again.</div>";
	$("#alertPlace").html(content);
}

function showNoOffersAlert() {
	var content = "<div class=\"alert alert-info alert-dismissable\"> " +
	  "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
	  "<strong>Nobody invited you.</strong> Try to create or search group.</div>";
	$("#alertPlace").html(content);
}