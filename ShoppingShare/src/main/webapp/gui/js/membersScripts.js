var selectedGroupPosition = null;
var selectedRequest = null;

var requests = null;
var offers = null;

var inviteUserSource = [ ];
var userToInvite = null;

$(window).load(function() {
	loadGroups();
});

$(document).ready(function() {
	
	addMenuPanel();
	
	$("#membersTab").click(function() {
		 membersTabClicked();
	});
	
	$("#requestsTab").click(function() {
		requestsTabClicked();
	});
	
	$("#offersTab").click(function() {
		offersTabClicked();
	});
	
	$('#searchUserToInviteInput').keypress(function(event){
	    var keycode = (event.keyCode ? event.keyCode : event.which);
	    if(keycode == '13'){
	    	inviteUser();
	    }
	});
	
	$('#searchUserToInviteInput').autocomplete({
	     minLength: 3,
	     delay: 500,
	     appendTo: '#mainContent',
	     autoFocus: true,
	     select: function(event, ui) {
	    	 selectUserToInvite(ui.item.value);
	     },
	     source: function(request, response, url){
	    	var form = {tag: request.term};
		    $.ajax({
	            url: baseUrl + "user/startsWith",
	            contentType: "application/json; charset=utf-8",
	            dataType: "json",
	            type: "POST",
	            data : JSON.stringify(form),
	            success: function (data) {
	            	inviteUserSource = data;
	            	response($.map(data, function(item) {
	                    return { 
	                        label: item.name,
	                        value: item.name
	                    };
	            	}));
	            }
		     });
	      }
	 });
});

function loadGroups() {
	var getGroupsUrl = baseUrl + "group/web/all";
	$.getJSON(getGroupsUrl, function (data) {
		groups = data;
		addGroupsToGroupPanel();
	});
}

function addGroupsToGroupPanel() {
	var empty = isEmpty(groups);
	var content = "";
	if (!empty) {
		for (i in groups) {
			var group = groups[i];
			content += "<li onclick=\"groupClicked(" + i + ")\" id=\"group" + i + "\"><a href=\"#\">" + group.name + "</a></li>"; 
		}
		$("#groupPanel").html(content);
		$("#membersRequestsOffersTabs").show();
		$("#inviteUserInputGroup").show();
		$("#group0").click();
	} else {
		addNoGroupsParticipatedAlert();
	}
}

function groupClicked(position) {
	selectedGroupPosition = position;
	$("#mainPanelContent").html("");
	$("#mainPanelLabel").html("");
	clearAlertPlace();
	
	var groupButton = $("#group" + position);
	removeActiveClassOfSiblings(groupButton);
	addActiveClass(groupButton);
	
	membersTabClicked();
}

function membersTabClicked() {
	$("#membersActionButtons").html("");
	var membersTab = $("#membersTab");
	removeActiveClassOfSiblings(membersTab);
	addActiveClass(membersTab);
	loadMembers();
	clearAlertPlace();
}

function requestsTabClicked() {
	$("#membersActionButtons").html("");
	var requestsTab = $("#requestsTab");
	removeActiveClassOfSiblings(requestsTab);
	addActiveClass(requestsTab);
	loadRequests();
	clearAlertPlace();
}

function offersTabClicked() {
	$("#membersActionButtons").html("");
	var offersTab = $("#offersTab");
	removeActiveClassOfSiblings(offersTab);
	addActiveClass(offersTab);
	loadOffers();
	clearAlertPlace();
}

function loadMembers() {
	var groupId = groups[selectedGroupPosition].id;
	var loadMembersUrl = baseUrl + "members/web";
	$.ajax({     
        type: "POST",  
        url: loadMembersUrl,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(groupId),
        success: function (data) {
        	processMembersData(data);
        },
        error: function(e) {
            console.log(e);
        }
    }); 
}

function loadRequests() {
	var groupId = groups[selectedGroupPosition].id;
	var loadRequestsUrl = baseUrl + "membershiprequest/all";
	$.ajax({     
        type: "POST",  
        url: loadRequestsUrl,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(groupId),
        success: function (data) {
        	processRequestsData(data);
        },
        error: function(e) {
            console.log(e);
        }
    });
}

function loadOffers() {
	var groupId = groups[selectedGroupPosition].id;
	var loadOffersUrl = baseUrl + "membershipoffer/web/all";
	$.ajax({     
        type: "POST",  
        url: loadOffersUrl,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(groupId),
        success: function (data) {
        	processOffersData(data);
        },
        error: function(e) {
            console.log(e);
        }
    });
}

function processMembersData(data) {
	var empty = isEmpty(data);
	var content = "";
	if (!empty) {
		content += "<ul class=\"list-group itemsList\" id=\"membersList\">";
		for (i in data) {
			var member = data[i];
			content += "<li class=\"list-group-item\">" + member.firstname + " " + member.lastname + " (" + member.login + ")</li>";
		}
		content += "</ul>";
	} else {
		
	}
	$("#mainPanelContent").html(content);
}

function processRequestsData(data) {
	requests = data;
	var empty = isEmpty(data);
	var content = "";
	if (!empty) {
		content += "<ul class=\"list-group\" id=\"requestsList\">";
		for (i in data) {
			var request = data[i];
			content += "<li class=\"list-group-item\">" + request.firstname + " " + request.lastname + " (" + request.login + ")</li>";
		}
		content += "</ul>";
		addRequestsActionButtons();
	}
	$("#mainPanelContent").html(content);
	$('#requestsList').delegate('li', 'click', function() {
		 removeActiveClassOfSiblings($(this));
		 addActiveClass($(this));
		 requestClicked($(this).html());
	 });
}

function processOffersData(data) {
	offers = data;
	var empty = isEmpty(data);
	var content = "";
	if (!empty) {
		content += "<ul class=\"list-group\" id=\"offersList\">";
		for (i in data) {
			var offer = data[i];
			content += "<li class=\"list-group-item\">" + offer.firstname + " " + offer.lastname + " (" + offer.login + ")</li>";
		}
		content += "</ul>";
	} else {
		
	}
	$("#mainPanelContent").html(content);
}

function addRequestsActionButtons() {
	var content = "<div class=\"row\">";
	content += "<button type=\"button\" class=\"btn btn-success btn-lg width100 disabled\" id=\"acceptRequest\">Accept</button>";
	content += "</div>";
    content += "<div class=\"row\">";
    content += "<button type=\"button\" class=\"btn btn-danger btn-lg width100 margintop10 disabled\" id=\"rejectRequest\">Reject</button>";
    content += "</div>";
    $("#membersActionButtons").html(content);
    $("#acceptRequest").click(function() {
		acceptRequest();
	});
    $("#rejectRequest").click(function() {
		rejectRequest();
	});
}

function selectUserToInvite(username) {
	for (i in inviteUserSource) {
		if (username == inviteUserSource[i].name) {
			userToInvite = inviteUserSource[i];
			return;
		}
	}
}

function requestClicked(username) {
	removeDisabledClassOfMembersActionButtons();
	selectedRequest = getRequestWithUsername(username);
}

function getRequestWithUsername(username) {
	for (i in requests) {
		if (username.indexOf(requests[i].login) !=-1) {
			return requests[i];
		}
	}
}

function inviteUser() {
	if (userToInvite == null) {
		return;
	}
	var inviteUserUrl = baseUrl + "membershipoffer";
	var invitationForm = {"groupId": groups[selectedGroupPosition].id, "userId": userToInvite.id};
	$.ajax({     
        type: "PUT",  
        url: inviteUserUrl,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(invitationForm),
        success: function (data) {
        	offersTabClicked();
        	showOfferSentAlert();
        },
        error: function(e) {
            console.log(e);
            showOfferSendingErrorAlert();
        }
    }); 
	$("#searchUserToInviteInput").val("");
	userToInvite = null;
}

function acceptRequest() {
	var acceptRequestUrl = baseUrl + "membershiprequest/accept";
	$.ajax({     
        type: "POST",  
        url: acceptRequestUrl,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(selectedRequest.userId),
        success: function (data) {
        	requestsTabClicked();
        	if (data.ok == true) {
        		showRequestAcceptedAlert();
        	} else {
        		showRequestAcceptionErrorAlert();
        	}
        },
        error: function(e) {
            console.log(e);
            showRequestAcceptionErrorAlert();
        }
    }); 
}

function rejectRequest() {
	var rejectRequestUrl = baseUrl + "membershiprequest/reject";
	$.ajax({     
        type: "POST",  
        url: rejectRequestUrl,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(selectedRequest.userId),
        success: function (data) {
        	requestsTabClicked();
        	if (data.ok == true) {
        		showRequestRejectedAlert();
        	} else {
        		showRequestRejectionErrorAlert();
        	}
        },
        error: function(e) {
            console.log(e);
            showRequestRejectionErrorAlert();
        }
    }); 
}

function removeDisabledClassOfMembersActionButtons() {
	$("#membersActionButtons button").removeClass("disabled");
}

function showRequestRejectedAlert() {
	var content = "<div class=\"alert alert-success alert-dismissable\"> " +
	  "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
	  "<strong>Success!</strong> Membership request rejected.</div>";
	$("#alertPlace").html(content);
}

function showRequestAcceptedAlert() {
	var content = "<div class=\"alert alert-success alert-dismissable\"> " +
	  "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
	  "<strong>Success!</strong> Membership request accepted.</div>";
	$("#alertPlace").html(content);
}

function showRequestRejectionErrorAlert() {
	var content = "<div class=\"alert alert-danger alert-dismissable\"> " +
	  "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
	  "<strong>Error rejecting request!</strong> Try again.</div>";
	$("#alertPlace").html(content);
}

function showRequestAcceptionErrorAlert() {
	var content = "<div class=\"alert alert-danger alert-dismissable\"> " +
		  "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
		  "<strong>Error accepting request!</strong> Try again.</div>";
	$("#alertPlace").html(content);
}

function showOfferSentAlert() {
	var content = "<div class=\"alert alert-success alert-dismissable\"> " +
		  "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
		  "<strong>Success!</strong> Membership offer successfully sent.</div>";
	$("#alertPlace").html(content);
}

function showOfferSendingErrorAlert() {
	var content = "<div class=\"alert alert-danger alert-dismissable\"> " +
		  "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
		  "<strong>Error sending offer!</strong> Try again.</div>";
	$("#alertPlace").html(content);
}
