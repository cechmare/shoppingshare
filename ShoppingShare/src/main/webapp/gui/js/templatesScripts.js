var templates = null;

var items = null;

var selectedTemplatePosition = null;

var selectedItemsPositions = [ ];

$(window).load(function() {
	addButtonsToBelowGroupPanel();
	loadTemplates();
});

$(document).ready(function() {
	addLeftPanel();
});

function addLeftPanel() {
	var content = "<ul class=\"nav nav-pills nav-stacked\" id=\"leftPanel\"></ul>";
	content += "<div class=\"margintop10\" id=\"belowGroupPanel\"></div>";
	$("#menuPanel").html(content);
}

function loadTemplates() {
	var getTemplatesUrl = baseUrl + "purchasetemplate";
	$.getJSON(getTemplatesUrl, function (data) {
		templates = data;
		addTemplatesToGroupPanel(0);
	});
}

function addTemplatesToGroupPanel(positionToClick) {
	var empty = isEmpty(templates);
	var content = "";
	if (!empty) {
		for (i in templates) {
			var template = templates[i];
			content += "<li onclick=\"templateClicked(" + i + ")\" id=\"template" + i + "\"><a href=\"#\">" + template.name + "</a></li>"; 
		}
		$("#leftPanel").html(content);
		templateClicked(positionToClick);
		addAddItemButton();
		addEditItemButton();
		addDeleteItemButton();
		addEditTemplateButton();
		addDeleteTemplateButton();
	} else {
		$("#leftPanel").html(content);
		$("#mainPanelContent").html("");
		removeNoTemplateButtons();
		showNoTemplatesAlert();
		return;
	}
}

function templateClicked(position) {
	selectedTemplatePosition = position;
	$("#mainPanelContent").html("");
	clearAlertPlace();
	
	var templateButton = $("#template" + position);
	removeActiveClassOfSiblings(templateButton);
	addActiveClass(templateButton);
	
	loadItems();
}

function loadItems() {
	var templateId = templates[selectedTemplatePosition].purchaseId;
	var loadItemsUrl = baseUrl + "item/template/all";
	$.ajax({     
        type: "POST",  
        url: loadItemsUrl,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(templateId),
        success: function (data) {
        	items = data;
        	processItemsData();
        },
        error: function(e) {
            console.log(e);
        }
    }); 
}

function processItemsData() {
	var empty = isEmpty(items);
	var content = "";
	if (!empty) {
		content += "<ul class=\"list-group itemsList\" id=\"itemsList\">";
		for (i in items) {
			var item = items[i];
			content += "<li class=\"list-group-item\" onclick=\"selectItem(" + i + ")\">" + item.name + "</li>";
		}
		content += "</ul>";
	} else {
		
	}
	$("#editItemButton").addClass("disabled");
	$("#deleteItemButton").addClass("disabled");
	$("#mainPanelContent").html(content);
}

function addDeleteItemButton() {
	var content = "<button type=\"button\" class=\"btn btn-danger width100percent margintop10 disabled\" id=\"deleteItemButton\" title=\"Delete item\">" +
	"<span class=\"glyphicon glyphicon-trash\"></span> Delete item</button>";
	$("#deleteItemDiv").html(content);
	$("#deleteItemButton").click(function() {
		deleteItemButtonPressed();
	});
}

function addEditItemButton() {
	var content = "<button type=\"button\" class=\"btn btn-warning width100percent margintop10 disabled\" id=\"editItemButton\" title=\"Edit item\">" +
	"<span class=\"glyphicon glyphicon-edit\"></span> Edit item</button>";
	$("#editItemDiv").html(content);
	$("#editItemButton").click(function() {
		editItemButtonPressed();
	});
}

function addAddItemButton() {
	var content = "<button type=\"button\" class=\"btn btn-success width100percent\" id=\"addItemButton\" title=\"Add item\">" +
		"<span class=\"glyphicon glyphicon-plus\"></span> Add item</button>";
	$("#addItemDiv").html(content);
	$("#addItemButton").click(function() {
		addItemButtonPressed();
	});
}

function addButtonsToBelowGroupPanel() {
	var content = "<div class=\"row\" id=\"addTemplateDiv\"></div>";
	content += "<div class=\"row\" id=\"editTemplateDiv\"></div>";
	content += "<div class=\"row\" id=\"deleteTemplateDiv\"></div>";
	$("#belowGroupPanel").html(content);
	addAddTemplateButton();
}

function addAddTemplateButton() {
	var content = "<button type=\"button\" class=\"btn btn-success width100percent\" id=\"addTemplateButton\" title=\"Add template\">" +
	"<span class=\"glyphicon glyphicon-plus\"></span></button>";
	$("#addTemplateDiv").html(content);
	$("#addTemplateButton").click(function() {
		addTemplateButtonPressed();
	});
}

function addEditTemplateButton() {
	var content = "<button type=\"button\" class=\"btn btn-warning width100percent margintop10\" id=\"editTemplateButton\" title=\"Edit template\">" +
	"<span class=\"glyphicon glyphicon-edit\"></span></button>";
	$("#editTemplateDiv").html(content);
	$("#editTemplateButton").click(function() {
		editTemplateButtonPressed();
	});
}

function addDeleteTemplateButton() {
	var content = "<button type=\"button\" class=\"btn btn-danger width100percent margintop10\" id=\"deleteTemplateButton\" title=\"Delete template\">" +
	"<span class=\"glyphicon glyphicon-trash\"></span></button>";
	$("#deleteTemplateDiv").html(content);
	$("#deleteTemplateButton").click(function() {
		deleteTemplateButtonPressed();
	});
}

function deleteItemButtonPressed() {
	if (selectedItemsPositions.length < 1) {
		return;
	}
	var itemIdsToDelete = [];
	for (i in selectedItemsPositions) {
		itemIdsToDelete.push(items[selectedItemsPositions[i]].itemId);
	}
	var deleteItemsUrl = baseUrl + "item/all";
	var deleteItemsForm = {ids: itemIdsToDelete};
	$.ajax({     
        type: "DELETE",  
        url: deleteItemsUrl,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data : JSON.stringify(deleteItemsForm),
        success: function (success) {
        	if (success.ok == true) {
        		selectedItemsPositions = [];
        		loadItems();
        	} else {
        		showDeleteItemErrorAlert();
        	}
        },
        error: function(e) {
            console.log(e);
            showDeleteItemErrorAlert();
        }
    }); 
}

function editItemButtonPressed() {
	if (selectedItemsPositions.length != 1) {
		return;
	}
	var selectedItem = items[selectedItemsPositions[0]];
	var content = "<input type=\"text\" class=\"form-control margintop10\" id=\"editItemInput\" value=\"" + selectedItem.name + "\">";
	$("#editItemDiv").html(content);
	$("#editItemInput").focus();
	$('#editItemInput').keypress(function(event){
	    var keycode = (event.keyCode ? event.keyCode : event.which);
	    if(keycode == '13'){
	    	processEditedItem(selectedItem);
	    }
	});
	$('#editItemInput').blur(function() {
		addEditItemButton();
	});
}

function addItemButtonPressed() {
	var content = "<input type=\"text\" class=\"form-control\" id=\"addItemInput\" value=\"\">";
	$("#addItemDiv").html(content);
	$("#addItemInput").focus();
	$('#addItemInput').keypress(function(event){
	    var keycode = (event.keyCode ? event.keyCode : event.which);
	    if(keycode == '13'){
	    	processNewItem();
	    }
	});
	$('#addItemInput').blur(function() {
		addAddItemButton();
	});
}

function addTemplateButtonPressed() {
	var content = "<input type=\"text\" class=\"form-control\" id=\"addTemplateInput\" value=\"\">";
	$("#addTemplateDiv").html(content);
	$("#addTemplateInput").focus();
	$('#addTemplateInput').keypress(function(event){
	    var keycode = (event.keyCode ? event.keyCode : event.which);
	    if(keycode == '13'){
	    	processNewTemplate(); 
	    }
	});
	$('#addTemplateInput').blur(function() {
		addAddTemplateButton();
	});
}

function editTemplateButtonPressed() {
	var content = "<input type=\"text\" class=\"form-control margintop10\" id=\"editTemplateInput\" value=\"" + templates[selectedTemplatePosition].name + "\">";
	$("#editTemplateDiv").html(content);
	$("#editTemplateInput").focus();
	$('#editTemplateInput').keypress(function(event){
	    var keycode = (event.keyCode ? event.keyCode : event.which);
	    if(keycode == '13'){
	    	processEditedTemplate();
	    }
	});
	$('#editTemplateInput').blur(function() {
		addEditTemplateButton();
	});
}

function deleteTemplateButtonPressed() {
	var deleteTemplateUrl = baseUrl + "purchasetemplate";
	$.ajax({     
        type: "DELETE",  
        url: deleteTemplateUrl,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data : JSON.stringify(templates[selectedTemplatePosition].purchaseId),
        success: function (success) {
        	if (success.ok == true) {
        		deleteTemplate(selectedTemplatePosition);
        	} else {
        		showDeleteTemplateErrorAlert();
        	}
        },
        error: function(e) {
            console.log(e);
            showDeleteTemplateErrorAlert();
        }
    }); 
}

function processNewItem() {
	var itemName = $("#addItemInput").val();
	if (itemName.length < 1) {
		$("#addItemInput").popover({content:'Minimal length is 1.'}).popover('show');
		return;
	} else {
		$("#addItemInput").popover('destroy');
	}
	var sendItemUrl = baseUrl + "item/template";
	var form = {name: itemName, entityId:templates[selectedTemplatePosition].purchaseId};
	$.ajax({     
        type: "PUT",  
        url: sendItemUrl,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data : JSON.stringify(form),
        success: function (itemId) {
        	if (itemId == null) {
        		showNewItemErrorAlert();
        	} else {
        		var item = {itemId:itemId, name:itemName};
        		items.push(item);
        		processItemsData();
        		$("#addItemInput").val("");
        	}
        },
        error: function(e) {
            console.log(e);
            showNewItemErrorAlert();
        }
    }); 
}

function processNewTemplate() {
	var templateName = $("#addTemplateInput").val();
	if (templateName.length < 3) {
		$("#addTemplateInput").popover({content:'Minimal length is 3.'}).popover('show');
		return;
	} else {
		$("#addTemplateInput").popover('destroy');
	}
	var sendTemplateUrl = baseUrl + "purchasetemplate/web/form";
	$.ajax({     
        type: "PUT",  
        url: sendTemplateUrl,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: templateName,
        success: function (templateId) {
        	if (templateId == null) {
        		showNewTemplateErrorAlert();
        	} else {
        		$("#addTemplateInput").val("");
        		addTemplate(templateName, templateId);
        	}
        },
        error: function(e) {
            console.log(e);
            showNewTemplateErrorAlert();
        }
    }); 
}

function processEditedItem(selectedItem) {
	var editedItemName = $("#editItemInput").val();
	if (editedItemName.length < 1) {
		$("#editItemInput").popover({content:'Minimal length is 1.'}).popover('show');
		return;
	} else if (selectedItem.name == editedItemName) {
		addEditItemButton();
		$("#editItemButton").removeClass("disabled");
		return;
	} else {
		$("#editItemInput").popover('destroy');
	}
	var editItemUrl = baseUrl + "item/web";
	var form = {name: editedItemName, entityId: selectedItem.itemId};
	$.ajax({     
        type: "POST",  
        url: editItemUrl,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(form),
        success: function (success) {
        	if (success.ok == true) {
        		selectedItem.name = editedItemName;
        		processItemsData();
        		selectedItemsPositions = [];
        	} else {
        		showEditItemErrorAlert();
        	}
        },
        error: function(e) {
            console.log(e);
            showEditItemErrorAlert();
        }
    });
}

function processEditedTemplate() {
	var editedTemplateName = $("#editTemplateInput").val();
	if (editedTemplateName.length < 3) {
		$("#editTemplateInput").popover({content:'Minimal length is 3.'}).popover('show');
		return;
	} else if (editedTemplateName == templates[selectedTemplatePosition].name) {
		addEditTemplateButton();
		return;
	} else {
		$("#editTemplateInput").popover('destroy');
	}
	var editTemplateUrl = baseUrl + "purchasetemplate/web";
	var form = {name: editedTemplateName, entityId: templates[selectedTemplatePosition].purchaseId};
	$.ajax({     
        type: "POST",  
        url: editTemplateUrl,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(form),
        success: function (success) {
        	if (success.ok == true) {
        		templates[selectedTemplatePosition].name = editedTemplateName;
        		$("#template" + selectedTemplatePosition + " a").html(editedTemplateName);
        		addEditTemplateButton();
        	} else {
        		showEditTemplateErrorAlert();
        	}
        },
        error: function(e) {
            console.log(e);
            showEditTemplateErrorAlert();
        }
    });
}

function selectItem(position) {
	var item = $("#itemsList li:eq(" + position + ")");
	if ($(item).hasClass("active")) {
		removeSelectionOfItemInPosition(position);
		$(item).removeClass("active");
	} else {
		$(item).addClass("active");
		selectedItemsPositions.push(position);
	}
	if (selectedItemsPositions.length == 1) {
		$("#editItemButton").removeClass("disabled");
	} else {
		$("#editItemButton").addClass("disabled");
	}
	if (selectedItemsPositions.length > 0) {
		$("#deleteItemButton").removeClass("disabled");
	} else {
		$("#deleteItemButton").addClass("disabled");
	}
}

function removeSelectionOfItemInPosition(position) {
	for (i in selectedItemsPositions) {
		if (position == selectedItemsPositions[i]) {
			selectedItemsPositions.splice(i, 1);
		}
	}
}

function removeNoTemplateButtons() {
	$("#editTemplateDiv").html("");
	$("#deleteTemplateDiv").html("");
	$("#addItemDiv").html("");
	$("#editItemDiv").html("");
}

function addTemplate(templateName, templateId) {
	var template = {name:templateName, purchaseId:templateId};
	templates.push(template);
	addTemplatesToGroupPanel(templates.length - 1);
}

function deleteTemplate(position) {
	templates.splice(position, 1);
	addTemplatesToGroupPanel(0);
}

function showDeleteItemErrorAlert() {
	var content = "<div class=\"alert alert-danger alert-dismissable\"> " +
	  "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
	  "<strong>Deleting item error.</strong> Try again.</div>";
	$("#alertPlace").html(content);
}

function showEditItemErrorAlert() {
	var content = "<div class=\"alert alert-danger alert-dismissable\"> " +
	  "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
	  "<strong>Editing item error.</strong> Try again.</div>";
	$("#alertPlace").html(content);
}

function showDeleteTemplateErrorAlert() {
	var content = "<div class=\"alert alert-danger alert-dismissable\"> " +
	  "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
	  "<strong>Deleting template error.</strong> Try again.</div>";
	$("#alertPlace").html(content);
}

function showEditTemplateErrorAlert() {
	var content = "<div class=\"alert alert-danger alert-dismissable\"> " +
	  "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
	  "<strong>Editing template error.</strong> Try again.</div>";
	$("#alertPlace").html(content);
}

function showNewItemErrorAlert() {
	var content = "<div class=\"alert alert-danger alert-dismissable\"> " +
	  "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
	  "<strong>Adding item error.</strong> Try again.</div>";
	$("#alertPlace").html(content);
}

function showNewTemplateErrorAlert() {
	var content = "<div class=\"alert alert-danger alert-dismissable\"> " +
	  "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
	  "<strong>Creating template error.</strong> Try again.</div>";
	$("#alertPlace").html(content);
}

function showNoTemplatesAlert() {
	var content = "<div class=\"alert alert-info alert-dismissable\"> " +
	  "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
	  "<strong>No templates created.</strong> Try to create one.</div>";
	$("#alertPlace").html(content);
}