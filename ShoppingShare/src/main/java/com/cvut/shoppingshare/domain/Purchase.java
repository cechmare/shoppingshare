package com.cvut.shoppingshare.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity(name = "purchase")
@Table(name = "purchase")
@NamedQueries({
		@NamedQuery(name = Purchase.FIND_BY_SHAREGROUP_ID, query = "SELECT NEW com.cvut.shoppingshare.web.form.PurchaseWebDto(p.entityId, p.name) FROM purchase p WHERE p.sharegroup.entityId = :groupId AND p.state != com.cvut.shoppingshare.domain.EntityState.DELETED"),
		@NamedQuery(name = Purchase.FIND_NEWS_BY_SHAREGROUP_ID , query = "SELECT NEW com.cvut.shoppingshare.web.form.PurchaseDto(p.entityId, p.name, p.state) FROM purchase p WHERE p.sharegroup.entityId = :groupId AND p.updated > :lastSync")
})
public class Purchase extends ShoppingShareEntity {
	
	public static final String FIND_BY_SHAREGROUP_ID = "Purchase.findBySharegroupId";
	
	public static final String FIND_NEWS_BY_SHAREGROUP_ID = "Purchase.findNewsBySharegroupId";
	
	@ManyToOne
	@JoinColumn(name = "sharegroup_id")
	private ShareGroup sharegroup;
	
	@OneToMany(orphanRemoval = true, mappedBy = "purchase", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
	private List<Item> items;
	
	@Column(nullable = false)
	private String name;
	
	@Column(nullable = false)
	private Date updated;
	
	@Enumerated(EnumType.STRING)
	private EntityState state;
	
	public Purchase() {
		items = new ArrayList<Item>();
		updated = new Date();
		state = EntityState.NEW;
	}
	
	public Purchase(ShareGroup shareGroup, String name) {
		this();
		this.sharegroup = shareGroup;
		this.name = name;
	}
	
	public void addItem(Item item) {
		items.add(item);
	}
	
	public void removeAllItems() {
		items.clear();
	}
	
	@JsonIgnore
	public ShareGroup getSharegroup() {
		return sharegroup;
	}

	public void setGroup(ShareGroup group) {
		this.sharegroup = group;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonIgnore
	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	@JsonIgnore
	public EntityState getState() {
		return state;
	}

	public void setState(EntityState state) {
		this.state = state;
	}

}
