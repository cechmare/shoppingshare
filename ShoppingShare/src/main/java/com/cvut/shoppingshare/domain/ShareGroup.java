package com.cvut.shoppingshare.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

@Entity(name = "sharegroup")
@Table(name = "sharegroup")
@NamedQueries ({
	@NamedQuery(name = ShareGroup.FIND_WHERE_PARTICIPATE, query = "SELECT DISTINCT NEW com.cvut.shoppingshare.web.form.GroupListMessage(g.name, g.entityId) FROM sharegroup g INNER JOIN g.users u WHERE u.entityId = (:userId)"),
	@NamedQuery(name = ShareGroup.FIND_WHERE_PARTICIPATE_CREATED_SINCE_DATE, 
		query = "SELECT DISTINCT NEW com.cvut.shoppingshare.web.form.GroupListMessage(g.name, g.entityId) FROM sharegroup g JOIN g.users u WHERE u.entityId = :userId AND g.created > :lastSync"),
	@NamedQuery(name = ShareGroup.FIND_WHERE_PARTICIPATE_SINCE_DATE_BY_MEMBERSHIP_REQUEST, query = "SELECT NEW com.cvut.shoppingshare.web.form.GroupListMessage(m.sharegroup.name, m.sharegroup.entityId) FROM "
			+ "membershiprequest m WHERE m.shareuser.entityId = :userId AND m.dealt > :lastSync AND m.membershipRequestState = com.cvut.shoppingshare.domain.MembershipRequestState.ACCEPTED"),
	@NamedQuery(name = ShareGroup.FIND_BY_NAME_STARTS_WITH, query = "SELECT NEW com.cvut.shoppingshare.web.form.GroupListMessage(g.name, g.entityId) FROM sharegroup g WHERE g.name LIKE :tagName")
})
public class ShareGroup extends ShoppingShareEntity {
	
	public static final String FIND_WHERE_PARTICIPATE = "ShareGroup.findWhereParticipate";
	
	public static final String FIND_WHERE_PARTICIPATE_CREATED_SINCE_DATE = "ShareGroup.findWhereParticipateSinceDate";
	
	public static final String FIND_WHERE_PARTICIPATE_SINCE_DATE_BY_MEMBERSHIP_REQUEST = "ShareGroup.findWhereParticipateSinceDateByMembershipRequest";

	public static final String FIND_BY_NAME_STARTS_WITH = "ShareGroup.findByNameStartsWith";
	
	@OneToMany(mappedBy = "sharegroup", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
	private List<Purchase> purchases;
	
	@OneToMany(mappedBy = "sharegroup")
	private List<MembershipRequest> membershipRequests;
	
	@ManyToMany
	private List<ShareUser> users;
	
	@Index(name = "sharegroup_name")
	@Column(nullable = false)
	private String name;
	
	@Column(nullable = false)
	private Date created;
	
	public ShareGroup() {
		purchases = new ArrayList<Purchase>();
		membershipRequests = new ArrayList<MembershipRequest>();
		users = new ArrayList<ShareUser>();
	}
	
	public ShareGroup(String name, ShareUser founder) {
		this();
		this.name = name;
		this.created = new Date();
		addUser(founder);
	}
	
	public void addUser(ShareUser user) {
		users.add(user);
	}
	
	public void addPurchase(Purchase purchase) {
		purchases.add(purchase);
	}
	
	public void addMembershipRequest(MembershipRequest membershipRequest) {
		membershipRequests.add(membershipRequest);
	}

	public List<Purchase> getPurchases() {
		return purchases;
	}

	public void setPurchases(List<Purchase> purchases) {
		this.purchases = purchases;
	}

	public List<MembershipRequest> getMembershipRequests() {
		return membershipRequests;
	}

	public void setMembershipRequests(List<MembershipRequest> membershipRequests) {
		this.membershipRequests = membershipRequests;
	}

	public List<ShareUser> getUsers() {
		return users;
	}

	public void setUsers(List<ShareUser> users) {
		this.users = users;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}
	
}
