package com.cvut.shoppingshare.domain;

public enum MembershipRequestState {
	NEW, ACCEPTED, REJECTED
}
