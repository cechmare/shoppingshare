package com.cvut.shoppingshare.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity(name = "purchasetemplate")
@Table(name = "purchasetemplate")
@NamedQueries({
	@NamedQuery(name = PurchaseTemplate.FIND_NEWS_BY_USER_ID, query = "SELECT NEW com.cvut.shoppingshare.web.form.PurchaseDto(t.entityId, t.name, t.state) FROM purchasetemplate t WHERE t.shareuser.entityId = :userId AND t.updated > :lastSync"),
	@NamedQuery(name = PurchaseTemplate.GET_ALL, query = "SELECT NEW com.cvut.shoppingshare.web.form.PurchaseWebDto(t.entityId, t.name) FROM purchasetemplate t WHERE t.shareuser.entityId = :userId AND t.state != com.cvut.shoppingshare.domain.EntityState.DELETED")
})
public class PurchaseTemplate extends ShoppingShareEntity {
	
	public static final String FIND_NEWS_BY_USER_ID = "Purchase.findNewsByUserId";
	
	public static final String GET_ALL = "Purchase.getAll";
	
	@ManyToOne
	@JoinColumn(name = "shareuser_id")
	private ShareUser shareuser;
	
	@OneToMany(orphanRemoval = true, mappedBy = "template", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
	private List<Item> items;
	
	@Column(nullable = false)
	private String name;
	
	@Column(nullable = false)
	private Date updated;
	
	@Enumerated(EnumType.STRING)
	private EntityState state;

	public PurchaseTemplate() {
		state = EntityState.NEW;
		items = new ArrayList<Item>();
		updated = new Date();
	}

	public PurchaseTemplate(String name) {
		this();
		this.name = name;
	}

	public PurchaseTemplate(ShareUser shareuser, String name) {
		this();
		this.shareuser = shareuser;
		this.name = name;
	}

	public PurchaseTemplate(ShareUser shareuser, String name, Date updated,
			EntityState state) {
		items = new ArrayList<Item>();
		this.shareuser = shareuser;
		this.name = name;
		this.updated = updated;
		this.state = state;
	}
	
	public void addItem(Item item) {
		items.add(item);
	}
	
	public void removeAllItems() {
		items.clear();
	}

	public ShareUser getShareuser() {
		return shareuser;
	}

	public void setShareuser(ShareUser shareuser) {
		this.shareuser = shareuser;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public EntityState getState() {
		return state;
	}

	public void setState(EntityState state) {
		this.state = state;
	}

}
