package com.cvut.shoppingshare.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity(name = "item")
@Table(name = "item")
@NamedQueries({
	@NamedQuery(name = Item.FIND_NEWS_BY_SHAREGROUP_ID , query = "SELECT NEW com.cvut.shoppingshare.web.form.ItemDto(p.entityId, i.entityId, i.name, i.state) FROM item i JOIN i.purchase p WHERE p.sharegroup.entityId = :groupId AND i.updated > :lastSync"),
	@NamedQuery(name = Item.FIND_TEMPLATES_NEWS_BY_USER_ID, query = "SELECT NEW com.cvut.shoppingshare.web.form.ItemDto(t.entityId, i.entityId, i.name, i.state) FROM item i JOIN i.template t WHERE t.shareuser.entityId = :userId AND i.updated > :lastSync"),
	@NamedQuery(name = Item.FIND_TEMPLATES_BY_PURCHASE_TEMPLATE_ID, query = "SELECT NEW com.cvut.shoppingshare.web.form.ItemTemplateDto(i.entityId, i.name) FROM item i WHERE i.template.entityId = :templateId AND i.state != com.cvut.shoppingshare.domain.ItemState.DELETED"),
	@NamedQuery(name = Item.FIND_NON_DELETED_BY_PURCHASE_ID, query = "SELECT NEW java.lang.String(i.name) FROM item i WHERE i.purchase.entityId = :purchaseId AND i.state != com.cvut.shoppingshare.domain.ItemState.DELETED"),
	@NamedQuery(name = Item.GET_ALL_OF_PURCHASE, query = "SELECT NEW com.cvut.shoppingshare.web.form.ItemWebDto(i.entityId, i.name, i.state) FROM item i WHERE i.purchase.entityId = :purchaseId AND i.state != com.cvut.shoppingshare.domain.ItemState.DELETED ORDER BY i.state DESC"),
	@NamedQuery(name = Item.FIND_NON_DELETED_BY_PURCHASE_TEMPLATE_ID, query = "SELECT NEW java.lang.String(i.name) FROM item i WHERE i.template.entityId = :templateId AND i.state != com.cvut.shoppingshare.domain.ItemState.DELETED")
})
public class Item extends ShoppingShareEntity {
	
	public static final String FIND_NEWS_BY_SHAREGROUP_ID = "Item.findNewsBySharegroupId";
	
	public static final String FIND_TEMPLATES_NEWS_BY_USER_ID = "Item.findTemplatesNewsByUserId";
	
	public static final String FIND_TEMPLATES_BY_PURCHASE_TEMPLATE_ID = "Item.findTemplatesByPurchaseTemplateId";
	
	public static final String FIND_NON_DELETED_BY_PURCHASE_ID = "Item.findNonDeletedByPurchaseId";
	
	public static final String GET_ALL_OF_PURCHASE = "Item.getAllOfPurchase";
	
	public static final String FIND_NON_DELETED_BY_PURCHASE_TEMPLATE_ID = "Item.findNonDeletedByPurchaseTemplateId";
	
	@ManyToOne
	@JoinColumn(name = "purchase_id")
	private Purchase purchase;
	
	@ManyToOne
	@JoinColumn(name = "purchasetemplate_id")
	private PurchaseTemplate template;
	
	@Column(nullable = false)
	private String name;
	
	@Column(nullable = false)
	private Date updated;
	
	@Enumerated(EnumType.STRING)
	private ItemState state;

	public Item() {
		updated = new Date();
		state = ItemState.NEW;
	}

	public Item(Purchase purchase, String name) {
		this();
		this.purchase = purchase;
		this.name = name;
	}
	
	public Item(PurchaseTemplate template, String name) {
		this();
		this.template = template;
		this.name = name;
	}

	public Item(PurchaseTemplate template, String name, Date updated) {
		this.template = template;
		this.name = name;
		this.updated = updated;
	}

	@JsonIgnore
	public Purchase getPurchase() {
		return purchase;
	}

	public void setPurchase(Purchase purchase) {
		this.purchase = purchase;
	}

	@JsonIgnore
	public PurchaseTemplate getTemplate() {
		return template;
	}

	public void setTemplate(PurchaseTemplate template) {
		this.template = template;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonIgnore
	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	@JsonIgnore
	public ItemState getState() {
		return state;
	}

	public void setState(ItemState state) {
		this.state = state;
	}

}
