package com.cvut.shoppingshare.domain;

public enum EntityState {
	NEW, DELETED
}
