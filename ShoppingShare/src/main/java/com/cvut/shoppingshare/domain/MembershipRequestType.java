package com.cvut.shoppingshare.domain;

public enum MembershipRequestType {
	OFFER, REQUEST
}
