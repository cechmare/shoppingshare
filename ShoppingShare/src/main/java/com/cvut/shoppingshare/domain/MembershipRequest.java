package com.cvut.shoppingshare.domain;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


@Entity(name = "membershiprequest")
@Table(name = "membershiprequest")
@NamedQueries({
	@NamedQuery(name = MembershipRequest.GET_DEALT_REQUESTS_OF_GROUP_SINCE_DATE, query = "SELECT NEW com.cvut.shoppingshare.web.form.DealtMembershipRequest(m.entityId, m.membershipRequestState) FROM membershiprequest m WHERE m.sharegroup.entityId = :groupId AND m.dealt > :lastSync"),
	@NamedQuery(name = MembershipRequest.GET_NON_DEALT_REQUESTS_OF_GROUP_SINCE_DATE, query = "SELECT NEW com.cvut.shoppingshare.web.form.MembershipRequestDto(m.membershipRequestType, m.entityId, m.shareuser.entityId, m.shareuser.login, m.shareuser.firstname, m.shareuser.lastname) FROM membershiprequest m WHERE m.sharegroup.entityId = :groupId AND m.created > :lastSync"),
	@NamedQuery(name = MembershipRequest.GET_OFFERS_OF_USER_SINCE_DATE, query = "SELECT NEW com.cvut.shoppingshare.web.form.MembershipOfferDto(m.entityId, m.sharegroup.name) FROM membershiprequest m WHERE m.shareuser.entityId = :userId AND m.membershipRequestType = com.cvut.shoppingshare.domain.MembershipRequestType.OFFER AND m.created > :lastSync AND m.dealt IS NULL"),
	@NamedQuery(name = MembershipRequest.GET_OFFERS_OF_USER, query = "SELECT NEW com.cvut.shoppingshare.web.form.MembershipOfferDto(m.entityId, m.sharegroup.name) FROM membershiprequest m WHERE m.shareuser.entityId = :userId AND m.membershipRequestType = com.cvut.shoppingshare.domain.MembershipRequestType.OFFER AND m.dealt IS NULL"),
	@NamedQuery(name = MembershipRequest.GET_REQUESTS_OF_GROUP, query = "SELECT NEW com.cvut.shoppingshare.web.form.ShareUserDto(m.entityId, m.shareuser.login, m.shareuser.firstname, m.shareuser.lastname) FROM membershiprequest m WHERE m.sharegroup.entityId = :groupId AND m.membershipRequestType = com.cvut.shoppingshare.domain.MembershipRequestType.REQUEST AND m.dealt IS NULL"),
	@NamedQuery(name = MembershipRequest.GET_OFFERS_OF_GROUP, query = "SELECT NEW com.cvut.shoppingshare.web.form.ShareUserDto(m.entityId, m.shareuser.login, m.shareuser.firstname, m.shareuser.lastname) FROM membershiprequest m WHERE m.sharegroup.entityId = :groupId AND m.membershipRequestType = com.cvut.shoppingshare.domain.MembershipRequestType.OFFER AND m.dealt IS NULL ORDER BY m.created DESC")
})
public class MembershipRequest extends ShoppingShareEntity {
	
	public static final String GET_DEALT_REQUESTS_OF_GROUP_SINCE_DATE = "MembershipRequest.getDealtRequestsOfGroupSinceDate";
	
	public static final String GET_NON_DEALT_REQUESTS_OF_GROUP_SINCE_DATE = "MembershipRequest.getNonDealtRequestsOfGroupSinceDate";
	
	public static final String GET_OFFERS_OF_USER_SINCE_DATE = "MembershipRequest.getOffersOfUserSinceDate";
	
	public static final String GET_OFFERS_OF_USER = "MembershipRequest.getOffersOfUser";
	
	public static final String GET_REQUESTS_OF_GROUP = "MembershipRequest.getRequestsOfGroup";
	
	public static final String GET_OFFERS_OF_GROUP = "MembershipRequest.getOffersOfGroup";

	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name = "sharegroup_id")
	private ShareGroup sharegroup;
	
	@ManyToOne
	@JoinColumn(name = "shareuser_id")
	private ShareUser shareuser;
	
	@Column(nullable = false)
	private Date created;
	
	@Column(nullable = true)
	private Date dealt;
	
	@Enumerated(EnumType.STRING)
	private MembershipRequestType membershipRequestType;

	@Enumerated(EnumType.STRING)
	private MembershipRequestState membershipRequestState;
	
	public MembershipRequest() {
		this.created = new Date();
	}

	public MembershipRequest(ShareGroup sharegroup, ShareUser shareuser,
			MembershipRequestType membershipRequestType,
			MembershipRequestState membershipRequestState) {
		this();
		this.sharegroup = sharegroup;
		this.shareuser = shareuser;
		this.membershipRequestType = membershipRequestType;
		this.membershipRequestState = membershipRequestState;
	}

	public ShareUser getShareuser() {
		return shareuser;
	}

	public void setShareuser(ShareUser shareuser) {
		this.shareuser = shareuser;
	}

	public ShareGroup getSharegroup() {
		return sharegroup;
	}

	public void setSharegroup(ShareGroup sharegroup) {
		this.sharegroup = sharegroup;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getDealt() {
		return dealt;
	}

	public void setDealt(Date dealt) {
		this.dealt = dealt;
	}

	public MembershipRequestType getMembershipRequestType() {
		return membershipRequestType;
	}

	public void setMembershipRequestType(MembershipRequestType membershipRequestType) {
		this.membershipRequestType = membershipRequestType;
	}

	public MembershipRequestState getMembershipRequestState() {
		return membershipRequestState;
	}

	public void setMembershipRequestState(
			MembershipRequestState membershipRequestState) {
		this.membershipRequestState = membershipRequestState;
	}
	
}
