package com.cvut.shoppingshare.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

@Entity(name = "shareuser")
@Table(name = "shareuser")
@NamedQueries ({
	@NamedQuery(name = ShareUser.FIND_BY_LOGIN, query = "SELECT s FROM shareuser s WHERE s.login = (:login)"),
	@NamedQuery(name = ShareUser.FIND_BY_IDS, query = "SELECT s FROM shareuser s WHERE s.entityId IN (:ids)"),
	@NamedQuery(name = ShareUser.GET_SHARE_USERS_OF_GROUP, query = "SELECT NEW com.cvut.shoppingshare.web.form.ShareUserDto(u.entityId, u.login, u.firstname, u.lastname) FROM shareuser u INNER JOIN u.groups g WHERE  g.entityId = :groupId"),
	@NamedQuery(name = ShareUser.FIND_BY_LOGIN_FIRSTNAME_LASTNAME_STARTS_WITH, query = "SELECT NEW com.cvut.shoppingshare.web.form.UserAutocompleteForm(u.entityId, u.firstname, u.lastname, u.login) FROM shareuser u WHERE u.firstname LIKE :tagName OR u.lastname LIKE :tagName OR u.login LIKE :tagName OR CONCAT(u.firstname, ' ', u.lastname) LIKE :tagName"),
	@NamedQuery(name = ShareUser.GET_ALL_OF_GROUP_WITH_ID, query = "SELECT NEW com.cvut.shoppingshare.web.form.ShareUserWebDto(u.login, u.firstname, u.lastname) FROM shareuser u INNER JOIN u.groups g WHERE  g.entityId = :groupId")
})
public class ShareUser extends ShoppingShareEntity {
	
	public static final String FIND_BY_LOGIN = "ShareUser.findByLogin";
	
	public static final String FIND_BY_IDS = "ShareUser.findByIds";
	
	public static final String GET_SHARE_USERS_OF_GROUP = "ShareUser.getShareUsersOfGroup";
	
	public static final String FIND_BY_LOGIN_FIRSTNAME_LASTNAME_STARTS_WITH = "ShareUser.findByLoginFirstnameLastnameStartsWith";
	
	public static final String GET_ALL_OF_GROUP_WITH_ID = "ShareUser.getAllOfGroupWithId";
	
	@OneToMany(mappedBy = "shareuser")
	private List<MembershipRequest> membershipRequests;
	
	@OneToMany(mappedBy = "shareuser", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
	private List<PurchaseTemplate> templates;
	
	@ManyToMany
	@JoinTable(
	      name="shareuser_sharegroup",
	      joinColumns={@JoinColumn(name="shareuser_id", referencedColumnName="entityId")},
	      inverseJoinColumns={@JoinColumn(name="sharegroup_id", referencedColumnName="entityId")})
	private List<ShareGroup> groups;
	
	@Index(name = "shareuser_login")
	@Column(nullable = false, unique = true)
	private String login;

	@Column(nullable = false)
	private String password;
	
	@Index(name = "shareuser_firtname")
	@Column(nullable = true)
	private String firstname;
	
	@Index(name = "shareuser_lastname")
	@Column(nullable = true)
	private String lastname;
	
	public ShareUser() {
		membershipRequests = new ArrayList<MembershipRequest>();
		groups = new ArrayList<ShareGroup>();
		templates = new ArrayList<PurchaseTemplate>();
	}
	
	public void addGroup(ShareGroup shareGroup) {
		groups.add(shareGroup);
	}
	
	public void addMembershipRequest(MembershipRequest membershipRequest) {
		membershipRequests.add(membershipRequest);
	}
	
	public void addTemplate(PurchaseTemplate template) {
		templates.add(template);
	}
	
	public List<MembershipRequest> getMembershipRequests() {
		return membershipRequests;
	}

	public void setMembershipRequests(List<MembershipRequest> membershipRequests) {
		this.membershipRequests = membershipRequests;
	}

	public List<ShareGroup> getGroups() {
		return groups;
	}

	public void setGroups(List<ShareGroup> groups) {
		this.groups = groups;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public List<PurchaseTemplate> getTemplates() {
		return templates;
	}

	public void setTemplates(List<PurchaseTemplate> templates) {
		this.templates = templates;
	}

}
