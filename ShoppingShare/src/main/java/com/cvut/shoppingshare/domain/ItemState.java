package com.cvut.shoppingshare.domain;

public enum ItemState {
	NEW, BOUGHT, DELETED
}
