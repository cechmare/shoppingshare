package com.cvut.shoppingshare.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cvut.shoppingshare.service.PurchaseService;
import com.cvut.shoppingshare.web.form.ConfirmMessage;
import com.cvut.shoppingshare.web.form.DoubleEntityForm;
import com.cvut.shoppingshare.web.form.EditEntityForm;
import com.cvut.shoppingshare.web.form.EntityForm;
import com.cvut.shoppingshare.web.form.NewsRequestMessage;
import com.cvut.shoppingshare.web.form.PurchaseWebDto;
import com.cvut.shoppingshare.web.form.PurchasesItemsDto;

@Controller
public class PurchaseController {
	
	@Autowired
	private PurchaseService purchaseService;
	
	@RequestMapping(value="/", method = RequestMethod.GET)
	public String main() {
		return "purchases";
	}
	
	@RequestMapping(value="/purchase/all", method=RequestMethod.POST, produces = "application/json",
			consumes = "application/json")
	public @ResponseBody PurchaseWebDto[] getAll(@RequestBody Long groupId) {
		return purchaseService.getAllPurchasesOfGroup(groupId);
	}
	
	@RequestMapping(value="/purchase/news", method=RequestMethod.POST, produces = "application/json",
			consumes = "application/json")
	public @ResponseBody PurchasesItemsDto getPurchasesNews(@RequestBody NewsRequestMessage request) {
		return purchaseService.getPurchasesItemsNews(request);
	}
	
	@RequestMapping(value="/purchase", method=RequestMethod.PUT)
	public @ResponseBody Long addPurchase(@RequestBody EntityForm purchaseForm) {
		if (purchaseForm == null || purchaseForm.getEntityId() == null || purchaseForm.getName() == null) {
			return null;
		}
		return purchaseService.add(purchaseForm);	
	}
	
	@RequestMapping(value="/purchase/fromtemplate", method=RequestMethod.PUT, produces = "application/json",
			consumes = "application/json")
	public @ResponseBody Long addPurchaseFromTemplate(@RequestBody DoubleEntityForm form) {
		return purchaseService.addPurchaseFromTemplate(form);
	}
	
	@RequestMapping(value="/purchase", method=RequestMethod.POST, produces = "application/json",
			consumes = "application/json")
	public @ResponseBody ConfirmMessage editPurchase(@RequestBody EditEntityForm form) {
		boolean success = purchaseService.edit(form);
		return new ConfirmMessage(success);
	}
	
	@RequestMapping(value="/purchase/web", method=RequestMethod.POST, produces = "application/json",
			consumes = "application/json")
	public @ResponseBody ConfirmMessage editPurchase(@RequestBody EntityForm form) {
		boolean success = purchaseService.edit(form);
		return new ConfirmMessage(success);
	}
	
	@RequestMapping(value="/purchase", method=RequestMethod.DELETE)
	public @ResponseBody ConfirmMessage removePurchase(@RequestBody Long purchaseId) {
		boolean success = purchaseService.markAsRemoved(purchaseId);
		return new ConfirmMessage(success);
	}

	public void setPurchaseService(PurchaseService purchaseService) {
		this.purchaseService = purchaseService;
	}

}
