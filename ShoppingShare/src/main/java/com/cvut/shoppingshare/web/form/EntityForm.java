package com.cvut.shoppingshare.web.form;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

public class EntityForm {
	
	private String name;
	
	private Long entityId;

	@JsonCreator
	public EntityForm(@JsonProperty("name") String name, @JsonProperty("entityId") Long entityId) {
		this.name = name;
		this.entityId = entityId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getEntityId() {
		return entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

}
