package com.cvut.shoppingshare.web.form;

import java.util.List;

public class PurchasesItemsDto {

	private List<PurchaseDto> purchases;
	
	private List<ItemDto> items;

	public PurchasesItemsDto(List<PurchaseDto> purchases, List<ItemDto> items) {
		this.purchases = purchases;
		this.items = items;
	}

	public List<PurchaseDto> getPurchases() {
		return purchases;
	}

	public void setPurchases(List<PurchaseDto> purchases) {
		this.purchases = purchases;
	}

	public List<ItemDto> getItems() {
		return items;
	}

	public void setItems(List<ItemDto> items) {
		this.items = items;
	}
	
}
