package com.cvut.shoppingshare.web.form;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

public class AutocompleteForm {
	
	private String tag;

	@JsonCreator
	public AutocompleteForm(@JsonProperty("tag") String tag) {
		this.tag = tag;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

}
