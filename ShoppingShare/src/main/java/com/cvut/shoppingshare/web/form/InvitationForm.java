package com.cvut.shoppingshare.web.form;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

public class InvitationForm {

	private Long userId;
	
	private Long groupId;

	@JsonCreator
	public InvitationForm(@JsonProperty("userId") Long userId, @JsonProperty("groupId") Long groupId) {
		this.userId = userId;
		this.groupId = groupId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}
	
}
