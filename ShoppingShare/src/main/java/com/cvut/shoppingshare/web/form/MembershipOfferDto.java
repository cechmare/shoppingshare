package com.cvut.shoppingshare.web.form;

public class MembershipOfferDto {
	
	private Long requestId;
	
	private String groupName;

	public MembershipOfferDto() {
		super();
	}

	public MembershipOfferDto(Long requestId, String groupName) {
		this.requestId = requestId;
		this.groupName = groupName;
	}

	public Long getRequestId() {
		return requestId;
	}

	public void setRequestId(Long requestId) {
		this.requestId = requestId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

}
