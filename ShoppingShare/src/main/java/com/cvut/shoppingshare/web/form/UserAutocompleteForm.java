package com.cvut.shoppingshare.web.form;

public class UserAutocompleteForm {

	private Long id;
	
	private String name;

	public UserAutocompleteForm(Long id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public UserAutocompleteForm(Long id, String firstname, String lastname, String login) {
		this.id = id;
		this.name = firstname + " " + lastname + " (" + login + ")";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
