package com.cvut.shoppingshare.web.form;

import java.util.List;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

public class GroupForm {

	private String name;
	
	private List<Long> userIdsToInvite;
	
	public GroupForm() {
	}

	@JsonCreator
	public GroupForm(@JsonProperty("name") String name, @JsonProperty("userIdsToInvite") List<Long> userIdsToInvite) {
		this.name = name;
		this.userIdsToInvite = userIdsToInvite;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Long> getUserIdsToInvite() {
		return userIdsToInvite;
	}

	public void setUserIdsToInvite(List<Long> userIdsToInvite) {
		this.userIdsToInvite = userIdsToInvite;
	}
	
}
