package com.cvut.shoppingshare.web.form;

import com.cvut.shoppingshare.domain.ItemState;

public class ItemWebDto extends ItemTemplateDto {

	private ItemState state;

	public ItemWebDto(Long itemId, String name, ItemState state) {
		super(itemId, name);
		this.state = state;
	}

	public ItemState getState() {
		return state;
	}

	public void setState(ItemState state) {
		this.state = state;
	}
	
}
