package com.cvut.shoppingshare.web.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cvut.shoppingshare.service.GroupService;
import com.cvut.shoppingshare.web.form.AutocompleteForm;
import com.cvut.shoppingshare.web.form.GroupForm;
import com.cvut.shoppingshare.web.form.GroupListMessage;

@Controller
public class GroupController {
	
	@Autowired
	private GroupService groupService;
	
	@RequestMapping(value="/group", method=RequestMethod.PUT)
	public @ResponseBody Long addGroup(@RequestBody GroupForm form) {
		return groupService.add(getLoggedUserId(), form);
	}
	
	@RequestMapping(value="/group/web/all", method=RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public @ResponseBody GroupListMessage[] getAll() {
		return groupService.getAllGroupsWhereParticipate(getLoggedUserId());
	}
	
	@RequestMapping(value="/group/all", method=RequestMethod.POST, produces = "application/json",
			consumes = "application/json")
	public @ResponseBody GroupListMessage[] getAllSinceDate(@RequestBody Date lastSync) {
		return groupService.getAllGroupsWhereParticipateSinceDate(getLoggedUserId(), lastSync);
	}
	
	@RequestMapping(value = "/group/startsWith", method = RequestMethod.POST, produces = "application/json",
			consumes = "application/json")
	public @ResponseBody GroupListMessage[] getTags(@RequestBody AutocompleteForm form) {
		return groupService.findByNameStartsWith(form.getTag());
	}
	
	private Long getLoggedUserId() {
		return (Long) SecurityContextHolder.getContext().getAuthentication().getDetails();
	}

	public void setGroupService(GroupService groupService) {
		this.groupService = groupService;
	}

}
