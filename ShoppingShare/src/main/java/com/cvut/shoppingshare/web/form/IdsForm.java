package com.cvut.shoppingshare.web.form;

import java.util.List;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

public class IdsForm {

	private List<Long> ids;

	@JsonCreator
	public IdsForm(@JsonProperty("ids") List<Long> ids) {
		this.ids = ids;
	}

	public List<Long> getIds() {
		return ids;
	}

	public void setIds(List<Long> ids) {
		this.ids = ids;
	}
	
}
