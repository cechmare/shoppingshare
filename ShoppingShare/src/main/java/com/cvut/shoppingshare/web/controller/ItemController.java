package com.cvut.shoppingshare.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cvut.shoppingshare.service.ItemService;
import com.cvut.shoppingshare.web.form.ConfirmMessage;
import com.cvut.shoppingshare.web.form.EditEntityForm;
import com.cvut.shoppingshare.web.form.EntityForm;
import com.cvut.shoppingshare.web.form.IdsForm;
import com.cvut.shoppingshare.web.form.ItemTemplateDto;
import com.cvut.shoppingshare.web.form.ItemWebDto;

@Controller
public class ItemController {
	
	@Autowired
	private ItemService itemService;
	
	@RequestMapping(value="/item", method=RequestMethod.DELETE)
	public @ResponseBody ConfirmMessage removeItem(@RequestBody Long itemId) {
		boolean success = itemService.markAsRemoved(itemId);
		if (success) {
			return new ConfirmMessage(true);
		} else {
			return new ConfirmMessage(false);
		}
	}
	
	@RequestMapping(value="/item", method=RequestMethod.PUT)
	public @ResponseBody Long addItem(@RequestBody EntityForm itemForm) {
		if (itemForm == null || itemForm.getEntityId() == null || itemForm.getName() == null) {
			return null;
		}
		return itemService.addItem(itemForm);	
	}
	
	@RequestMapping(value="/item", method=RequestMethod.POST, produces = "application/json",
			consumes = "application/json")
	public @ResponseBody ConfirmMessage editItemTemplate(@RequestBody EditEntityForm form) {
		boolean success = itemService.editItem(form);
		return new ConfirmMessage(success);
	}
	
	@RequestMapping(value="/item/all", method=RequestMethod.DELETE, produces = "application/json",
			consumes = "application/json")
	public @ResponseBody ConfirmMessage deleteAll(@RequestBody IdsForm form) {
		boolean success = itemService.markAsRemoved(form);
		return new ConfirmMessage(success);
	}
	
	@RequestMapping(value="/item/all", method=RequestMethod.POST, produces = "application/json",
			consumes = "application/json")
	public @ResponseBody ItemWebDto[] getAll(@RequestBody Long purchaseId) {
		return itemService.getAll(purchaseId);
	}
	
	@RequestMapping(value="/item/all/bought", method=RequestMethod.POST, produces = "application/json",
			consumes = "application/json")
	public @ResponseBody ConfirmMessage markAsBought(@RequestBody IdsForm form) {
		boolean success = itemService.markAsBought(form);
		return new ConfirmMessage(success);
	}
	
	@RequestMapping(value="/item/web", method=RequestMethod.POST, produces = "application/json",
			consumes = "application/json")
	public @ResponseBody ConfirmMessage editItemWeb(@RequestBody EntityForm form) {
		boolean success = itemService.editItem(form);
		return new ConfirmMessage(success);
	}
	
	@RequestMapping(value="/item/template", method=RequestMethod.PUT, produces = "application/json",
			consumes = "application/json")
	public @ResponseBody Long addItemTemplate(@RequestBody EntityForm itemForm) {
		if (itemForm == null || itemForm.getEntityId() == null || itemForm.getName() == null) {
			return null;
		}
		return itemService.addItemTemplate(itemForm);	
	}
	
	@RequestMapping(value="/item/template/all", method=RequestMethod.POST, produces = "application/json",
			consumes = "application/json")
	public @ResponseBody ItemTemplateDto[] getAllTemplates(@RequestBody Long templateId) {
		return itemService.getAllTemplates(templateId);
	}

	public void setItemService(ItemService itemService) {
		this.itemService = itemService;
	}

}
