package com.cvut.shoppingshare.web.form;

public class PurchaseWebDto {

	private Long purchaseId;

	private String name;

	public PurchaseWebDto(Long purchaseId, String name) {
		this.purchaseId = purchaseId;
		this.name = name;
	}

	public Long getPurchaseId() {
		return purchaseId;
	}

	public void setPurchaseId(Long purchaseId) {
		this.purchaseId = purchaseId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
