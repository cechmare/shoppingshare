package com.cvut.shoppingshare.web.form;

import java.util.List;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

public class TemplateForm {
	
	private String name;
	
	private List<String> items;

	@JsonCreator
	public TemplateForm(@JsonProperty("name") String name, @JsonProperty("items") List<String> items) {
		this.name = name;
		this.items = items;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getItems() {
		return items;
	}

	public void setItems(List<String> items) {
		this.items = items;
	}

}
