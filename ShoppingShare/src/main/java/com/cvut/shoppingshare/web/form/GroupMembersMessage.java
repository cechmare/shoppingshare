package com.cvut.shoppingshare.web.form;

import java.util.List;

public class GroupMembersMessage {

	private List<ShareUserDto> users;
	
	private List<MembershipRequestDto> requests;
	
	private List<DealtMembershipRequest> dealtRequests;

	public GroupMembersMessage() {
	}

	public List<ShareUserDto> getUsers() {
		return users;
	}

	public void setUsers(List<ShareUserDto> users) {
		this.users = users;
	}

	public List<MembershipRequestDto> getRequests() {
		return requests;
	}

	public void setRequests(List<MembershipRequestDto> requests) {
		this.requests = requests;
	}

	public List<DealtMembershipRequest> getDealtRequests() {
		return dealtRequests;
	}

	public void setDealtRequests(List<DealtMembershipRequest> dealtRequests) {
		this.dealtRequests = dealtRequests;
	}
	
}
