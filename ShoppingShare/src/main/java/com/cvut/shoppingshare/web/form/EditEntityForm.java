package com.cvut.shoppingshare.web.form;

import java.util.Date;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

public class EditEntityForm extends EntityForm {
	
	private Date lastSync;

	@JsonCreator
	public EditEntityForm(@JsonProperty("lastSync") Date lastSync, @JsonProperty("name") String name, 
			@JsonProperty("entityId") Long entityId) {
		super(name, entityId);
		this.lastSync = lastSync;
	}

	public Date getLastSync() {
		return lastSync;
	}

	public void setLastSync(Date lastSync) {
		this.lastSync = lastSync;
	}

}
