package com.cvut.shoppingshare.web.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cvut.shoppingshare.service.PurchaseTemplateService;
import com.cvut.shoppingshare.web.form.ConfirmMessage;
import com.cvut.shoppingshare.web.form.EditEntityForm;
import com.cvut.shoppingshare.web.form.EntityForm;
import com.cvut.shoppingshare.web.form.PurchaseWebDto;
import com.cvut.shoppingshare.web.form.PurchasesItemsDto;
import com.cvut.shoppingshare.web.form.TemplateForm;
import com.cvut.shoppingshare.web.form.TemplateFromExistingForm;

@Controller
public class PurchaseTemplateController {

	@Autowired
	private PurchaseTemplateService purchaseTemplateService;

	@RequestMapping(value="/purchasetemplate/news", method=RequestMethod.POST, produces = "application/json",
			consumes = "application/json")
	public @ResponseBody PurchasesItemsDto getNews(@RequestBody Date lastSync) {
		return purchaseTemplateService.getNews(lastSync, getLoggedUserId());
	}
	
	@RequestMapping(value="/purchasetemplate", method=RequestMethod.PUT, produces = "application/json",
			consumes = "application/json")
	public @ResponseBody ConfirmMessage addPurchaseTemplateFromExisting(@RequestBody TemplateFromExistingForm form) {
		boolean success = purchaseTemplateService.add(form, getLoggedUserId());
		return new ConfirmMessage(success);
	}
	
	@RequestMapping(value="/purchasetemplate/form", method=RequestMethod.PUT, produces = "application/json",
			consumes = "application/json")
	public @ResponseBody ConfirmMessage addPurchaseTemplate(@RequestBody TemplateForm form) {
		boolean success = purchaseTemplateService.add(form, getLoggedUserId());
		return new ConfirmMessage(success);
	}
	
	@RequestMapping(value="/purchasetemplate/web/form", method=RequestMethod.PUT, produces = "application/json",
			consumes = "application/json")
	public @ResponseBody Long addPurchaseTemplate(@RequestBody String name) {
		return purchaseTemplateService.add(name, getLoggedUserId());
	}
	
	@RequestMapping(value="/purchasetemplate", method=RequestMethod.DELETE, produces = "application/json",
			consumes = "application/json")
	public @ResponseBody ConfirmMessage removePurchase(@RequestBody Long templateId) {
		boolean success = purchaseTemplateService.markAsRemoved(templateId);
		return new ConfirmMessage(success);
	}
	
	@RequestMapping(value="/purchasetemplate", method=RequestMethod.POST, produces = "application/json",
			consumes = "application/json")
	public @ResponseBody ConfirmMessage editPurchaseTemplate(@RequestBody EditEntityForm form) {
		boolean success = purchaseTemplateService.edit(form);
		return new ConfirmMessage(success);
	}
	
	@RequestMapping(value="/purchasetemplate/web", method=RequestMethod.POST, produces = "application/json",
			consumes = "application/json")
	public @ResponseBody ConfirmMessage editPurchaseTemplate(@RequestBody EntityForm form) {
		boolean success = purchaseTemplateService.edit(form);
		return new ConfirmMessage(success);
	}
	
	@RequestMapping(value="/purchasetemplate", method=RequestMethod.GET, produces = "application/json")
	public @ResponseBody PurchaseWebDto[] getAll() {
		Long userId = getLoggedUserId();
		return purchaseTemplateService.getAll(userId);
	}
	
	@RequestMapping(value="/templates", method=RequestMethod.GET)
	public String templates() {
		return "templates";
	}
	
	private Long getLoggedUserId() {
		return (Long) SecurityContextHolder.getContext().getAuthentication().getDetails();
	}
	
	public PurchaseTemplateService getPurchaseTemplateService() {
		return purchaseTemplateService;
	}

	public void setPurchaseTemplateService(
			PurchaseTemplateService purchaseTemplateService) {
		this.purchaseTemplateService = purchaseTemplateService;
	}
	
}
