package com.cvut.shoppingshare.web.form;

import com.cvut.shoppingshare.domain.MembershipRequestType;

public class MembershipRequestDto {

	private MembershipRequestType membershipRequestType;
	
	private Long requestId;
	
	private Long userId;
	
	private String login;
	
	private String firstname;
	
	private String lastname;

	public MembershipRequestDto(MembershipRequestType membershipRequestType,
			Long requestId, Long userId, String login, String firstname,
			String lastname) {
		this.membershipRequestType = membershipRequestType;
		this.requestId = requestId;
		this.userId = userId;
		this.login = login;
		this.firstname = firstname;
		this.lastname = lastname;
	}

	public MembershipRequestType getMembershipRequestType() {
		return membershipRequestType;
	}

	public void setMembershipRequestType(MembershipRequestType membershipRequestType) {
		this.membershipRequestType = membershipRequestType;
	}

	public Long getRequestId() {
		return requestId;
	}

	public void setRequestId(Long requestId) {
		this.requestId = requestId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
}
