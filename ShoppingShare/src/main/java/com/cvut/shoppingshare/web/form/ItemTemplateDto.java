package com.cvut.shoppingshare.web.form;

public class ItemTemplateDto {

	private Long itemId;
	
	private String name;

	public ItemTemplateDto(Long itemId, String name) {
		this.itemId = itemId;
		this.name = name;
	}

	public Long getItemId() {
		return itemId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
