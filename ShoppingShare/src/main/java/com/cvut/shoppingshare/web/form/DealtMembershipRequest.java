package com.cvut.shoppingshare.web.form;

import com.cvut.shoppingshare.domain.MembershipRequestState;

public class DealtMembershipRequest {

	private Long requestId;
	
	private MembershipRequestState membershipRequestState;

	public DealtMembershipRequest(Long requestId,
			MembershipRequestState membershipRequestState) {
		this.requestId = requestId;
		this.membershipRequestState = membershipRequestState;
	}

	public Long getRequestId() {
		return requestId;
	}

	public void setRequestId(Long requestId) {
		this.requestId = requestId;
	}

	public MembershipRequestState getMembershipRequestState() {
		return membershipRequestState;
	}

	public void setMembershipRequestState(
			MembershipRequestState membershipRequestState) {
		this.membershipRequestState = membershipRequestState;
	}
	
}
