package com.cvut.shoppingshare.web.form;

import java.util.Date;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

public class NewsRequestMessage {
	
	private Date lastSync;
	
	private Long entityId;

	@JsonCreator
	public NewsRequestMessage(@JsonProperty("lastSync") Date lastSync, @JsonProperty("entityId") Long entityId) {
		this.lastSync = lastSync;
		this.entityId = entityId;
	}

	public Date getLastSync() {
		return lastSync;
	}

	public void setLastSync(Date lastSync) {
		this.lastSync = lastSync;
	}

	public Long getEntityId() {
		return entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

}
