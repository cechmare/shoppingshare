package com.cvut.shoppingshare.web.form;

import com.cvut.shoppingshare.domain.EntityState;

public class PurchaseDto extends PurchaseWebDto {
	
	private EntityState state;

	public PurchaseDto(Long purchaseId, String name, EntityState state) {
		super(purchaseId, name);
		this.state = state;
	}

	public EntityState getState() {
		return state;
	}

	public void setState(EntityState state) {
		this.state = state;
	}
	
}
