package com.cvut.shoppingshare.web.form;

public class ConfirmMessage {

	private boolean ok;
	
	public ConfirmMessage() {
		
	}

    public ConfirmMessage(boolean ok) {
		this.ok = ok;
	}

	public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }
	
}
