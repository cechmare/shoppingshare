package com.cvut.shoppingshare.web.form;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

public class DoubleEntityForm extends EntityForm {

	private Long entityId2;

	@JsonCreator
	public DoubleEntityForm(@JsonProperty("name") String name, @JsonProperty("entityId") Long entityId, 
			@JsonProperty("entityId2") Long entityId2) {
		super(name, entityId);
		this.entityId2 = entityId2;
	}

	public Long getEntityId2() {
		return entityId2;
	}

	public void setEntityId2(Long entityId2) {
		this.entityId2 = entityId2;
	}
	
}
