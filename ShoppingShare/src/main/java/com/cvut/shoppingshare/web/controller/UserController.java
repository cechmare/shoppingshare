package com.cvut.shoppingshare.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cvut.shoppingshare.security.ShoppingShareAuthenticationProvider;
import com.cvut.shoppingshare.service.UserService;
import com.cvut.shoppingshare.web.form.AutocompleteForm;
import com.cvut.shoppingshare.web.form.ConfirmMessage;
import com.cvut.shoppingshare.web.form.GroupMembersMessage;
import com.cvut.shoppingshare.web.form.NewsRequestMessage;
import com.cvut.shoppingshare.web.form.ShareUserWebDto;
import com.cvut.shoppingshare.web.form.SignupForm;
import com.cvut.shoppingshare.web.form.SignupWebForm;
import com.cvut.shoppingshare.web.form.UserAutocompleteForm;

@Controller
public class UserController {

	@Autowired
	private UserService userService;
	
	@Autowired
	private ShoppingShareAuthenticationProvider authenticationProvider;

	@RequestMapping(value="/signup", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", 
			produces = "application/json;charset=UTF-8")
	public @ResponseBody ConfirmMessage signUp(@RequestBody SignupForm form) {
		Long id;
		try {
			id = userService.singUp(form);
		} catch (Exception e) {
			id = null;
		}
		
		if (id == null) {
			return new ConfirmMessage(false);
		} else {
			return new ConfirmMessage(true);
		}
	}
	
	@RequestMapping(value="/signup", method = RequestMethod.POST)
	public String signup(@ModelAttribute("SpringWeb") SignupWebForm form) {
		Long userId;
		try {
			userId = userService.singUp(form);
		} catch (Exception e) {
			userId = null;
		}
		if (userId != null) {
			authenticationProvider.logIn(userId, form);
			return "redirect:/";
		}
		return "login";
	}
	
	@RequestMapping(value="/login", method = RequestMethod.GET)
	public ModelAndView loginWeb() {
		return new ModelAndView("login", "signup", new SignupWebForm());
	}
	
	@RequestMapping(value="/login/basic", method=RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public @ResponseBody ConfirmMessage login() {
		return new ConfirmMessage(true);
	}
	
	@RequestMapping(value="/logout", method=RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public @ResponseBody ConfirmMessage logout(HttpServletRequest request) {
		try {
	        HttpSession session = request.getSession(false);
	        if (session != null) {
	            session.invalidate();
	        }

	        SecurityContextHolder.clearContext();
	    } catch (Exception e) {
	    	return new ConfirmMessage(false);
	    }
		return new ConfirmMessage(true);
	}
	
	@RequestMapping(value="/members", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", 
			produces = "application/json;charset=UTF-8")
	public @ResponseBody GroupMembersMessage getGroupMembers(@RequestBody NewsRequestMessage request) {
		return userService.getGroupMembers(request);
	}
	
	@RequestMapping(value = "/user/startsWith", method = RequestMethod.POST)
	public @ResponseBody UserAutocompleteForm[] getTags(@RequestBody AutocompleteForm form) {
		return userService.findByLoginFirstnameLastnameStartsWith(form.getTag());
	}
	
	@RequestMapping(value="/members/web", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", 
			produces = "application/json;charset=UTF-8")
	public @ResponseBody ShareUserWebDto[] getAllOfGroupWithId(@RequestBody Long groupId) {
		return userService.getAllOfGroupWithId(groupId);
	}
	
	@RequestMapping(value="/members", method = RequestMethod.GET)
	public String members() {
		return "members";
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	
	public void setAuthenticationProvider(
			ShoppingShareAuthenticationProvider authenticationProvider) {
		this.authenticationProvider = authenticationProvider;
	}
	
}
