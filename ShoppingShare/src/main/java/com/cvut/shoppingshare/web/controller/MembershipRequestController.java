package com.cvut.shoppingshare.web.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cvut.shoppingshare.domain.MembershipRequestType;
import com.cvut.shoppingshare.service.MembershipRequestService;
import com.cvut.shoppingshare.web.form.ConfirmMessage;
import com.cvut.shoppingshare.web.form.InvitationForm;
import com.cvut.shoppingshare.web.form.MembershipOfferDto;
import com.cvut.shoppingshare.web.form.ShareUserDto;

@Controller
public class MembershipRequestController {
	
	@Autowired
	private MembershipRequestService membershipRequestService;

	@RequestMapping(value="/membershiprequest/accept", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", 
			produces = "application/json;charset=UTF-8")
	public @ResponseBody ConfirmMessage accept(@RequestBody Long requestId) {
		boolean ok = membershipRequestService.accept(requestId);
		return new ConfirmMessage(ok);
	}
	
	@RequestMapping(value="/membershiprequest/reject", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", 
			produces = "application/json;charset=UTF-8")
	public @ResponseBody ConfirmMessage reject(@RequestBody Long requestId) {
		boolean ok = membershipRequestService.reject(requestId);
		return new ConfirmMessage(ok);
	}

	@RequestMapping(value="/membershipoffer/all", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", 
			produces = "application/json;charset=UTF-8")
	public @ResponseBody MembershipOfferDto[] getOffersOfUserSinceDate(@RequestBody Date lastSync) {
		Long userId = getLoggedUserId();
		return membershipRequestService.getOffersOfUserSinceDate(userId, lastSync);
	}
	
	@RequestMapping(value="/membershiprequest", method = RequestMethod.PUT, consumes = "application/json;charset=UTF-8", 
			produces = "application/json;charset=UTF-8")
	public @ResponseBody ConfirmMessage request(@RequestBody Long groupId) {
		Long userId = getLoggedUserId();
		boolean ok = membershipRequestService.create(userId, groupId, MembershipRequestType.REQUEST);
		return new ConfirmMessage(ok);
	}
	
	@RequestMapping(value="/membershipoffer", method = RequestMethod.PUT, consumes = "application/json;charset=UTF-8", 
			produces = "application/json;charset=UTF-8")
	public @ResponseBody ConfirmMessage invite(@RequestBody InvitationForm form) {
		boolean ok = membershipRequestService.create(form.getUserId(), form.getGroupId(), MembershipRequestType.OFFER);
		return new ConfirmMessage(ok);
	}
	
	@RequestMapping(value="/membershipoffer", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public @ResponseBody MembershipOfferDto[] getOffersOfUser() {
		Long userId = getLoggedUserId();
		return membershipRequestService.getOffersOfUser(userId);
	}
	
	@RequestMapping(value="/membershiprequest/all", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", 
			produces = "application/json;charset=UTF-8")
	public @ResponseBody ShareUserDto[] getAllRequestsOfGroupWithId(@RequestBody Long groupId) {
		return membershipRequestService.getAllRequestsOfGroupWithId(groupId);
	}
	
	@RequestMapping(value="/membershipoffer/web/all", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", 
			produces = "application/json;charset=UTF-8")
	public @ResponseBody ShareUserDto[] getAllOffersOfGroupWithId(@RequestBody Long groupId) {
		return membershipRequestService.getAllOffersOfGroupWithId(groupId);
	}
	
	@RequestMapping(value="/offers", method = RequestMethod.GET)
	public String offersOfUser() {
		return "offers";
	}
	
	private Long getLoggedUserId() {
		return (Long) SecurityContextHolder.getContext(). getAuthentication().getDetails();
	}
	
	public void setMembershipRequestService(
			MembershipRequestService membershipRequestService) {
		this.membershipRequestService = membershipRequestService;
	}
	
}
