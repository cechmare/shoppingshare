package com.cvut.shoppingshare.web.form;

public class ShareUserDto extends ShareUserWebDto {

	private Long userId;
	
	public ShareUserDto(Long userId, String login, String firstname,
			String lastname) {
		super(login, firstname, lastname);
		this.userId = userId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
}
