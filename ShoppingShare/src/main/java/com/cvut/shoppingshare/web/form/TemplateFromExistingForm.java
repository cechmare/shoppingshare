package com.cvut.shoppingshare.web.form;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

public class TemplateFromExistingForm {
	
	private Long entityId;
	
	private PurchaseTemplateSourceEnum templateSource;
	
	private String name;

	@JsonCreator
	public TemplateFromExistingForm(@JsonProperty("entityId") Long entityId,
			@JsonProperty("templateSource") PurchaseTemplateSourceEnum templateSource,
			@JsonProperty("name") String name) {
		this.entityId = entityId;
		this.templateSource = templateSource;
		this.name = name;
	}

	public Long getEntityId() {
		return entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	public PurchaseTemplateSourceEnum getTemplateSource() {
		return templateSource;
	}

	public void setTemplateSource(PurchaseTemplateSourceEnum templateSource) {
		this.templateSource = templateSource;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
