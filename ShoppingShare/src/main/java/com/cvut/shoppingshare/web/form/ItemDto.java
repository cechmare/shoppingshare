package com.cvut.shoppingshare.web.form;

import com.cvut.shoppingshare.domain.ItemState;

public class ItemDto extends ItemWebDto {

	private Long purchaseId;

	public ItemDto(Long purchaseId, Long itemId, String name, ItemState state) {
		super(itemId, name, state);
		this.purchaseId = purchaseId;
	}

	public Long getPurchaseId() {
		return purchaseId;
	}

	public void setPurchaseId(Long purchaseId) {
		this.purchaseId = purchaseId;
	}
	
}
