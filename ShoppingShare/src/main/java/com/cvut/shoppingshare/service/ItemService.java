package com.cvut.shoppingshare.service;

import java.util.Date;
import java.util.List;

import com.cvut.shoppingshare.domain.Item;
import com.cvut.shoppingshare.web.form.EditEntityForm;
import com.cvut.shoppingshare.web.form.EntityForm;
import com.cvut.shoppingshare.web.form.IdsForm;
import com.cvut.shoppingshare.web.form.ItemDto;
import com.cvut.shoppingshare.web.form.ItemTemplateDto;
import com.cvut.shoppingshare.web.form.ItemWebDto;
import com.cvut.shoppingshare.web.form.NewsRequestMessage;

public interface ItemService {
	
	public boolean markAsRemoved(Long itemId);
	
	public boolean markAsRemoved(IdsForm form);
	
	public boolean markAsBought(Long itemId);
	
	public boolean markAsBought(IdsForm form);
	
	public Long addItem(EntityForm itemForm);
	
	public Long addItemTemplate(EntityForm itemForm);
	
	public boolean editItem(EditEntityForm form);
	
	public boolean editItem(EntityForm form);
	
	public Item findById(Long itemId);
	
	public List<ItemDto> getItemsNews(NewsRequestMessage request);
	
	public List<ItemDto> getItemTemplatesNews(Date lastSync, Long userId);
	
	public List<String> getNonDeletedByPurchaseId(Long purchaseId);
	
	public List<String> getNonDeletedByPurchaseTemplateId(Long templateId);
	
	public ItemTemplateDto[] getAllTemplates(Long templateId);
	
	public ItemWebDto[] getAll(Long purchaseId);

}
