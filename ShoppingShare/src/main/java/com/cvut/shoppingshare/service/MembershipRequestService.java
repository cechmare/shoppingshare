package com.cvut.shoppingshare.service;

import java.util.Date;
import java.util.List;

import com.cvut.shoppingshare.domain.MembershipRequest;
import com.cvut.shoppingshare.domain.MembershipRequestType;
import com.cvut.shoppingshare.web.form.DealtMembershipRequest;
import com.cvut.shoppingshare.web.form.MembershipOfferDto;
import com.cvut.shoppingshare.web.form.MembershipRequestDto;
import com.cvut.shoppingshare.web.form.ShareUserDto;

public interface MembershipRequestService {
	
	public Long persist(MembershipRequest membershipRequest);
	
	public List<MembershipRequestDto> getNonDealtRequestsOfGroupSinceDate(Long groupId, Date lastSync);
	
	public List<DealtMembershipRequest> getDealtRequestsOfGroupSinceDate(Long groupId, Date lastSync);
	
	public boolean accept(Long requestId);
	
	public boolean reject(Long requestId);
	
	public MembershipOfferDto[] getOffersOfUserSinceDate(Long userId, Date lastSync);
	
	public MembershipOfferDto[] getOffersOfUser(Long userId);
	
	public boolean create(Long userId, Long groupId, MembershipRequestType type);
	
	public ShareUserDto[] getAllRequestsOfGroupWithId(Long groupId);
	
	public ShareUserDto[] getAllOffersOfGroupWithId(Long groupId);
	
}
