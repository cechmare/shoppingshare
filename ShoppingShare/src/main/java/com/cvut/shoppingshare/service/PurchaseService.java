package com.cvut.shoppingshare.service;

import com.cvut.shoppingshare.domain.Purchase;
import com.cvut.shoppingshare.web.form.DoubleEntityForm;
import com.cvut.shoppingshare.web.form.EditEntityForm;
import com.cvut.shoppingshare.web.form.EntityForm;
import com.cvut.shoppingshare.web.form.NewsRequestMessage;
import com.cvut.shoppingshare.web.form.PurchaseWebDto;
import com.cvut.shoppingshare.web.form.PurchasesItemsDto;

public interface PurchaseService {
	
	public PurchaseWebDto[] getAllPurchasesOfGroup(Long groupId);
	
	public PurchasesItemsDto getPurchasesItemsNews(NewsRequestMessage request);
	
	public Purchase findById(Long purchaseId);
	
	public void update(Purchase purchase);
	
	public Long add(EntityForm purchaseForm);
	
	public Long addPurchaseFromTemplate(DoubleEntityForm form);
	
	public boolean markAsRemoved(Long purchaseId);
	
	public boolean edit(EditEntityForm form);
	
	public boolean edit(EntityForm form);

}
