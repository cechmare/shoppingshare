package com.cvut.shoppingshare.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cvut.shoppingshare.dao.PurchaseTemplateDao;
import com.cvut.shoppingshare.domain.EntityState;
import com.cvut.shoppingshare.domain.Item;
import com.cvut.shoppingshare.domain.PurchaseTemplate;
import com.cvut.shoppingshare.domain.ShareUser;
import com.cvut.shoppingshare.web.form.EditEntityForm;
import com.cvut.shoppingshare.web.form.EntityForm;
import com.cvut.shoppingshare.web.form.ItemDto;
import com.cvut.shoppingshare.web.form.PurchaseDto;
import com.cvut.shoppingshare.web.form.PurchaseWebDto;
import com.cvut.shoppingshare.web.form.PurchasesItemsDto;
import com.cvut.shoppingshare.web.form.TemplateForm;
import com.cvut.shoppingshare.web.form.TemplateFromExistingForm;

@Service
@Transactional
public class PurchaseTemplateServiceImpl implements PurchaseTemplateService {
	
	@Autowired
	private PurchaseTemplateDao purchaseTemplateDao;
	
	@Autowired
	private ItemService itemService;
	
	@Autowired
	private UserService userService;

	@Override
	public PurchasesItemsDto getNews(Date lastSync, Long userId) {
		if (lastSync == null || userId == null) {
			return null;
		}
		List<PurchaseDto> templates = purchaseTemplateDao.getTemplatesNews(lastSync, userId);
		List<ItemDto> items = itemService.getItemTemplatesNews(lastSync, userId);
		return new PurchasesItemsDto(templates, items);
	}

	@Override
	public boolean add(TemplateFromExistingForm form, Long userId) {
		if (userId == null || form == null || form.getEntityId() == null || form.getName() == null || form.getTemplateSource() == null) {
			return false;
		}
		switch (form.getTemplateSource()) {
			case PURCHASE:
				return addFromPurchase(form, userId);
			case TEMPLATE:
				return addFromTemplate(form, userId);
			default:
				return false;
		}
	}
	
	@Override
	public boolean markAsRemoved(Long templateId) {
		if (templateId == null) {
			return false;
		}
		PurchaseTemplate template = purchaseTemplateDao.findById(templateId);
		if (template == null) {
			return false;
		} else {
			template.setState(EntityState.DELETED);
			template.setUpdated(new Date());
			template.removeAllItems();
			purchaseTemplateDao.update(template);
			return true;
		}
	}

	private boolean addFromPurchase(TemplateFromExistingForm form, Long userId) {
		ShareUser user = userService.findById(userId);
		if (user == null) {
			return false;
		}
		PurchaseTemplate template = new PurchaseTemplate(user, form.getName());
		List<String> itemNames = itemService.getNonDeletedByPurchaseId(form.getEntityId());
		for (String s: itemNames) {
			Item item = new Item(template, s);
			template.addItem(item);
		}
		user.addTemplate(template);
		return true;
	}
	
	private boolean addFromTemplate(TemplateFromExistingForm form, Long userId) {
		ShareUser user = userService.findById(userId);
		if (user == null) {
			return false;
		}
		PurchaseTemplate template = new PurchaseTemplate(user, form.getName());
		List<String> itemNames = itemService.getNonDeletedByPurchaseTemplateId(form.getEntityId());
		for (String s: itemNames) {
			Item item = new Item(template, s);
			template.addItem(item);
		}
		user.addTemplate(template);
		return true;
	}

	@Override
	public boolean add(TemplateForm form, Long userId) {
		if (form == null || form.getName() == null || form.getItems() == null || userId == null) {
			return false;
		}
		ShareUser user = userService.findById(userId);
		if (user == null) {
			return false;
		}
		PurchaseTemplate template = new PurchaseTemplate(user, form.getName());
		for (String s: form.getItems()) {
			Item item = new Item(template, s);
			template.addItem(item);
		}
		user.addTemplate(template);
		return true;
	}

	@Override
	public Long add(String name, Long userId) {
		if (name == null || userId == null) {
			return null;
		}
		ShareUser user = userService.findById(userId);
		if (user == null) {
			return null;
		}
		PurchaseTemplate template = new PurchaseTemplate(user, name);
		user.addTemplate(template);
		purchaseTemplateDao.persist(template);
		return template.getEntityId();
	}

	@Override
	public PurchaseTemplate findById(Long templateId) {
		return purchaseTemplateDao.findById(templateId);
	}

	@Override
	public void update(PurchaseTemplate template) {
		purchaseTemplateDao.update(template);
	}

	@Override
	public boolean edit(EditEntityForm form) {
		if (form == null || form.getEntityId() == null || form.getLastSync() == null || form.getName() == null) {
			return false;
		}
		PurchaseTemplate template = purchaseTemplateDao.findById(form.getEntityId());
		if (template == null || template.getUpdated().after(form.getLastSync())) {
			return false;
		}
		template.setName(form.getName());
		template.setUpdated(new Date());
		purchaseTemplateDao.update(template);
		return true;
	}

	@Override
	public boolean edit(EntityForm form) {
		if (form == null || form.getEntityId() == null || form.getName() == null) {
			return false;
		}
		PurchaseTemplate template = purchaseTemplateDao.findById(form.getEntityId());
		if (template == null) {
			return false;
		}
		template.setName(form.getName());
		purchaseTemplateDao.update(template);
		return true;
	}

	@Override
	public PurchaseWebDto[] getAll(Long userId) {
		List<PurchaseWebDto> templates = purchaseTemplateDao.getAll(userId);
		return templates.toArray(new PurchaseWebDto[templates.size()]);
	}

	public void setPurchaseTemplateDao(PurchaseTemplateDao purchaseTemplateDao) {
		this.purchaseTemplateDao = purchaseTemplateDao;
	}

	public void setItemService(ItemService itemService) {
		this.itemService = itemService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

}
