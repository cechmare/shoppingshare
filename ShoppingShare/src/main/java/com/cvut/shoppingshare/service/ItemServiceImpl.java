package com.cvut.shoppingshare.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cvut.shoppingshare.dao.ItemDao;
import com.cvut.shoppingshare.domain.Item;
import com.cvut.shoppingshare.domain.ItemState;
import com.cvut.shoppingshare.domain.Purchase;
import com.cvut.shoppingshare.domain.PurchaseTemplate;
import com.cvut.shoppingshare.web.form.EditEntityForm;
import com.cvut.shoppingshare.web.form.EntityForm;
import com.cvut.shoppingshare.web.form.IdsForm;
import com.cvut.shoppingshare.web.form.ItemDto;
import com.cvut.shoppingshare.web.form.ItemTemplateDto;
import com.cvut.shoppingshare.web.form.ItemWebDto;
import com.cvut.shoppingshare.web.form.NewsRequestMessage;

@Service
@Transactional
public class ItemServiceImpl implements ItemService {
	
	@Autowired
	private ItemDao itemDao;
	
	@Autowired
	private PurchaseService purchaseService;
	
	@Autowired
	private PurchaseTemplateService purchaseTemplateService;

	@Override
	public boolean markAsRemoved(Long itemId) {
		if (itemId == null) {
			return false;
		}
		Item item = findById(itemId);
		if (item == null) {
			return false;
		} else {
			item.setState(ItemState.DELETED);
			item.setUpdated(new Date());
			itemDao.update(item);
			return true;
		}
	}

	@Override
	public boolean markAsRemoved(IdsForm form) {
		if (form == null || form.getIds() == null) {
			return false;
		}
		for (Long itemId: form.getIds()) {
			markAsRemoved(itemId);
		}
		return true;
	}

	@Override
	public boolean markAsBought(Long itemId) {
		if (itemId == null) {
			return false;
		}
		Item item = findById(itemId);
		if (item == null || item.getState() != ItemState.NEW) {
			return false;
		} else {
			item.setState(ItemState.BOUGHT);
			item.setUpdated(new Date());
			itemDao.update(item);
			return true;
		}
	}

	@Override
	public boolean markAsBought(IdsForm form) {
		if (form == null || form.getIds() == null) {
			return false;
		}
		for (Long itemId: form.getIds()) {
			markAsBought(itemId);
		}
		return true;
	}

	@Override
	public Long addItem(EntityForm itemForm) {
		Purchase purchase = purchaseService.findById(itemForm.getEntityId());
		if (purchase == null) {
			return null;
		}
		Item item = new Item(purchase, itemForm.getName());
		itemDao.persist(item);
		
		purchase.addItem(item);
		purchaseService.update(purchase);
		
		return item.getEntityId();
	}

	@Override
	public Long addItemTemplate(EntityForm itemForm) {
		PurchaseTemplate template = purchaseTemplateService.findById(itemForm.getEntityId());
		if (template == null) {
			return null;
		}
		Item item = new Item(template, itemForm.getName());
		itemDao.persist(item);
		
		template.addItem(item);
		purchaseTemplateService.update(template);
		
		return item.getEntityId();
	}

	@Override
	public boolean editItem(EditEntityForm form) {
		if (form == null || form.getEntityId() == null || form.getLastSync() == null || form.getName() == null) {
			return false;
		}
		Item item = itemDao.findById(form.getEntityId());
		if (item == null || item.getUpdated().after(form.getLastSync())) {
			return false;
		}
		item.setName(form.getName());
		item.setUpdated(new Date());
		itemDao.update(item);
		return true;
	}

	@Override
	public boolean editItem(EntityForm form) {
		if (form == null || form.getEntityId() == null || form.getName() == null) {
			return false;
		}
		Item item = itemDao.findById(form.getEntityId());
		if (item == null) {
			return false;
		}
		item.setName(form.getName());
		item.setUpdated(new Date());
		itemDao.update(item);
		return true;
	}

	@Override
	public Item findById(Long itemId) {
		return itemDao.findById(itemId);
	}

	@Override
	public List<ItemDto> getItemsNews(NewsRequestMessage request) {
		if (request == null || request.getEntityId() == null || request.getLastSync() == null) {
			return null;
		}
		return itemDao.getItemsNews(request); 
	}

	@Override
	public List<ItemDto> getItemTemplatesNews(Date lastSync, Long userId) {
		if (lastSync == null || userId == null) {
			return null;
		}
		return itemDao.getItemTemplatesNews(lastSync, userId);
	}

	@Override
	public List<String> getNonDeletedByPurchaseId(Long purchaseId) {
		if (purchaseId == null) {
			return null;
		} else {
			return itemDao.getNonDeletedByPurchaseId(purchaseId);
		}
	}

	@Override
	public List<String> getNonDeletedByPurchaseTemplateId(Long templateId) {
		if (templateId == null) {
			return null;
		} else {
			return itemDao.getNonDeletedByPurchaseTemplateId(templateId);
		}
	}

	@Override
	public ItemTemplateDto[] getAllTemplates(Long templateId) {
		if (templateId == null) {
			return null;
		}
		List<ItemTemplateDto> items = itemDao.getAllTemplates(templateId);
		return items.toArray(new ItemTemplateDto[items.size()]);
	}

	@Override
	public ItemWebDto[] getAll(Long purchaseId) {
		if (purchaseId == null) {
			return null;
		}
		List<ItemWebDto> items = itemDao.getAll(purchaseId);
		return items.toArray(new ItemWebDto[items.size()]);
	}

	public void setItemDao(ItemDao itemDao) {
		this.itemDao = itemDao;
	}

	public void setPurchaseService(PurchaseService purchaseService) {
		this.purchaseService = purchaseService;
	}

	public void setPurchaseTemplateService(
			PurchaseTemplateService purchaseTemplateService) {
		this.purchaseTemplateService = purchaseTemplateService;
	}
	
}
