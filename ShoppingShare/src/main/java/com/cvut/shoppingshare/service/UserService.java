package com.cvut.shoppingshare.service;

import java.util.List;

import com.cvut.shoppingshare.domain.ShareUser;
import com.cvut.shoppingshare.web.form.GroupMembersMessage;
import com.cvut.shoppingshare.web.form.NewsRequestMessage;
import com.cvut.shoppingshare.web.form.ShareUserWebDto;
import com.cvut.shoppingshare.web.form.SignupForm;
import com.cvut.shoppingshare.web.form.UserAutocompleteForm;

public interface UserService {
	
	public ShareUser findById(Long userId);
	
	public List<ShareUser> findByIds(List<Long> userIds);
	
	public UserAutocompleteForm[] findByLoginFirstnameLastnameStartsWith(String tagName);
	
	public Long singUp(SignupForm form);
	
	public Long singUp(com.cvut.shoppingshare.web.form.SignupWebForm form);
	
	public void update(ShareUser user);
	
	public GroupMembersMessage getGroupMembers(NewsRequestMessage request);
	
	public ShareUserWebDto[] getAllOfGroupWithId(Long groupId);

}
