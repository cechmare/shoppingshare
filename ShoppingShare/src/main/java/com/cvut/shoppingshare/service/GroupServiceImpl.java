package com.cvut.shoppingshare.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cvut.shoppingshare.dao.GroupDao;
import com.cvut.shoppingshare.domain.MembershipRequest;
import com.cvut.shoppingshare.domain.MembershipRequestState;
import com.cvut.shoppingshare.domain.MembershipRequestType;
import com.cvut.shoppingshare.domain.ShareGroup;
import com.cvut.shoppingshare.domain.ShareUser;
import com.cvut.shoppingshare.web.form.GroupForm;
import com.cvut.shoppingshare.web.form.GroupListMessage;

@Service
@Transactional
public class GroupServiceImpl implements GroupService {
	
	@Autowired
	private GroupDao groupDao;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private MembershipRequestService membershipRequestService;
	
	@Override
	public Long persist(ShareGroup shareGroup) {
		return groupDao.persist(shareGroup);
	}
	
	@Override
	public void update(ShareGroup shareGroup) {
		groupDao.update(shareGroup);
	}
	
	@Override
	public GroupListMessage[] getAllGroupsWhereParticipate(Long userId) {
		return groupDao.getAllGroupsWhereParticipate(userId);
	}
	
	@Override
	public GroupListMessage[] getAllGroupsWhereParticipateSinceDate(
			Long userId, Date lastSync) {
		return groupDao.getAllGroupsWhereParticipateSinceDate(userId, lastSync);
	}

	@Override
	public Long add(Long userId, GroupForm form) {
		ShareUser founder = userService.findById(userId);
		if (founder == null || form == null) {
			return null;
		}
		ShareGroup group = new ShareGroup(form.getName(), founder);
		groupDao.persist(group);
		
		founder.addGroup(group);
		userService.update(founder);
		
		List<Long> userIdsToInvite = form.getUserIdsToInvite();
		if (userIdsToInvite != null && userIdsToInvite.size() > 0) {
			List<ShareUser> usersToInvite = userService.findByIds(userIdsToInvite);
			if (usersToInvite != null) {
				for (ShareUser userToInvite: usersToInvite) {
					if (userToInvite == null) {
						continue;
					}
					MembershipRequest request = new MembershipRequest(group, userToInvite, MembershipRequestType.OFFER, MembershipRequestState.NEW);
					membershipRequestService.persist(request);
					userToInvite.addMembershipRequest(request);
					userService.update(userToInvite);
				}
			}
		}
		return group.getEntityId();
	}

	@Override
	public GroupListMessage[] findByNameStartsWith(String tagName) {
		if (tagName == null) {
			return null;
		}
		return groupDao.findByNameStartsWith(tagName);
	}

	@Override
	public ShareGroup findById(Long groupId) {
		if (groupId == null) {
			return null;
		}
		return groupDao.findById(groupId);
	}

	public void setGroupDao(GroupDao groupDao) {
		this.groupDao = groupDao;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public void setMembershipRequestService(
			MembershipRequestService membershipRequestService) {
		this.membershipRequestService = membershipRequestService;
	}
	
}
