package com.cvut.shoppingshare.service;

import java.util.Date;

import com.cvut.shoppingshare.domain.ShareGroup;
import com.cvut.shoppingshare.web.form.GroupForm;
import com.cvut.shoppingshare.web.form.GroupListMessage;

public interface GroupService {
	
	public Long persist(ShareGroup shareGroup);
	
	public void update(ShareGroup shareGroup);
	
	public GroupListMessage[] getAllGroupsWhereParticipate(Long userId);
	
	public GroupListMessage[] getAllGroupsWhereParticipateSinceDate(Long userId, Date lastSync);
	
	public Long add(Long userId, GroupForm form);
	
	public GroupListMessage[] findByNameStartsWith(String tagName);
	
	public ShareGroup findById(Long groupId);

}
