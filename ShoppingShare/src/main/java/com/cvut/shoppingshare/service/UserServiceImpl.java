package com.cvut.shoppingshare.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cvut.shoppingshare.dao.UserDao;
import com.cvut.shoppingshare.domain.Item;
import com.cvut.shoppingshare.domain.Purchase;
import com.cvut.shoppingshare.domain.ShareGroup;
import com.cvut.shoppingshare.domain.ShareUser;
import com.cvut.shoppingshare.web.form.DealtMembershipRequest;
import com.cvut.shoppingshare.web.form.GroupMembersMessage;
import com.cvut.shoppingshare.web.form.MembershipRequestDto;
import com.cvut.shoppingshare.web.form.NewsRequestMessage;
import com.cvut.shoppingshare.web.form.ShareUserDto;
import com.cvut.shoppingshare.web.form.ShareUserWebDto;
import com.cvut.shoppingshare.web.form.SignupForm;
import com.cvut.shoppingshare.web.form.SignupWebForm;
import com.cvut.shoppingshare.web.form.UserAutocompleteForm;

@Service
@Transactional
public class UserServiceImpl implements UserService {
	
	private static final String IDS = "ids";
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private GroupService groupService;
	
	@Autowired
	private MembershipRequestService membershipRequestService;

	@Override
	public ShareUser findById(Long userId) {
		if (userId == null) {
			return null;
		} else {
			return userDao.findById(userId);
		}
	}

	@Override
	public List<ShareUser> findByIds(List<Long> userIds) {
		if (userIds == null || userIds.size() == 0) {
			return null;
		}
		Map<String, Object> values = new HashMap<String, Object>();
		values.put(IDS, userIds);
		return userDao.find(ShareUser.FIND_BY_IDS, values);
	}

	@Override
	public UserAutocompleteForm[] findByLoginFirstnameLastnameStartsWith(
			String tagName) {
		if (tagName == null) {
			return null;
		}
		List<UserAutocompleteForm> users = userDao.findByLoginFirstnameLastnameStartsWith(tagName);
		return users.toArray(new UserAutocompleteForm[users.size()]);
	}

	@Override
	public Long singUp(SignupForm form) {
		ShareUser user = new ShareUser();
		user.setLogin(form.getEmail());
		user.setPassword(form.getPassword());
		try {
			Long id = userDao.persist(user);
			afterSignUpTasks(user, id);
			return id;
		} catch (Exception e) {
			//TODO logging
			return null;
		}
	}
	
	@Override
	public Long singUp(SignupWebForm form) {
		ShareUser user = new ShareUser();
		user.setLogin(form.getEmail());
		user.setPassword(form.getPassword());
		try {
			Long id = userDao.persist(user);
			afterSignUpTasks(user, id);
			return id;
		} catch (Exception e) {
			//TODO logging
			return null;
		}
	}

	private void afterSignUpTasks(ShareUser user, Long userId) {
		ShareGroup personalGroup = new ShareGroup();
		personalGroup.addUser(user);
		personalGroup.setName("Personal");
		personalGroup.setCreated(new Date());
		
		Purchase p1 = new Purchase(personalGroup, "Bakery");
		Purchase p2 = new Purchase(personalGroup, "Weekend");
		personalGroup.addPurchase(p1);
		personalGroup.addPurchase(p2);
		
		Item i1 = new Item(p1, "bread");
		Item i2 = new Item(p1, "donuts");
		Item i3 = new Item(p1, "cookies");
		p1.addItem(i1);
		p1.addItem(i2);
		p1.addItem(i3);
		
		Item i4 = new Item(p2, "beer");
		Item i5 = new Item(p2, "steaks");
		p2.addItem(i4);
		p2.addItem(i5);
		
		groupService.persist(personalGroup);
		
		user.addGroup(personalGroup);
		userDao.update(user);
	}

	@Override
	public void update(ShareUser user) {
		userDao.update(user);
	}

	@Override
	public GroupMembersMessage getGroupMembers(NewsRequestMessage request) {
		GroupMembersMessage response = new GroupMembersMessage();
		Long groupId = request.getEntityId();
		Date lastSync = request.getLastSync();
		
		if (lastSync == null || groupId == null) {
			return response;
		}
		
		if (lastSync.getTime() == 1) {
			List<ShareUserDto> users = userDao.getShareUsersOfGroup(groupId);
			response.setUsers(users);
		}
		List<MembershipRequestDto> requests = membershipRequestService.getNonDealtRequestsOfGroupSinceDate(groupId, lastSync);
		response.setRequests(requests);
		
		List<DealtMembershipRequest> dealtRequests = membershipRequestService.getDealtRequestsOfGroupSinceDate(groupId, lastSync);
		response.setDealtRequests(dealtRequests);
		
		return response;
	}

	@Override
	public ShareUserWebDto[] getAllOfGroupWithId(Long groupId) {
		if (groupId == null) {
			return null;
		}
		List<ShareUserWebDto> users = userDao.getAllOfGroupWithId(groupId);
		return users.toArray(new ShareUserWebDto[users.size()]);
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public void setGroupService(GroupService groupService) {
		this.groupService = groupService;
	}

	public void setMembershipRequestService(
			MembershipRequestService membershipRequestService) {
		this.membershipRequestService = membershipRequestService;
	}
	
}
