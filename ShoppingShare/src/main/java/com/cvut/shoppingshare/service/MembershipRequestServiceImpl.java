package com.cvut.shoppingshare.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cvut.shoppingshare.dao.MembershipRequestDao;
import com.cvut.shoppingshare.domain.MembershipRequest;
import com.cvut.shoppingshare.domain.MembershipRequestState;
import com.cvut.shoppingshare.domain.MembershipRequestType;
import com.cvut.shoppingshare.domain.ShareGroup;
import com.cvut.shoppingshare.domain.ShareUser;
import com.cvut.shoppingshare.web.form.DealtMembershipRequest;
import com.cvut.shoppingshare.web.form.MembershipOfferDto;
import com.cvut.shoppingshare.web.form.MembershipRequestDto;
import com.cvut.shoppingshare.web.form.ShareUserDto;

@Service
@Transactional
public class MembershipRequestServiceImpl implements MembershipRequestService {
	
	@Autowired
	private MembershipRequestDao membershipRequestDao;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private GroupService groupService;

	@Override
	public Long persist(MembershipRequest membershipRequest) {
		return membershipRequestDao.persist(membershipRequest);
	}

	@Override
	public List<MembershipRequestDto> getNonDealtRequestsOfGroupSinceDate(
			Long groupId, Date lastSync) {
		if (groupId == null || lastSync == null) {
			return null;
		}
		return membershipRequestDao.getNonDealtRequestsOfGroupSinceDate(groupId, lastSync);
	}

	@Override
	public List<DealtMembershipRequest> getDealtRequestsOfGroupSinceDate(
			Long groupId, Date lastSync) {
		if (groupId == null || lastSync == null) {
			return null;
		}
		return membershipRequestDao.getDealtRequestsOfGroupSinceDate(groupId, lastSync);
	}

	@Override
	public boolean accept(Long requestId) {
		if (requestId == null) {
			return false;
		}
		MembershipRequest request = membershipRequestDao.findById(requestId);
		if (request.getMembershipRequestState() == MembershipRequestState.NEW) {
			request.setMembershipRequestState(MembershipRequestState.ACCEPTED);
			request.setDealt(new Date());
			
			ShareUser user = request.getShareuser();
			ShareGroup group = request.getSharegroup();
			user.addGroup(group);
			group.addUser(user);
			
			membershipRequestDao.update(request);
			userService.update(user);
			groupService.update(group);
		}
		return true;
	}

	@Override
	public boolean reject(Long requestId) {
		if (requestId == null) {
			return false;
		}
		MembershipRequest request = membershipRequestDao.findById(requestId);
		if (request.getMembershipRequestState() == MembershipRequestState.NEW) {
			request.setMembershipRequestState(MembershipRequestState.REJECTED);
			request.setDealt(new Date());
			membershipRequestDao.update(request);
		}
		return true;
	}

	@Override
	public MembershipOfferDto[] getOffersOfUserSinceDate(Long userId, Date lastSync) {
		if (lastSync == null) {
			return null;
		}
		List<MembershipOfferDto> offers = membershipRequestDao.getOffersOfUserSinceDate(userId, lastSync);
		return offers.toArray(new MembershipOfferDto[offers.size()]);
	}

	@Override
	public MembershipOfferDto[] getOffersOfUser(Long userId) {
		List<MembershipOfferDto> offers = membershipRequestDao.getOffersOfUser(userId);
		return offers.toArray(new MembershipOfferDto[offers.size()]);
	}

	@Override
	public boolean create(Long userId, Long groupId, MembershipRequestType type) {
		ShareUser user = userService.findById(userId);
		ShareGroup group = groupService.findById(groupId);
		if (user == null || group == null || type == null) {
			return false;
		}
		MembershipRequest request = new MembershipRequest(group, user, type, MembershipRequestState.NEW);
		membershipRequestDao.persist(request);
		user.addMembershipRequest(request);
		userService.update(user);
		return true;
	}

	@Override
	public ShareUserDto[] getAllRequestsOfGroupWithId(Long groupId) {
		if (groupId == null) {
			return null;
		}
		List<ShareUserDto> requests = membershipRequestDao.getAllRequestsOfGroupWithId(groupId);
		return requests.toArray(new ShareUserDto[requests.size()]);
	}

	@Override
	public ShareUserDto[] getAllOffersOfGroupWithId(Long groupId) {
		if (groupId == null) {
			return null;
		}
		List<ShareUserDto> offers = membershipRequestDao.getAllOffersOfGroupWithId(groupId);
		return offers.toArray(new ShareUserDto[offers.size()]);
	}

	public void setMembershipRequestDao(MembershipRequestDao membershipRequestDao) {
		this.membershipRequestDao = membershipRequestDao;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public void setGroupService(GroupService groupService) {
		this.groupService = groupService;
	}

}
