package com.cvut.shoppingshare.service;

import java.util.Date;

import com.cvut.shoppingshare.domain.PurchaseTemplate;
import com.cvut.shoppingshare.web.form.EditEntityForm;
import com.cvut.shoppingshare.web.form.EntityForm;
import com.cvut.shoppingshare.web.form.PurchaseWebDto;
import com.cvut.shoppingshare.web.form.PurchasesItemsDto;
import com.cvut.shoppingshare.web.form.TemplateForm;
import com.cvut.shoppingshare.web.form.TemplateFromExistingForm;

public interface PurchaseTemplateService {

	public PurchasesItemsDto getNews(Date lastSync, Long userId);
	
	public boolean add(TemplateFromExistingForm form, Long userId);
	
	public boolean add(TemplateForm form, Long userId);
	
	public Long add(String name, Long userId);
	
	public boolean markAsRemoved(Long templateId);
	
	public PurchaseTemplate findById(Long templateId);
	
	public void update(PurchaseTemplate template);
	
	public boolean edit(EditEntityForm form);
	
	public boolean edit(EntityForm form);
	
	public PurchaseWebDto[] getAll(Long userId);
	
}
