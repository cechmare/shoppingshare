package com.cvut.shoppingshare.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cvut.shoppingshare.dao.PurchaseDao;
import com.cvut.shoppingshare.domain.EntityState;
import com.cvut.shoppingshare.domain.Item;
import com.cvut.shoppingshare.domain.ItemState;
import com.cvut.shoppingshare.domain.Purchase;
import com.cvut.shoppingshare.domain.PurchaseTemplate;
import com.cvut.shoppingshare.domain.ShareGroup;
import com.cvut.shoppingshare.web.form.DoubleEntityForm;
import com.cvut.shoppingshare.web.form.EditEntityForm;
import com.cvut.shoppingshare.web.form.EntityForm;
import com.cvut.shoppingshare.web.form.ItemDto;
import com.cvut.shoppingshare.web.form.NewsRequestMessage;
import com.cvut.shoppingshare.web.form.PurchaseDto;
import com.cvut.shoppingshare.web.form.PurchaseWebDto;
import com.cvut.shoppingshare.web.form.PurchasesItemsDto;

@Service
@Transactional
public class PurchaseServiceImpl implements PurchaseService {
	
	@Autowired
	private PurchaseDao purchaseDao;
	
	@Autowired
	private ItemService itemService;
	
	@Autowired
	private GroupService groupService;
	
	@Autowired
	private PurchaseTemplateService purchaseTemplateService;
	
	@Override
	public PurchaseWebDto[] getAllPurchasesOfGroup(Long groupId) {
		if (groupId == null) {
			return null;
		}
		List<PurchaseWebDto> purchases = purchaseDao.getAllPurchasesOfGroup(groupId);
		return purchases.toArray(new PurchaseWebDto[purchases.size()]);
	}

	@Override
	public PurchasesItemsDto getPurchasesItemsNews(NewsRequestMessage request) {
		if (request == null || request.getEntityId() == null || request.getLastSync() == null) {
			return null;
		}
		List<PurchaseDto> purchases = getPurchasesNews(request);
		List<ItemDto> items = itemService.getItemsNews(request);
		return new PurchasesItemsDto(purchases, items);
	}

	private List<PurchaseDto> getPurchasesNews(NewsRequestMessage request) {
		return purchaseDao.getPurchasesNews(request);
	}

	@Override
	public Purchase findById(Long purchaseId) {
		return purchaseDao.findById(purchaseId);
	}

	@Override
	public void update(Purchase purchase) {
		purchaseDao.update(purchase);
	}

	@Override
	public Long add(EntityForm purchaseForm) {
		ShareGroup group = groupService.findById(purchaseForm.getEntityId());
		if (group == null) {
			return null;
		}
		Purchase purchase = new Purchase(group, purchaseForm.getName());
		purchaseDao.persist(purchase);
		
		group.addPurchase(purchase);
		groupService.update(group);
		
		return purchase.getEntityId();
	}
 
	@Override
	public Long addPurchaseFromTemplate(DoubleEntityForm form) {
		if (form == null || form.getEntityId() == null || form.getEntityId2() == null) {
			return null;
		}
		
		PurchaseTemplate template = purchaseTemplateService.findById(form.getEntityId());
		ShareGroup group = groupService.findById(form.getEntityId2());
		if (template == null || group == null) {
			return null;
		}
		
		Purchase purchase = new Purchase(group, form.getName());
		for (Item item: template.getItems()) {
			if (item.getState() != ItemState.DELETED) {
				purchase.addItem(new Item(purchase, item.getName()));
			}
		}
		purchaseDao.persist(purchase);
		
		group.addPurchase(purchase);
		groupService.update(group);
		
		return purchase.getEntityId();
	}

	@Override
	public boolean markAsRemoved(Long purchaseId) {
		if (purchaseId == null) {
			return false;
		}
		Purchase purchase = purchaseDao.findById(purchaseId);
		if (purchase == null) {
			return false;
		} else {
			purchase.setState(EntityState.DELETED);
			purchase.setUpdated(new Date());
			purchase.removeAllItems();
			purchaseDao.update(purchase);
			return true;
		}
	}

	@Override
	public boolean edit(EditEntityForm form) {
		if (form == null || form.getEntityId() == null || form.getLastSync() == null || form.getName() == null) {
			return false;
		}
		Purchase purchase = purchaseDao.findById(form.getEntityId());
		if (purchase == null || purchase.getUpdated().after(form.getLastSync())) {
			return false;
		}
		purchase.setName(form.getName());
		purchase.setUpdated(new Date());
		purchaseDao.update(purchase);
		return true;
	}

	@Override
	public boolean edit(EntityForm form) {
		if (form == null || form.getEntityId() == null || form.getName() == null) {
			return false;
		}
		Purchase purchase = purchaseDao.findById(form.getEntityId());
		if (purchase == null) {
			return false;
		}
		purchase.setName(form.getName());
		purchase.setUpdated(new Date());
		purchaseDao.update(purchase);
		return true;
	}

	public void setPurchaseDao(PurchaseDao purchaseDao) {
		this.purchaseDao = purchaseDao;
	}

	public void setItemService(ItemService itemService) {
		this.itemService = itemService;
	}

	public void setGroupService(GroupService groupService) {
		this.groupService = groupService;
	}

	public void setPurchaseTemplateService(
			PurchaseTemplateService purchaseTemplateService) {
		this.purchaseTemplateService = purchaseTemplateService;
	}

}
