package com.cvut.shoppingshare.security;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.cvut.shoppingshare.dao.UserDao;
import com.cvut.shoppingshare.domain.ShareUser;
import com.cvut.shoppingshare.web.form.SignupWebForm;

@Component("shoppingShareAuthenticationProvider")
public class ShoppingShareAuthenticationProvider implements AuthenticationProvider {
	
	private final String authority = "ROLE_USER";
	
	@Autowired
	private UserDao userDao;
	
	/**
	 * Custom implementation of authentication method.
	 */
	@Override
	public Authentication authenticate(Authentication authentication)
			throws AuthenticationException {
		UsernamePasswordAuthenticationToken token = (UsernamePasswordAuthenticationToken) authentication;
		String login = token.getName();
		String password = token.getCredentials().toString();
        
        Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("login", login);
	    List<ShareUser> result = userDao.find(ShareUser.FIND_BY_LOGIN, parameters);
	    if (result.isEmpty()) {
	    	return null;
	    }
	    ShareUser persistedUser = result.get(0);
	    if (checkPassword(persistedUser, password)) {
	    	return null;
	    }
    	List<GrantedAuthority> grantedAuths = new ArrayList<GrantedAuthority>();
    	grantedAuths.add(new SimpleGrantedAuthority(authority));
    	return new ShoppingShareAuthenticationToken(login, password, grantedAuths, persistedUser.getEntityId());
	}

	/**
	 * Method for checking which class of token provider requires.
	 */
	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}
	
	public void logIn(Long userId, SignupWebForm form) {
		List<GrantedAuthority> grantedAuths = new ArrayList<GrantedAuthority>();
    	grantedAuths.add(new SimpleGrantedAuthority(authority));
    	Authentication authentication = new ShoppingShareAuthenticationToken(form.getEmail(), form.getPassword(), grantedAuths, userId);
    	SecurityContextHolder.getContext().setAuthentication(authentication);
	}
	
	private boolean checkPassword(ShareUser persistedUser, String password) {
		if (!persistedUser.getPassword().equals(password)) {
			return true;
		} else {
			return false;
		}
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

}
