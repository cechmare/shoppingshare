package com.cvut.shoppingshare.dao;

import java.util.Date;
import java.util.List;

import com.cvut.shoppingshare.domain.MembershipRequest;
import com.cvut.shoppingshare.web.form.DealtMembershipRequest;
import com.cvut.shoppingshare.web.form.MembershipOfferDto;
import com.cvut.shoppingshare.web.form.MembershipRequestDto;
import com.cvut.shoppingshare.web.form.ShareUserDto;

public interface MembershipRequestDao extends ShoppingShareBaseDao<MembershipRequest> {
	
	public List<MembershipRequestDto> getNonDealtRequestsOfGroupSinceDate(Long groupId, Date lastSync);
	
	public List<DealtMembershipRequest> getDealtRequestsOfGroupSinceDate(Long groupId, Date lastSync);
	
	public List<MembershipOfferDto> getOffersOfUserSinceDate(Long userId, Date lastSync);
	
	public List<MembershipOfferDto> getOffersOfUser(Long userId);
	
	public List<ShareUserDto> getAllRequestsOfGroupWithId(Long groupId);
	
	public List<ShareUserDto> getAllOffersOfGroupWithId(Long groupId);

}
