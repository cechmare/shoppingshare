package com.cvut.shoppingshare.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.cvut.shoppingshare.domain.ShareGroup;
import com.cvut.shoppingshare.web.form.GroupListMessage;

@Repository
public class GroupJpaDao extends ShoppingShareJpaBaseDao<ShareGroup> implements GroupDao {
	
	private static final String USER_ID = "userId";
	
	private static final String LAST_SYNC = "lastSync";
	
	private static final String TAGNAME = "tagName";

	/**
	 * Constructor calling superclass.
	 */
	public GroupJpaDao() {
		super(ShareGroup.class);
	}

	@Override
	public GroupListMessage[] getAllGroupsWhereParticipate(Long userId) {
		TypedQuery<GroupListMessage> query = getEntityManager().createNamedQuery(
				ShareGroup.FIND_WHERE_PARTICIPATE, GroupListMessage.class);
		query.setParameter(USER_ID, userId);
		List<GroupListMessage> result1 = query.getResultList();
		return result1.toArray(new GroupListMessage[result1.size()]);
	}

	@Override
	public GroupListMessage[] getAllGroupsWhereParticipateSinceDate(
			Long userId, Date lastSync) {
		TypedQuery<GroupListMessage> query = getEntityManager().createNamedQuery(
				ShareGroup.FIND_WHERE_PARTICIPATE_CREATED_SINCE_DATE, GroupListMessage.class);
		query.setParameter(USER_ID, userId);
		query.setParameter(LAST_SYNC, lastSync);
		List<GroupListMessage> result1 = query.getResultList();
		
		TypedQuery<GroupListMessage> query2 = getEntityManager().createNamedQuery(
				ShareGroup.FIND_WHERE_PARTICIPATE_SINCE_DATE_BY_MEMBERSHIP_REQUEST, GroupListMessage.class);
		query2.setParameter(USER_ID, userId);
		query2.setParameter(LAST_SYNC, lastSync);
		List<GroupListMessage> result2 = query2.getResultList();
		
		result1.removeAll(result2);
		result1.addAll(result2);
		return result1.toArray(new GroupListMessage[result1.size()]);
	}

	@Override
	public GroupListMessage[] findByNameStartsWith(String tagName) {
		TypedQuery<GroupListMessage> query = getEntityManager().createNamedQuery(
				ShareGroup.FIND_BY_NAME_STARTS_WITH, GroupListMessage.class);
		query.setParameter(TAGNAME, tagName + "%");
		query.setMaxResults(10);
		List<GroupListMessage> groups = query.getResultList();
		return groups.toArray(new GroupListMessage[groups.size()]);
	}
	
}
