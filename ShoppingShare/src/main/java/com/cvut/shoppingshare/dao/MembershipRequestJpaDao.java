package com.cvut.shoppingshare.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.cvut.shoppingshare.domain.MembershipRequest;
import com.cvut.shoppingshare.web.form.DealtMembershipRequest;
import com.cvut.shoppingshare.web.form.MembershipOfferDto;
import com.cvut.shoppingshare.web.form.MembershipRequestDto;
import com.cvut.shoppingshare.web.form.ShareUserDto;

@Repository
public class MembershipRequestJpaDao extends ShoppingShareJpaBaseDao<MembershipRequest> implements MembershipRequestDao {
	
	private static final String GROUP_ID = "groupId";
	
	private static final String USER_ID = "userId";
	
	private static final String LAST_SYNC = "lastSync";
	
	/**
	 * Constructor calling superclass.
	 */
	public MembershipRequestJpaDao() {
		super(MembershipRequest.class);
	}

	@Override
	public List<MembershipRequestDto> getNonDealtRequestsOfGroupSinceDate(Long groupId, Date lastSync) {
		TypedQuery<MembershipRequestDto> query = getEntityManager().createNamedQuery(
				MembershipRequest.GET_NON_DEALT_REQUESTS_OF_GROUP_SINCE_DATE, MembershipRequestDto.class);
		query.setParameter(GROUP_ID, groupId);
		query.setParameter(LAST_SYNC, lastSync);
		return query.getResultList();
	}

	@Override
	public List<DealtMembershipRequest> getDealtRequestsOfGroupSinceDate(Long groupId, Date lastSync) {
		TypedQuery<DealtMembershipRequest> query = getEntityManager().createNamedQuery(
				MembershipRequest.GET_DEALT_REQUESTS_OF_GROUP_SINCE_DATE, DealtMembershipRequest.class);
		query.setParameter(GROUP_ID, groupId);
		query.setParameter(LAST_SYNC, lastSync);
		return query.getResultList();
	}

	@Override
	public List<MembershipOfferDto> getOffersOfUserSinceDate(Long userId,
			Date lastSync) {
		TypedQuery<MembershipOfferDto> query = getEntityManager().createNamedQuery(
				MembershipRequest.GET_OFFERS_OF_USER_SINCE_DATE, MembershipOfferDto.class);
		query.setParameter(USER_ID, userId);
		query.setParameter(LAST_SYNC, lastSync);
		return query.getResultList();
	}

	@Override
	public List<MembershipOfferDto> getOffersOfUser(Long userId) {
		TypedQuery<MembershipOfferDto> query = getEntityManager().createNamedQuery(
				MembershipRequest.GET_OFFERS_OF_USER, MembershipOfferDto.class);
		query.setParameter(USER_ID, userId);
		return query.getResultList();
	}

	@Override
	public List<ShareUserDto> getAllRequestsOfGroupWithId(Long groupId) {
		TypedQuery<ShareUserDto> query = getEntityManager().createNamedQuery(
				MembershipRequest.GET_REQUESTS_OF_GROUP, ShareUserDto.class);
		query.setParameter(GROUP_ID, groupId);
		return query.getResultList();
	}

	@Override
	public List<ShareUserDto> getAllOffersOfGroupWithId(Long groupId) {
		TypedQuery<ShareUserDto> query = getEntityManager().createNamedQuery(
				MembershipRequest.GET_OFFERS_OF_GROUP, ShareUserDto.class);
		query.setParameter(GROUP_ID, groupId);
		return query.getResultList();
	}

}
