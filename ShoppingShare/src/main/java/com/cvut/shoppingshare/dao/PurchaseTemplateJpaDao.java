package com.cvut.shoppingshare.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.cvut.shoppingshare.domain.PurchaseTemplate;
import com.cvut.shoppingshare.web.form.PurchaseDto;
import com.cvut.shoppingshare.web.form.PurchaseWebDto;

@Repository
public class PurchaseTemplateJpaDao extends ShoppingShareJpaBaseDao<PurchaseTemplate> implements PurchaseTemplateDao {
	
	private static final String USER_ID = "userId";
	
	private static final String LAST_SYNC = "lastSync";

	public PurchaseTemplateJpaDao() {
		super(PurchaseTemplate.class);
	}

	@Override
	public List<PurchaseDto> getTemplatesNews(Date lastSync, Long userId) {
		TypedQuery<PurchaseDto> query = getEntityManager().createNamedQuery(
				PurchaseTemplate.FIND_NEWS_BY_USER_ID, PurchaseDto.class);
		query.setParameter(USER_ID, userId);
		query.setParameter(LAST_SYNC, lastSync);
		return query.getResultList();
	}

	@Override
	public List<PurchaseWebDto> getAll(Long userId) {
		TypedQuery<PurchaseWebDto> query = getEntityManager().createNamedQuery(
				PurchaseTemplate.GET_ALL, PurchaseWebDto.class);
		query.setParameter(USER_ID, userId);
		return query.getResultList();
	}
	
}
