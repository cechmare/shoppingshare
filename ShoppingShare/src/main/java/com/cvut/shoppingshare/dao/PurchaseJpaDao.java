package com.cvut.shoppingshare.dao;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.cvut.shoppingshare.domain.Purchase;
import com.cvut.shoppingshare.web.form.NewsRequestMessage;
import com.cvut.shoppingshare.web.form.PurchaseDto;
import com.cvut.shoppingshare.web.form.PurchaseWebDto;

@Repository
public class PurchaseJpaDao extends ShoppingShareJpaBaseDao<Purchase> implements PurchaseDao {
	
	private static final String GROUP_ID = "groupId";
	
	private static final String LAST_SYNC = "lastSync";
	
	/**
	 * Constructor calling superclass.
	 */
	public PurchaseJpaDao() {
		super(Purchase.class);
	}

	@Override
	public List<PurchaseWebDto> getAllPurchasesOfGroup(Long groupId) {
		TypedQuery<PurchaseWebDto> query = getEntityManager().createNamedQuery(
				Purchase.FIND_BY_SHAREGROUP_ID, PurchaseWebDto.class);
		query.setParameter(GROUP_ID, groupId);
		return query.getResultList();
	}

	@Override
	public List<PurchaseDto> getPurchasesNews(NewsRequestMessage request) {
		TypedQuery<PurchaseDto> query = getEntityManager().createNamedQuery(
				Purchase.FIND_NEWS_BY_SHAREGROUP_ID, PurchaseDto.class);
		query.setParameter(GROUP_ID, request.getEntityId());
		query.setParameter(LAST_SYNC, request.getLastSync());
		return query.getResultList();
	}

}
