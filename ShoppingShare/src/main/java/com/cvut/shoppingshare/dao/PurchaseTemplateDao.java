package com.cvut.shoppingshare.dao;

import java.util.Date;
import java.util.List;

import com.cvut.shoppingshare.domain.PurchaseTemplate;
import com.cvut.shoppingshare.web.form.PurchaseDto;
import com.cvut.shoppingshare.web.form.PurchaseWebDto;

public interface PurchaseTemplateDao extends ShoppingShareBaseDao<PurchaseTemplate> {
	
	public List<PurchaseDto> getTemplatesNews(Date lastSync, Long userId);
	
	public List<PurchaseWebDto> getAll(Long userId);

}
