package com.cvut.shoppingshare.dao;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.cvut.shoppingshare.domain.ShareUser;
import com.cvut.shoppingshare.web.form.ShareUserDto;
import com.cvut.shoppingshare.web.form.ShareUserWebDto;
import com.cvut.shoppingshare.web.form.UserAutocompleteForm;

@Repository
public class UserJpaDao extends ShoppingShareJpaBaseDao<ShareUser> implements UserDao {
	
	private static final String GROUP_ID = "groupId";
	
	private static final String TAGNAME = "tagName"; 
	
	/**
	 * Constructor calling superclass.
	 */
	public UserJpaDao() {
		super(ShareUser.class);
	}

	@Override
	public List<ShareUserDto> getShareUsersOfGroup(Long groupId) {
		TypedQuery<ShareUserDto> query = getEntityManager().createNamedQuery(
				ShareUser.GET_SHARE_USERS_OF_GROUP, ShareUserDto.class);
		query.setParameter(GROUP_ID, groupId);
		return query.getResultList();
	}

	@Override
	public List<UserAutocompleteForm> findByLoginFirstnameLastnameStartsWith(
			String tagName) {
		TypedQuery<UserAutocompleteForm> query = getEntityManager().createNamedQuery(
				ShareUser.FIND_BY_LOGIN_FIRSTNAME_LASTNAME_STARTS_WITH, UserAutocompleteForm.class);
		query.setParameter(TAGNAME, tagName + "%");
		query.setMaxResults(10);
		return query.getResultList();
	}

	@Override
	public List<ShareUserWebDto> getAllOfGroupWithId(Long groupId) {
		TypedQuery<ShareUserWebDto> query = getEntityManager().createNamedQuery(
				ShareUser.GET_ALL_OF_GROUP_WITH_ID, ShareUserWebDto.class);
		query.setParameter(GROUP_ID, groupId);
		return query.getResultList();
	}

}
