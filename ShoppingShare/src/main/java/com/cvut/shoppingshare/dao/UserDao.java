package com.cvut.shoppingshare.dao;

import java.util.List;

import com.cvut.shoppingshare.domain.ShareUser;
import com.cvut.shoppingshare.web.form.ShareUserDto;
import com.cvut.shoppingshare.web.form.ShareUserWebDto;
import com.cvut.shoppingshare.web.form.UserAutocompleteForm;

public interface UserDao extends ShoppingShareBaseDao<ShareUser> {

	public List<ShareUserDto> getShareUsersOfGroup(Long groupId);
	
	public List<UserAutocompleteForm> findByLoginFirstnameLastnameStartsWith(String tagName);
	
	public List<ShareUserWebDto> getAllOfGroupWithId(Long groupId);
	
}
