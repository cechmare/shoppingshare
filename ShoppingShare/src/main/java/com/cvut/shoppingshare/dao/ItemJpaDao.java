package com.cvut.shoppingshare.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.cvut.shoppingshare.domain.Item;
import com.cvut.shoppingshare.web.form.ItemDto;
import com.cvut.shoppingshare.web.form.ItemTemplateDto;
import com.cvut.shoppingshare.web.form.ItemWebDto;
import com.cvut.shoppingshare.web.form.NewsRequestMessage;

@Repository
public class ItemJpaDao extends ShoppingShareJpaBaseDao<Item> implements ItemDao {
	
	private static final String GROUP_ID = "groupId";
	
	private static final String USER_ID = "userId";
	
	private static final String PURCHASE_ID = "purchaseId";
	
	private static final String LAST_SYNC = "lastSync";
	
	private static final String TEMPLATE_ID = "templateId";
	
	/**
	 * Constructor calling superclass.
	 */
	public ItemJpaDao() {
		super(Item.class);
	}

	@Override
	public List<ItemDto> getItemsNews(NewsRequestMessage request) {
		TypedQuery<ItemDto> query = getEntityManager().createNamedQuery(
				 Item.FIND_NEWS_BY_SHAREGROUP_ID, ItemDto.class);
		query.setParameter(GROUP_ID, request.getEntityId());
		query.setParameter(LAST_SYNC, request.getLastSync());
		return query.getResultList();
	}

	@Override
	public List<ItemDto> getItemTemplatesNews(Date lastSync, Long userId) {
		TypedQuery<ItemDto> query = getEntityManager().createNamedQuery(
				 Item.FIND_TEMPLATES_NEWS_BY_USER_ID, ItemDto.class);
		query.setParameter(USER_ID, userId);
		query.setParameter(LAST_SYNC, lastSync);
		return query.getResultList();
	}

	@Override
	public List<String> getNonDeletedByPurchaseId(Long purchaseId) {
		TypedQuery<String> query = getEntityManager().createNamedQuery(
				 Item.FIND_NON_DELETED_BY_PURCHASE_ID, String.class);
		query.setParameter(PURCHASE_ID, purchaseId);
		return query.getResultList();
	}

	@Override
	public List<String> getNonDeletedByPurchaseTemplateId(Long templateId) {
		TypedQuery<String> query = getEntityManager().createNamedQuery(
				 Item.FIND_NON_DELETED_BY_PURCHASE_TEMPLATE_ID, String.class);
		query.setParameter(TEMPLATE_ID, templateId);
		return query.getResultList();
	}

	@Override
	public List<ItemTemplateDto> getAllTemplates(Long templateId) {
		TypedQuery<ItemTemplateDto> query = getEntityManager().createNamedQuery(
				 Item.FIND_TEMPLATES_BY_PURCHASE_TEMPLATE_ID, ItemTemplateDto.class);
		query.setParameter(TEMPLATE_ID, templateId);
		return query.getResultList();
	}

	@Override
	public List<ItemWebDto> getAll(Long purchaseId) {
		TypedQuery<ItemWebDto> query = getEntityManager().createNamedQuery(
				 Item.GET_ALL_OF_PURCHASE, ItemWebDto.class);
		query.setParameter(PURCHASE_ID, purchaseId);
		return query.getResultList();
	}

}
