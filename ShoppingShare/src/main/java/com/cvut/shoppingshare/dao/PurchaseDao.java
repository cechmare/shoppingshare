package com.cvut.shoppingshare.dao;

import java.util.List;

import com.cvut.shoppingshare.domain.Purchase;
import com.cvut.shoppingshare.web.form.NewsRequestMessage;
import com.cvut.shoppingshare.web.form.PurchaseDto;
import com.cvut.shoppingshare.web.form.PurchaseWebDto;

public interface PurchaseDao extends ShoppingShareBaseDao<Purchase> {
	
	public List<PurchaseWebDto> getAllPurchasesOfGroup(Long groupId);
	
	public List<PurchaseDto> getPurchasesNews(NewsRequestMessage request);
	
}
