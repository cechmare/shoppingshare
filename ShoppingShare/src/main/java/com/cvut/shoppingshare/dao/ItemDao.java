package com.cvut.shoppingshare.dao;

import java.util.Date;
import java.util.List;

import com.cvut.shoppingshare.domain.Item;
import com.cvut.shoppingshare.web.form.ItemDto;
import com.cvut.shoppingshare.web.form.ItemTemplateDto;
import com.cvut.shoppingshare.web.form.ItemWebDto;
import com.cvut.shoppingshare.web.form.NewsRequestMessage;

public interface ItemDao extends ShoppingShareBaseDao<Item> {

	public List<ItemDto> getItemsNews(NewsRequestMessage request);
	
	public List<ItemDto> getItemTemplatesNews(Date lastSync, Long userId);
	
	public List<String> getNonDeletedByPurchaseId(Long purchaseId);
	
	public List<String> getNonDeletedByPurchaseTemplateId(Long templateId);
	
	public List<ItemTemplateDto> getAllTemplates(Long templateId);
	
	public List<ItemWebDto> getAll(Long purchaseId);
	
}
