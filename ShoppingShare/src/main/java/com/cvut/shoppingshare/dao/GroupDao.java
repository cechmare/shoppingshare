package com.cvut.shoppingshare.dao;

import java.util.Date;

import com.cvut.shoppingshare.domain.ShareGroup;
import com.cvut.shoppingshare.web.form.GroupListMessage;

public interface GroupDao extends ShoppingShareBaseDao<ShareGroup> {
	
	public GroupListMessage[] getAllGroupsWhereParticipate(Long userId);

	public GroupListMessage[] getAllGroupsWhereParticipateSinceDate(Long userId, Date lastSync);
	
	public GroupListMessage[] findByNameStartsWith(String tagName);
	
}
